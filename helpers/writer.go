package helpers

import (
	"dg-datamaster-service/services/logging"
	"encoding/json"
	"net/http"
)

func WriteResponse(r *http.Request, w http.ResponseWriter, response any) {
	w.Header().Set("Content-Type", "application/json")
	w.Header().Set("Access-Control-Allow-Origin", "*")

	logging.IncomingRequest(r.RequestURI, "response", response)

	_ = json.NewEncoder(w).Encode(response)
}
