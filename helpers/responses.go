package helpers

type ValidationError map[string]string

type Response struct {
	Metadata ResponseMetadata `json:"metadata"`
	Data     any              `json:"data,omitempty"`
}

type ResponseMetadata struct {
	ResponseCode string          `json:"response_code"`
	Message      string          `json:"message"`
	Errors       ValidationError `json:"errors,omitempty"`
	Page         *int64          `json:"page,omitempty"`
	PerPage      *int64          `json:"per_page,omitempty"`
	TotalData    *int64          `json:"total_data,omitempty"`
	TotalPage    *int64          `json:"total_page,omitempty"`
}

func SuccessResponse(data any) Response {
	return Response{
		Metadata: ResponseMetadata{
			ResponseCode: "000",
			Message:      "Berhasil",
		},
		Data: data,
	}
}

func PaginatedSuccessResponse(data any, page int64, perPage int64, totalData int64, totalPage int64) Response {
	return Response{
		Metadata: ResponseMetadata{
			ResponseCode: "000",
			Message:      "Berhasil",
			Page:         &page,
			PerPage:      &perPage,
			TotalData:    &totalData,
			TotalPage:    &totalPage,
		},
		Data: data,
	}
}

func UnauthorizedResponse() Response {
	return Response{
		Metadata: ResponseMetadata{
			ResponseCode: "901",
			Message:      "Unauthorized",
		},
		Data: nil,
	}
}

func BadRequestResponse() Response {
	return Response{
		Metadata: ResponseMetadata{
			ResponseCode: "902",
			Message:      "Bad Request",
		},
		Data: nil,
	}
}

func ValidationErrorResponse(validationError ValidationError) Response {
	return Response{
		Metadata: ResponseMetadata{
			ResponseCode: "903",
			Message:      "Validation Error",
			Errors:       validationError,
		},
		Data: nil,
	}
}

func NewResponse(responseCode string, message string, data any) Response {
	return Response{
		Metadata: ResponseMetadata{
			ResponseCode: responseCode,
			Message:      message,
		},
		Data: data,
	}
}

func ServerError(msg string) Response {
	var respMessage string
	if msg != "" {
		respMessage = "Server Error: " + msg
	} else {
		respMessage = "Server Error"
	}

	return Response{
		Metadata: ResponseMetadata{
			ResponseCode: "999",
			Message:      respMessage,
		},
		Data: nil,
	}
}

func ClientErrorResponse(msg string) Response {
	return Response{
		Metadata: ResponseMetadata{
			ResponseCode: "499",
			Message:      "Client Error: " + msg,
		},
	}
}
