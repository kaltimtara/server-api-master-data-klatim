package controllers

import (
	"database/sql"
	"dg-datamaster-service/helpers"
	"dg-datamaster-service/models"
	"dg-datamaster-service/services/logging"
	"encoding/json"
	"errors"
	"github.com/julienschmidt/httprouter"
	"math"
	"net/http"
)

type CityCreateRequest struct {
	CreatedBy    string `json:"created_by"`
	ProvinceId   int    `json:"province_id"`
	CityCode     string `json:"city_code"`
	BankCityCode string `json:"bank_city_code"`
	CityName     string `json:"city_name"`
}

type CityCreateResponse struct {
	Id           int64  `json:"id"`
	ProvinceId   int    `json:"province_id"`
	CityCode     string `json:"city_code"`
	BankCityCode string `json:"bank_city_code"`
	CityName     string `json:"city_name"`
}

type CityUpdateRequest struct {
	ProvinceId   int    `json:"province_id"`
	CityCode     string `json:"city_code"`
	BankCityCode string `json:"bank_city_code"`
	CityName     string `json:"city_name"`
	IsActive     bool   `json:"is_active"`
	UpdatedBy    string `json:"updated_by"`
}

type CityUpdateResponse struct {
	Id           int64  `json:"id"`
	ProvinceId   int    `json:"province_id"`
	CityCode     string `json:"city_code"`
	BankCityCode string `json:"bank_city_code"`
	CityName     string `json:"city_name"`
	IsActive     bool   `json:"is_active"`
}

type CityGetResponse struct {
	Id           int64  `json:"id"`
	ProvinceId   int    `json:"province_id"`
	CityCode     string `json:"city_code"`
	BankCityCode string `json:"bank_city_code"`
	CityName     string `json:"city_name"`
	IsActive     bool   `json:"is_active"`
}

func CityCreate(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	var req CityCreateRequest
	err := json.NewDecoder(r.Body).Decode(&req)
	if err != nil {
		helpers.WriteResponse(r, w, helpers.BadRequestResponse())
		return
	}

	city := req.ToModel()
	err = city.Create()
	if err != nil {
		logging.Error("city", "CityCreate: error create", logging.Data{
			"error": err.Error(),
			"body":  req,
		})

		helpers.WriteResponse(r, w, helpers.ServerError("Failed to save city data"))
		return
	}

	var rsp CityCreateResponse
	rsp.FromModel(city)

	helpers.WriteResponse(r, w, helpers.SuccessResponse(rsp))
}

func CityGet(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	id := ps.ByName("id")

	var city models.City
	err := city.Get(id)
	if errors.Is(err, sql.ErrNoRows) {
		helpers.WriteResponse(r, w, helpers.ClientErrorResponse("City data not found"))
		return
	} else if err != nil {
		logging.Error("city", "CityGet: error get", logging.Data{
			"id":    id,
			"error": err.Error(),
		})

		helpers.WriteResponse(r, w, helpers.ServerError("Failed to retrieve city data"))
		return
	}

	var rsp CityGetResponse
	rsp.FromModel(city)

	helpers.WriteResponse(r, w, helpers.SuccessResponse(rsp))
}

func CityList(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	var pr models.PaginationRequest
	err := pr.FromRequest(r)
	if err != nil {
		helpers.WriteResponse(r, w, helpers.BadRequestResponse())
		return
	}

	var f models.CityFilter
	f.FromRequest(r)

	var city models.City
	cities, total, err := city.List(pr, &f)
	if err != nil {
		logging.Error("city", "CityList: error list", logging.Data{
			"error": err.Error(),
		})

		helpers.WriteResponse(r, w, helpers.ServerError("Failed to retrieve city data"))
		return
	}

	var tmp CityGetResponse
	rsp := tmp.FromModels(cities)

	totalPage := math.Ceil(float64(total) / float64(pr.PerPage))
	helpers.WriteResponse(r, w, helpers.PaginatedSuccessResponse(rsp, int64(pr.Page), int64(pr.PerPage), total,
		int64(totalPage)))
}

func CityUpdate(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	id := ps.ByName("id")

	var req CityUpdateRequest
	err := json.NewDecoder(r.Body).Decode(&req)
	if err != nil {
		helpers.WriteResponse(r, w, helpers.BadRequestResponse())
		return
	}

	var city models.City
	err = city.Get(id)
	if errors.Is(err, sql.ErrNoRows) {
		helpers.WriteResponse(r, w, helpers.ClientErrorResponse("City data not found"))
		return
	} else if err != nil {
		logging.Error("city", "CityUpdate: error get", logging.Data{
			"id":    id,
			"error": err.Error(),
		})

		helpers.WriteResponse(r, w, helpers.ServerError("Failed to retrieve city data"))
		return
	}

	req.FillModel(&city)
	err = city.Update()
	if err != nil {
		logging.Error("city", "CityUpdate: error update", logging.Data{
			"error": err.Error(),
			"body":  req,
		})

		helpers.WriteResponse(r, w, helpers.ServerError("Failed to save city data"))
		return
	}

	var rsp CityUpdateResponse
	rsp.FromModel(city)
	helpers.WriteResponse(r, w, helpers.SuccessResponse(rsp))

}

func CityDelete(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	id := ps.ByName("id")

	var city models.City
	err := city.Get(id)
	if errors.Is(err, sql.ErrNoRows) {
		helpers.WriteResponse(r, w, helpers.ClientErrorResponse("City data not found"))
		return
	} else if err != nil {
		logging.Error("city", "CityDelete: error get", logging.Data{
			"id":    id,
			"error": err.Error(),
		})

		helpers.WriteResponse(r, w, helpers.ServerError("Failed to retrieve city data"))
		return
	}

	err = city.Delete()
	if err != nil {
		logging.Error("city", "CityDelete: error delete", logging.Data{
			"error": err.Error(),
			"id":    id,
		})

		helpers.WriteResponse(r, w, helpers.ServerError("Failed to delete city data"))
		return
	}

	helpers.WriteResponse(r, w, helpers.SuccessResponse(nil))
}

func (r *CityCreateRequest) ToModel() models.City {
	return models.City{
		CreatedBy:    r.CreatedBy,
		ProvinceId:   r.ProvinceId,
		CityCode:     r.CityCode,
		BankCityCode: r.BankCityCode,
		CityName:     r.CityName,
	}
}

func (r *CityUpdateRequest) FillModel(c *models.City) {
	c.ProvinceId = r.ProvinceId
	c.CityCode = r.CityCode
	c.BankCityCode = r.BankCityCode
	c.CityName = r.CityName
	c.IsActive = r.IsActive
	c.UpdatedBy = &r.UpdatedBy
}

func (r *CityCreateResponse) FromModel(c models.City) {
	r.Id = c.Id
	r.ProvinceId = c.ProvinceId
	r.CityCode = c.CityCode
	r.BankCityCode = c.BankCityCode
	r.CityName = c.CityName
}

func (r *CityGetResponse) FromModel(c models.City) {
	r.Id = c.Id
	r.ProvinceId = c.ProvinceId
	r.CityCode = c.CityCode
	r.BankCityCode = c.BankCityCode
	r.CityName = c.CityName
	r.IsActive = c.IsActive
}

func (r *CityGetResponse) FromModels(cs []models.Model) []CityGetResponse {
	var rs []CityGetResponse
	for _, m := range cs {
		var r CityGetResponse

		city := m.(*models.City)
		r.FromModel(*city)
		rs = append(rs, r)
	}

	return rs
}

func (r *CityUpdateResponse) FromModel(c models.City) {
	r.Id = c.Id
	r.ProvinceId = c.ProvinceId
	r.CityCode = c.CityCode
	r.BankCityCode = c.BankCityCode
	r.CityName = c.CityName
	r.IsActive = c.IsActive
}
