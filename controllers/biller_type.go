package controllers

import (
	"database/sql"
	"dg-datamaster-service/helpers"
	"dg-datamaster-service/models"
	"dg-datamaster-service/services/logging"
	"encoding/json"
	"errors"
	"math"
	"net/http"

	"github.com/julienschmidt/httprouter"
)

type BillerTypeCreateRequest struct {
	CreatedBy  string `json:"created_by"`
	BillerType string `json:"biller_type"`
	TrxTypeId  int64  `json:"trx_type_id"`
}

type BillerTypeCreateResponse struct {
	Id         int64  `json:"id"`
	BillerType string `json:"biller_type"`
	TrxTypeId  int64  `json:"trx_type_id"`
}

type BillerTypeUpdateRequest struct {
	BillerType string `json:"biller_type"`
	TrxTypeId  int64  `json:"trx_type_id"`
	IsActive   bool   `json:"is_active"`
	UpdatedBy  string `json:"updated_by"`
}

type BillerTypeUpdateResponse struct {
	Id         int64  `json:"id"`
	BillerType string `json:"biller_type"`
	TrxTypeId  int64  `json:"trx_type_id"`
	IsActive   bool   `json:"is_active"`
}

type BillerTypeGetResponse struct {
	Id         int64  `json:"id"`
	BillerType string `json:"biller_type"`
	TrxTypeId  int64  `json:"trx_type_id"`
	IsActive   bool   `json:"is_active"`
}

func BillerTypeCreate(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	var req BillerTypeCreateRequest
	err := json.NewDecoder(r.Body).Decode(&req)
	if err != nil {
		helpers.WriteResponse(r, w, helpers.BadRequestResponse())
		return
	}

	billerType := req.ToModel()
	err = billerType.Create()
	if err != nil {
		logging.Error("billerType", "BillerTypeCreate: error create", logging.Data{
			"error": err.Error(),
			"body":  req,
		})

		helpers.WriteResponse(r, w, helpers.ServerError("Failed to save biller type data"))
		return
	}

	var rsp BillerTypeCreateResponse
	rsp.FromModel(billerType)

	helpers.WriteResponse(r, w, helpers.SuccessResponse(rsp))
}

func BillerTypeGet(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	id := ps.ByName("id")

	var billerType models.BillerType
	err := billerType.Get(id)
	if errors.Is(err, sql.ErrNoRows) {
		helpers.WriteResponse(r, w, helpers.ClientErrorResponse("Biller type data not found"))
		return
	} else if err != nil {
		logging.Error("billerType", "BillerTypeGet: error get", logging.Data{
			"id":    id,
			"error": err.Error(),
		})

		helpers.WriteResponse(r, w, helpers.ServerError("Failed to retrieve biller type data"))
		return
	}

	var rsp BillerTypeGetResponse
	rsp.FromModel(billerType)

	helpers.WriteResponse(r, w, helpers.SuccessResponse(rsp))
}

func BillerTypeList(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	var pr models.PaginationRequest
	err := pr.FromRequest(r)
	if err != nil {
		helpers.WriteResponse(r, w, helpers.BadRequestResponse())
		return
	}

	var f models.BillerTypeFilter
	f.FromRequest(r)

	var billerType models.BillerType
	billerTypes, total, err := billerType.List(pr, &f)
	if err != nil {
		logging.Error("billerType", "BillerTypeList: error list", logging.Data{
			"error": err.Error(),
		})

		helpers.WriteResponse(r, w, helpers.ServerError("Failed to retrieve biller type data"))
		return
	}

	var tmp BillerTypeGetResponse
	rsp := tmp.FromModels(billerTypes)

	totalPage := math.Ceil(float64(total) / float64(pr.PerPage))
	helpers.WriteResponse(r, w, helpers.PaginatedSuccessResponse(rsp, int64(pr.Page), int64(pr.PerPage), total,
		int64(totalPage)))
}

func BillerTypeUpdate(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	id := ps.ByName("id")

	var req BillerTypeUpdateRequest
	err := json.NewDecoder(r.Body).Decode(&req)
	if err != nil {
		helpers.WriteResponse(r, w, helpers.BadRequestResponse())
		return
	}

	var billerType models.BillerType
	err = billerType.Get(id)
	if errors.Is(err, sql.ErrNoRows) {
		helpers.WriteResponse(r, w, helpers.ClientErrorResponse("Biller type data not found"))
		return
	} else if err != nil {
		logging.Error("billerType", "BillerTypeUpdate: error get", logging.Data{
			"id":    id,
			"error": err.Error(),
		})

		helpers.WriteResponse(r, w, helpers.ServerError("Failed to retrieve biller type data"))
		return
	}

	req.FillModel(&billerType)
	err = billerType.Update()
	if err != nil {
		logging.Error("billerType", "BillerTypeUpdate: error update", logging.Data{
			"error": err.Error(),
			"body":  req,
		})

		helpers.WriteResponse(r, w, helpers.ServerError("Failed to save biller type data"))
		return
	}

	var rsp BillerTypeUpdateResponse
	rsp.FromModel(billerType)
	helpers.WriteResponse(r, w, helpers.SuccessResponse(rsp))

}

func BillerTypeDelete(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	id := ps.ByName("id")

	var billerType models.BillerType
	err := billerType.Get(id)
	if errors.Is(err, sql.ErrNoRows) {
		helpers.WriteResponse(r, w, helpers.ClientErrorResponse("Biller type data not found"))
		return
	} else if err != nil {
		logging.Error("billerType", "BillerTypeDelete: error get", logging.Data{
			"id":    id,
			"error": err.Error(),
		})

		helpers.WriteResponse(r, w, helpers.ServerError("Failed to retrieve biller type data"))
		return
	}

	err = billerType.Delete()
	if err != nil {
		logging.Error("billerType", "BillerTypeDelete: error delete", logging.Data{
			"error": err.Error(),
			"id":    id,
		})

		helpers.WriteResponse(r, w, helpers.ServerError("Failed to delete biller type data"))
		return
	}

	helpers.WriteResponse(r, w, helpers.SuccessResponse(nil))
}

func (r *BillerTypeCreateRequest) ToModel() models.BillerType {
	return models.BillerType{
		CreatedBy:  r.CreatedBy,
		BillerType: r.BillerType,
		TrxTypeId:  r.TrxTypeId,
	}
}

func (r *BillerTypeUpdateRequest) FillModel(b *models.BillerType) {
	b.BillerType = r.BillerType
	b.TrxTypeId = r.TrxTypeId
	b.IsActive = r.IsActive
	b.UpdatedBy = &r.UpdatedBy
}

func (r *BillerTypeCreateResponse) FromModel(b models.BillerType) {
	r.Id = b.Id
	r.BillerType = b.BillerType
	r.TrxTypeId = b.TrxTypeId
}

func (r *BillerTypeGetResponse) FromModel(b models.BillerType) {
	r.Id = b.Id
	r.BillerType = b.BillerType
	r.TrxTypeId = b.TrxTypeId
	r.IsActive = b.IsActive
}

func (r *BillerTypeGetResponse) FromModels(bs []models.Model) []BillerTypeGetResponse {
	var rs []BillerTypeGetResponse
	for _, m := range bs {
		var r BillerTypeGetResponse

		billerType := m.(*models.BillerType)
		r.FromModel(*billerType)
		rs = append(rs, r)
	}

	return rs
}

func (r *BillerTypeUpdateResponse) FromModel(b models.BillerType) {
	r.Id = b.Id
	r.BillerType = b.BillerType
	r.TrxTypeId = b.TrxTypeId
	r.IsActive = b.IsActive
}
