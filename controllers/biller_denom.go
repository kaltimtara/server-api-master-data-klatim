package controllers

import (
	"database/sql"
	"dg-datamaster-service/helpers"
	"dg-datamaster-service/models"
	"dg-datamaster-service/services/logging"
	"encoding/json"
	"errors"
	"math"
	"net/http"

	"github.com/julienschmidt/httprouter"
)

type BillerDenomCreateRequest struct {
	CreatedBy    string  `json:"created_by"`
	BillerTypeId int64   `json:"biller_type_id"`
	BillerDenom  float64 `json:"biller_denom"`
	Description  string  `json:"description"`
}

type BillerDenomCreateResponse struct {
	Id           int64   `json:"id"`
	BillerTypeId int64   `json:"biller_type_id"`
	BillerDenom  float64 `json:"biller_denom"`
	Description  string  `json:"description"`
}

type BillerDenomUpdateRequest struct {
	BillerTypeId int64   `json:"biller_type_id"`
	BillerDenom  float64 `json:"biller_denom"`
	Description  string  `json:"description"`
	IsActive     bool    `json:"is_active"`
	UpdatedBy    string  `json:"updated_by"`
}

type BillerDenomUpdateResponse struct {
	Id           int64   `json:"id"`
	BillerTypeId int64   `json:"biller_type_id"`
	BillerDenom  float64 `json:"biller_denom"`
	Description  string  `json:"description"`
	IsActive     bool    `json:"is_active"`
}

type BillerDenomGetResponse struct {
	Id           int64   `json:"id"`
	BillerTypeId int64   `json:"biller_type_id"`
	BillerDenom  float64 `json:"biller_denom"`
	Description  string  `json:"description"`
	IsActive     bool    `json:"is_active"`
}

func BillerDenomCreate(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	var req BillerDenomCreateRequest
	err := json.NewDecoder(r.Body).Decode(&req)
	if err != nil {
		helpers.WriteResponse(r, w, helpers.BadRequestResponse())
		return
	}

	billerDenom := req.ToModel()
	err = billerDenom.Create()
	if err != nil {
		logging.Error("billerDenom", "BillerDenomCreate: error create", logging.Data{
			"error": err.Error(),
			"body":  req,
		})

		helpers.WriteResponse(r, w, helpers.ServerError("Failed to save biller denom data"))
		return
	}

	var rsp BillerDenomCreateResponse
	rsp.FromModel(billerDenom)

	helpers.WriteResponse(r, w, helpers.SuccessResponse(rsp))
}

func BillerDenomGet(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	id := ps.ByName("id")

	var billerDenom models.BillerDenom
	err := billerDenom.Get(id)
	if errors.Is(err, sql.ErrNoRows) {
		helpers.WriteResponse(r, w, helpers.ClientErrorResponse("Biller denom data not found"))
		return
	} else if err != nil {
		logging.Error("billerDenom", "BillerDenomGet: error get", logging.Data{
			"id":    id,
			"error": err.Error(),
		})

		helpers.WriteResponse(r, w, helpers.ServerError("Failed to retrieve biller denom data"))
		return
	}

	var rsp BillerDenomGetResponse
	rsp.FromModel(billerDenom)

	helpers.WriteResponse(r, w, helpers.SuccessResponse(rsp))
}

func BillerDenomList(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	var pr models.PaginationRequest
	err := pr.FromRequest(r)
	if err != nil {
		helpers.WriteResponse(r, w, helpers.BadRequestResponse())
		return
	}

	var f models.BillerDenomFilter
	f.FromRequest(r)

	var billerDenom models.BillerDenom
	billerDenoms, total, err := billerDenom.List(pr, &f)
	if err != nil {
		logging.Error("billerDenom", "BillerDenomList: error list", logging.Data{
			"error": err.Error(),
		})

		helpers.WriteResponse(r, w, helpers.ServerError("Failed to retrieve biller denom data"))
		return
	}

	var tmp BillerDenomGetResponse
	rsp := tmp.FromModels(billerDenoms)

	totalPage := math.Ceil(float64(total) / float64(pr.PerPage))
	helpers.WriteResponse(r, w, helpers.PaginatedSuccessResponse(rsp, int64(pr.Page), int64(pr.PerPage), total,
		int64(totalPage)))
}

func BillerDenomUpdate(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	id := ps.ByName("id")

	var req BillerDenomUpdateRequest
	err := json.NewDecoder(r.Body).Decode(&req)
	if err != nil {
		helpers.WriteResponse(r, w, helpers.BadRequestResponse())
		return
	}

	var billerDenom models.BillerDenom
	err = billerDenom.Get(id)
	if errors.Is(err, sql.ErrNoRows) {
		helpers.WriteResponse(r, w, helpers.ClientErrorResponse("Biller denom data not found"))
		return
	} else if err != nil {
		logging.Error("billerDenom", "BillerDenomUpdate: error get", logging.Data{
			"id":    id,
			"error": err.Error(),
		})

		helpers.WriteResponse(r, w, helpers.ServerError("Failed to retrieve biller denom data"))
		return
	}

	req.FillModel(&billerDenom)
	err = billerDenom.Update()
	if err != nil {
		logging.Error("billerDenom", "BillerDenomUpdate: error update", logging.Data{
			"error": err.Error(),
			"body":  req,
		})

		helpers.WriteResponse(r, w, helpers.ServerError("Failed to save biller denom data"))
		return
	}

	var rsp BillerDenomUpdateResponse
	rsp.FromModel(billerDenom)
	helpers.WriteResponse(r, w, helpers.SuccessResponse(rsp))

}

func BillerDenomDelete(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	id := ps.ByName("id")

	var billerDenom models.BillerDenom
	err := billerDenom.Get(id)
	if errors.Is(err, sql.ErrNoRows) {
		helpers.WriteResponse(r, w, helpers.ClientErrorResponse("Biller denom data not found"))
		return
	} else if err != nil {
		logging.Error("billerDenom", "BillerDenomDelete: error get", logging.Data{
			"id":    id,
			"error": err.Error(),
		})

		helpers.WriteResponse(r, w, helpers.ServerError("Failed to retrieve biller denom data"))
		return
	}

	err = billerDenom.Delete()
	if err != nil {
		logging.Error("billerDenom", "BillerDenomDelete: error delete", logging.Data{
			"error": err.Error(),
			"id":    id,
		})

		helpers.WriteResponse(r, w, helpers.ServerError("Failed to delete biller denom data"))
		return
	}

	helpers.WriteResponse(r, w, helpers.SuccessResponse(nil))
}

func (r *BillerDenomCreateRequest) ToModel() models.BillerDenom {
	return models.BillerDenom{
		CreatedBy:    r.CreatedBy,
		BillerTypeId: r.BillerTypeId,
		BillerDenom:  r.BillerDenom,
		Description:  r.Description,
	}
}

func (r *BillerDenomUpdateRequest) FillModel(b *models.BillerDenom) {
	b.BillerTypeId = r.BillerTypeId
	b.BillerDenom = r.BillerDenom
	b.Description = r.Description
	b.IsActive = r.IsActive
	b.UpdatedBy = &r.UpdatedBy
}

func (r *BillerDenomCreateResponse) FromModel(b models.BillerDenom) {
	r.Id = b.Id
	r.BillerTypeId = b.BillerTypeId
	r.BillerDenom = b.BillerDenom
	r.Description = b.Description
}

func (r *BillerDenomGetResponse) FromModel(b models.BillerDenom) {
	r.Id = b.Id
	r.BillerTypeId = b.BillerTypeId
	r.BillerDenom = b.BillerDenom
	r.Description = b.Description
	r.IsActive = b.IsActive
}

func (r *BillerDenomGetResponse) FromModels(bs []models.Model) []BillerDenomGetResponse {
	var rs []BillerDenomGetResponse
	for _, m := range bs {
		var r BillerDenomGetResponse

		billerDenom := m.(*models.BillerDenom)
		r.FromModel(*billerDenom)
		rs = append(rs, r)
	}

	return rs
}

func (r *BillerDenomUpdateResponse) FromModel(b models.BillerDenom) {
	r.Id = b.Id
	r.BillerTypeId = b.BillerTypeId
	r.BillerDenom = b.BillerDenom
	r.Description = b.Description
	r.IsActive = b.IsActive
}
