package controllers

import (
	"database/sql"
	"dg-datamaster-service/helpers"
	"dg-datamaster-service/models"
	"dg-datamaster-service/services/logging"
	"encoding/json"
	"errors"
	"math"
	"net/http"

	"github.com/julienschmidt/httprouter"
)

type BillingCodePrefixCreateRequest struct {
	CreatedBy    string `json:"created_by"`
	BillerTypeId int64  `json:"biller_type_id"`
	BillerPrefix string `json:"biller_prefix"`
	Description  string `json:"description"`
}

type BillingCodePrefixCreateResponse struct {
	Id           int64  `json:"id"`
	BillerTypeId int64  `json:"biller_type_id"`
	BillerPrefix string `json:"biller_prefix"`
	Description  string `json:"description"`
}

type BillingCodePrefixUpdateRequest struct {
	BillerTypeId int64  `json:"biller_type_id"`
	BillerPrefix string `json:"biller_prefix"`
	Description  string `json:"description"`
	IsActive     bool   `json:"is_active"`
	UpdatedBy    string `json:"updated_by"`
}

type BillingCodePrefixUpdateResponse struct {
	Id           int64  `json:"id"`
	BillerTypeId int64  `json:"biller_type_id"`
	BillerPrefix string `json:"biller_prefix"`
	Description  string `json:"description"`
	IsActive     bool   `json:"is_active"`
}

type BillingCodePrefixGetResponse struct {
	Id           int64  `json:"id"`
	BillerTypeId int64  `json:"biller_type_id"`
	BillerPrefix string `json:"biller_prefix"`
	Description  string `json:"description"`
	IsActive     bool   `json:"is_active"`
}

func BillingCodePrefixCreate(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	var req BillingCodePrefixCreateRequest
	err := json.NewDecoder(r.Body).Decode(&req)
	if err != nil {
		helpers.WriteResponse(r, w, helpers.BadRequestResponse())
		return
	}

	billingCodePrefix := req.ToModel()
	err = billingCodePrefix.Create()
	if err != nil {
		logging.Error("billingCodePrefix", "BillingCodePrefixCreate: error create", logging.Data{
			"error": err.Error(),
			"body":  req,
		})

		helpers.WriteResponse(r, w, helpers.ServerError("Failed to save billing code prefix data"))
		return
	}

	var rsp BillingCodePrefixCreateResponse
	rsp.FromModel(billingCodePrefix)

	helpers.WriteResponse(r, w, helpers.SuccessResponse(rsp))
}

func BillingCodePrefixGet(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	id := ps.ByName("id")

	var billingCodePrefix models.BillingCodePrefix
	err := billingCodePrefix.Get(id)
	if errors.Is(err, sql.ErrNoRows) {
		helpers.WriteResponse(r, w, helpers.ClientErrorResponse("Billing code prefix data not found"))
		return
	} else if err != nil {
		logging.Error("billingCodePrefix", "BillingCodePrefixGet: error get", logging.Data{
			"id":    id,
			"error": err.Error(),
		})

		helpers.WriteResponse(r, w, helpers.ServerError("Failed to retrieve billing code prefix data"))
		return
	}

	var rsp BillingCodePrefixGetResponse
	rsp.FromModel(billingCodePrefix)

	helpers.WriteResponse(r, w, helpers.SuccessResponse(rsp))
}

func BillingCodePrefixList(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	var pr models.PaginationRequest
	err := pr.FromRequest(r)
	if err != nil {
		helpers.WriteResponse(r, w, helpers.BadRequestResponse())
		return
	}

	var f models.BillingCodePrefixFilter
	f.FromRequest(r)

	var billingCodePrefix models.BillingCodePrefix
	billingCodePrefixes, total, err := billingCodePrefix.List(pr, &f)
	if err != nil {
		logging.Error("billingCodePrefix", "BillingCodePrefixList: error list", logging.Data{
			"error": err.Error(),
		})

		helpers.WriteResponse(r, w, helpers.ServerError("Failed to retrieve billing code prefix data"))
		return
	}

	var tmp BillingCodePrefixGetResponse
	rsp := tmp.FromModels(billingCodePrefixes)

	totalPage := math.Ceil(float64(total) / float64(pr.PerPage))
	helpers.WriteResponse(r, w, helpers.PaginatedSuccessResponse(rsp, int64(pr.Page), int64(pr.PerPage), total,
		int64(totalPage)))
}

func BillingCodePrefixUpdate(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	id := ps.ByName("id")

	var req BillingCodePrefixUpdateRequest
	err := json.NewDecoder(r.Body).Decode(&req)
	if err != nil {
		helpers.WriteResponse(r, w, helpers.BadRequestResponse())
		return
	}

	var billingCodePrefix models.BillingCodePrefix
	err = billingCodePrefix.Get(id)
	if errors.Is(err, sql.ErrNoRows) {
		helpers.WriteResponse(r, w, helpers.ClientErrorResponse("Billing code prefix data not found"))
		return
	} else if err != nil {
		logging.Error("billingCodePrefix", "BillingCodePrefixUpdate: error get", logging.Data{
			"id":    id,
			"error": err.Error(),
		})

		helpers.WriteResponse(r, w, helpers.ServerError("Failed to retrieve billing code prefix data"))
		return
	}

	req.FillModel(&billingCodePrefix)
	err = billingCodePrefix.Update()
	if err != nil {
		logging.Error("billingCodePrefix", "BillingCodePrefixUpdate: error update", logging.Data{
			"error": err.Error(),
			"body":  req,
		})

		helpers.WriteResponse(r, w, helpers.ServerError("Failed to save billing code prefix data"))
		return
	}

	var rsp BillingCodePrefixUpdateResponse
	rsp.FromModel(billingCodePrefix)
	helpers.WriteResponse(r, w, helpers.SuccessResponse(rsp))

}

func BillingCodePrefixDelete(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	id := ps.ByName("id")

	var billingCodePrefix models.BillingCodePrefix
	err := billingCodePrefix.Get(id)
	if errors.Is(err, sql.ErrNoRows) {
		helpers.WriteResponse(r, w, helpers.ClientErrorResponse("Billing code prefix data not found"))
		return
	} else if err != nil {
		logging.Error("billingCodePrefix", "BillingCodePrefixDelete: error get", logging.Data{
			"id":    id,
			"error": err.Error(),
		})

		helpers.WriteResponse(r, w, helpers.ServerError("Failed to retrieve billing code prefix data"))
		return
	}

	err = billingCodePrefix.Delete()
	if err != nil {
		logging.Error("billingCodePrefix", "BillingCodePrefixDelete: error delete", logging.Data{
			"error": err.Error(),
			"id":    id,
		})

		helpers.WriteResponse(r, w, helpers.ServerError("Failed to delete billing code prefix data"))
		return
	}

	helpers.WriteResponse(r, w, helpers.SuccessResponse(nil))
}

func (r *BillingCodePrefixCreateRequest) ToModel() models.BillingCodePrefix {
	return models.BillingCodePrefix{
		CreatedBy:    r.CreatedBy,
		BillerTypeId: r.BillerTypeId,
		BillerPrefix: r.BillerPrefix,
		Description:  r.Description,
	}
}

func (r *BillingCodePrefixUpdateRequest) FillModel(b *models.BillingCodePrefix) {
	b.BillerTypeId = r.BillerTypeId
	b.BillerPrefix = r.BillerPrefix
	b.Description = r.Description
	b.IsActive = r.IsActive
	b.UpdatedBy = &r.UpdatedBy
}

func (r *BillingCodePrefixCreateResponse) FromModel(b models.BillingCodePrefix) {
	r.Id = b.Id
	r.BillerTypeId = b.BillerTypeId
	r.BillerPrefix = b.BillerPrefix
	r.Description = b.Description
}

func (r *BillingCodePrefixGetResponse) FromModel(b models.BillingCodePrefix) {
	r.Id = b.Id
	r.BillerTypeId = b.BillerTypeId
	r.BillerPrefix = b.BillerPrefix
	r.Description = b.Description
	r.IsActive = b.IsActive
}

func (r *BillingCodePrefixGetResponse) FromModels(bs []models.Model) []BillingCodePrefixGetResponse {
	var rs []BillingCodePrefixGetResponse
	for _, m := range bs {
		var r BillingCodePrefixGetResponse

		billingCodePrefix := m.(*models.BillingCodePrefix)
		r.FromModel(*billingCodePrefix)
		rs = append(rs, r)
	}

	return rs
}

func (r *BillingCodePrefixUpdateResponse) FromModel(b models.BillingCodePrefix) {
	r.Id = b.Id
	r.BillerTypeId = b.BillerTypeId
	r.BillerPrefix = b.BillerPrefix
	r.Description = b.Description
	r.IsActive = b.IsActive
}
