package controllers

import (
	"database/sql"
	"dg-datamaster-service/helpers"
	"dg-datamaster-service/models"
	"dg-datamaster-service/services/logging"
	"encoding/json"
	"errors"
	"github.com/julienschmidt/httprouter"
	"math"
	"net/http"
)

type VillageCreateRequest struct {
	CreatedBy   string `json:"created_by"`
	DistrictId  int64  `json:"district_id"`
	VillageCode string `json:"village_code"`
	Postcode    string `json:"postcode"`
	VillageName string `json:"village_name"`
}

type VillageCreateResponse struct {
	Id          int64  `json:"id"`
	DistrictId  int64  `json:"district_id"`
	VillageCode string `json:"village_code"`
	Postcode    string `json:"postcode"`
	VillageName string `json:"village_name"`
}

type VillageUpdateRequest struct {
	DistrictId  int64  `json:"district_id"`
	VillageCode string `json:"village_code"`
	Postcode    string `json:"postcode"`
	VillageName string `json:"village_name"`
	IsActive    bool   `json:"is_active"`
	UpdatedBy   string `json:"updated_by"`
}

type VillageUpdateResponse struct {
	Id          int64  `json:"id"`
	DistrictId  int64  `json:"district_id"`
	VillageCode string `json:"village_code"`
	Postcode    string `json:"postcode"`
	VillageName string `json:"village_name"`
	IsActive    bool   `json:"is_active"`
}

type VillageGetResponse struct {
	Id          int64  `json:"id"`
	DistrictId  int64  `json:"district_id"`
	VillageCode string `json:"village_code"`
	Postcode    string `json:"postcode"`
	VillageName string `json:"village_name"`
	IsActive    bool   `json:"is_active"`
}

func VillageCreate(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	var req VillageCreateRequest
	err := json.NewDecoder(r.Body).Decode(&req)
	if err != nil {
		helpers.WriteResponse(r, w, helpers.BadRequestResponse())
		return
	}

	village := req.ToModel()
	err = village.Create()
	if err != nil {
		logging.Error("village", "VillageCreate: error create", logging.Data{
			"error": err.Error(),
			"body":  req,
		})

		helpers.WriteResponse(r, w, helpers.ServerError("Failed to save village data"))
		return
	}

	var rsp VillageCreateResponse
	rsp.FromModel(village)

	helpers.WriteResponse(r, w, helpers.SuccessResponse(rsp))
}

func VillageGet(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	id := ps.ByName("id")

	var village models.Village
	err := village.Get(id)
	if errors.Is(err, sql.ErrNoRows) {
		helpers.WriteResponse(r, w, helpers.ClientErrorResponse("Village data not found"))
		return
	} else if err != nil {
		logging.Error("village", "VillageGet: error get", logging.Data{
			"id":    id,
			"error": err.Error(),
		})

		helpers.WriteResponse(r, w, helpers.ServerError("Failed to retrieve village data"))
		return
	}

	var rsp VillageGetResponse
	rsp.FromModel(village)

	helpers.WriteResponse(r, w, helpers.SuccessResponse(rsp))
}

func VillageList(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	var pr models.PaginationRequest
	err := pr.FromRequest(r)
	if err != nil {
		helpers.WriteResponse(r, w, helpers.BadRequestResponse())
		return
	}

	var f models.VillageFilter
	f.FromRequest(r)

	var village models.Village
	villages, total, err := village.List(pr, &f)
	if err != nil {
		logging.Error("village", "VillageList: error list", logging.Data{
			"error": err.Error(),
		})

		helpers.WriteResponse(r, w, helpers.ServerError("Failed to retrieve village data"))
		return
	}

	var tmp VillageGetResponse
	rsp := tmp.FromModels(villages)

	totalPage := math.Ceil(float64(total) / float64(pr.PerPage))
	helpers.WriteResponse(r, w, helpers.PaginatedSuccessResponse(rsp, int64(pr.Page), int64(pr.PerPage), total,
		int64(totalPage)))
}

func VillageUpdate(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	id := ps.ByName("id")

	var req VillageUpdateRequest
	err := json.NewDecoder(r.Body).Decode(&req)
	if err != nil {
		helpers.WriteResponse(r, w, helpers.BadRequestResponse())
		return
	}

	var village models.Village
	err = village.Get(id)
	if errors.Is(err, sql.ErrNoRows) {
		helpers.WriteResponse(r, w, helpers.ClientErrorResponse("Village data not found"))
		return
	} else if err != nil {
		logging.Error("village", "VillageUpdate: error get", logging.Data{
			"id":    id,
			"error": err.Error(),
		})

		helpers.WriteResponse(r, w, helpers.ServerError("Failed to retrieve village data"))
		return
	}

	req.FillModel(&village)
	err = village.Update()
	if err != nil {
		logging.Error("village", "VillageUpdate: error update", logging.Data{
			"error": err.Error(),
			"body":  req,
		})

		helpers.WriteResponse(r, w, helpers.ServerError("Failed to save village data"))
		return
	}

	var rsp VillageUpdateResponse
	rsp.FromModel(village)
	helpers.WriteResponse(r, w, helpers.SuccessResponse(rsp))

}

func VillageDelete(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	id := ps.ByName("id")

	var village models.Village
	err := village.Get(id)
	if errors.Is(err, sql.ErrNoRows) {
		helpers.WriteResponse(r, w, helpers.ClientErrorResponse("Village data not found"))
		return
	} else if err != nil {
		logging.Error("village", "VillageDelete: error get", logging.Data{
			"id":    id,
			"error": err.Error(),
		})

		helpers.WriteResponse(r, w, helpers.ServerError("Failed to retrieve village data"))
		return
	}

	err = village.Delete()
	if err != nil {
		logging.Error("village", "VillageDelete: error delete", logging.Data{
			"error": err.Error(),
			"id":    id,
		})

		helpers.WriteResponse(r, w, helpers.ServerError("Failed to delete village data"))
		return
	}

	helpers.WriteResponse(r, w, helpers.SuccessResponse(nil))
}

func (r *VillageCreateRequest) ToModel() models.Village {
	return models.Village{
		CreatedBy:   r.CreatedBy,
		DistrictId:  r.DistrictId,
		VillageCode: r.VillageCode,
		Postcode:    r.Postcode,
		VillageName: r.VillageName,
	}
}

func (r *VillageUpdateRequest) FillModel(v *models.Village) {
	v.DistrictId = r.DistrictId
	v.VillageCode = r.VillageCode
	v.Postcode = r.Postcode
	v.VillageName = r.VillageName
	v.IsActive = r.IsActive
	v.UpdatedBy = &r.UpdatedBy
}

func (r *VillageCreateResponse) FromModel(v models.Village) {
	r.Id = v.Id
	r.DistrictId = v.DistrictId
	r.VillageCode = v.VillageCode
	r.Postcode = v.Postcode
	r.VillageName = v.VillageName
}

func (r *VillageGetResponse) FromModel(v models.Village) {
	r.Id = v.Id
	r.DistrictId = v.DistrictId
	r.VillageCode = v.VillageCode
	r.Postcode = v.Postcode
	r.VillageName = v.VillageName
	r.IsActive = v.IsActive
}

func (r *VillageGetResponse) FromModels(vs []models.Model) []VillageGetResponse {
	var rs []VillageGetResponse
	for _, m := range vs {
		var r VillageGetResponse

		village := m.(*models.Village)
		r.FromModel(*village)
		rs = append(rs, r)
	}

	return rs
}

func (r *VillageUpdateResponse) FromModel(v models.Village) {
	r.Id = v.Id
	r.DistrictId = v.DistrictId
	r.VillageCode = v.VillageCode
	r.Postcode = v.Postcode
	r.VillageName = v.VillageName
	r.IsActive = v.IsActive
}
