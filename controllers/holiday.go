package controllers

import (
	"database/sql"
	"dg-datamaster-service/helpers"
	"dg-datamaster-service/models"
	"dg-datamaster-service/services/logging"
	"encoding/json"
	"errors"
	"github.com/julienschmidt/httprouter"
	"math"
	"net/http"
)

type HolidayCreateRequest struct {
	CreatedBy          string `json:"created_by"`
	HolidayDate        string `json:"holiday_date"`
	HolidayDescription string `json:"holiday_description"`
}

type HolidayCreateResponse struct {
	Id                 int64  `json:"id"`
	HolidayDate        string `json:"holiday_date"`
	HolidayDescription string `json:"holiday_description"`
}

type HolidayUpdateRequest struct {
	HolidayDate        string `json:"holiday_date"`
	HolidayDescription string `json:"holiday_description"`
	Active             bool   `json:"is_active"`
	UpdatedBy          string `json:"updated_by"`
}

type HolidayUpdateResponse struct {
	Id                 int64  `json:"id"`
	HolidayDate        string `json:"holiday_date"`
	HolidayDescription string `json:"holiday_description"`
	Active             bool   `json:"is_active"`
	UpdatedBy          string `json:"updated_by"`
}

type HolidayGetResponse struct {
	Id                 int64  `json:"id"`
	HolidayDate        string `json:"holiday_date"`
	HolidayDescription string `json:"holiday_description"`
	Active             bool   `json:"is_active"`
}

func HolidayCreate(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	var req HolidayCreateRequest
	err := json.NewDecoder(r.Body).Decode(&req)
	if err != nil {
		helpers.WriteResponse(r, w, helpers.BadRequestResponse())
		return
	}

	holiday := req.ToModel()
	err = holiday.Create()
	if err != nil {
		logging.Error("holiday", "HolidayCreate: error create", logging.Data{
			"error": err.Error(),
			"body":  req,
		})

		helpers.WriteResponse(r, w, helpers.ServerError("Failed to save holiday data"))
		return
	}

	var rsp HolidayCreateResponse
	rsp.FromModel(holiday)

	helpers.WriteResponse(r, w, helpers.SuccessResponse(rsp))
}

func HolidayGet(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	id := ps.ByName("id")

	var holiday models.Holiday
	err := holiday.Get(id)
	if errors.Is(err, sql.ErrNoRows) {
		helpers.WriteResponse(r, w, helpers.ClientErrorResponse("Holiday data not found"))
		return
	} else if err != nil {
		logging.Error("holiday", "HolidayGet: error get", logging.Data{
			"id":    id,
			"error": err.Error(),
		})

		helpers.WriteResponse(r, w, helpers.ServerError("Failed to retrieve holiday data"))
		return
	}

	var rsp HolidayGetResponse
	rsp.FromModel(holiday)

	helpers.WriteResponse(r, w, helpers.SuccessResponse(rsp))
}

func HolidayList(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	var pr models.PaginationRequest
	err := pr.FromRequest(r)
	if err != nil {
		helpers.WriteResponse(r, w, helpers.BadRequestResponse())
		return
	}

	var f models.HolidayFilter
	f.FromRequest(r)

	var holiday models.Holiday
	holidays, total, err := holiday.List(pr, &f)
	if err != nil {
		logging.Error("holiday", "HolidayList: error list", logging.Data{
			"error": err.Error(),
		})

		helpers.WriteResponse(r, w, helpers.ServerError("Failed to retrieve holiday data"))
		return
	}

	var tmp HolidayGetResponse
	rsp := tmp.FromModels(holidays)

	totalPage := math.Ceil(float64(total) / float64(pr.PerPage))
	helpers.WriteResponse(r, w, helpers.PaginatedSuccessResponse(rsp, int64(pr.Page), int64(pr.PerPage), total,
		int64(totalPage)))
}

func HolidayUpdate(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	id := ps.ByName("id")

	var req HolidayUpdateRequest
	err := json.NewDecoder(r.Body).Decode(&req)
	if err != nil {
		helpers.WriteResponse(r, w, helpers.BadRequestResponse())
		return
	}

	var holiday models.Holiday
	err = holiday.Get(id)
	if errors.Is(err, sql.ErrNoRows) {
		helpers.WriteResponse(r, w, helpers.ClientErrorResponse("Holiday data not found"))
		return
	} else if err != nil {
		logging.Error("holiday", "HolidayUpdate: error get", logging.Data{
			"id":    id,
			"error": err.Error(),
		})

		helpers.WriteResponse(r, w, helpers.ServerError("Failed to retrieve holiday data"))
		return
	}

	req.FillModel(&holiday)
	err = holiday.Update()
	if err != nil {
		logging.Error("holiday", "HolidayUpdate: error update", logging.Data{
			"error": err.Error(),
			"body":  req,
		})

		helpers.WriteResponse(r, w, helpers.ServerError("Failed to save holiday data"))
		return
	}

	var rsp HolidayUpdateResponse
	rsp.FromModel(holiday)
	helpers.WriteResponse(r, w, helpers.SuccessResponse(rsp))

}

func HolidayDelete(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	id := ps.ByName("id")

	var holiday models.Holiday
	err := holiday.Get(id)
	if errors.Is(err, sql.ErrNoRows) {
		helpers.WriteResponse(r, w, helpers.ClientErrorResponse("Holiday data not found"))
		return
	} else if err != nil {
		logging.Error("holiday", "HolidayDelete: error get", logging.Data{
			"id":    id,
			"error": err.Error(),
		})

		helpers.WriteResponse(r, w, helpers.ServerError("Failed to retrieve holiday data"))
		return
	}

	err = holiday.Delete()
	if err != nil {
		logging.Error("holiday", "HolidayDelete: error delete", logging.Data{
			"error": err.Error(),
			"id":    id,
		})

		helpers.WriteResponse(r, w, helpers.ServerError("Failed to delete holiday data"))
		return
	}

	helpers.WriteResponse(r, w, helpers.SuccessResponse(nil))
}

func (r *HolidayCreateRequest) ToModel() models.Holiday {
	return models.Holiday{
		CreatedBy:          r.CreatedBy,
		HolidayDate:        r.HolidayDate,
		HolidayDescription: r.HolidayDescription,
	}
}

func (r *HolidayUpdateRequest) FillModel(h *models.Holiday) {
	h.HolidayDate = r.HolidayDate
	h.HolidayDescription = r.HolidayDescription
	h.Active = r.Active
	h.UpdatedBy = &r.UpdatedBy
}

func (r *HolidayCreateResponse) FromModel(h models.Holiday) {
	r.Id = h.Id
	r.HolidayDate = h.HolidayDate
	r.HolidayDescription = h.HolidayDescription
}

func (r *HolidayGetResponse) FromModel(h models.Holiday) {
	r.Id = h.Id
	r.HolidayDate = h.HolidayDate
	r.HolidayDescription = h.HolidayDescription
	r.Active = h.Active
}

func (r *HolidayGetResponse) FromModels(hs []models.Model) []HolidayGetResponse {
	var rs []HolidayGetResponse
	for _, m := range hs {
		var r HolidayGetResponse

		holiday := m.(*models.Holiday)
		r.FromModel(*holiday)
		rs = append(rs, r)
	}

	return rs
}

func (r *HolidayUpdateResponse) FromModel(h models.Holiday) {
	r.Id = h.Id
	r.HolidayDate = h.HolidayDate
	r.HolidayDescription = h.HolidayDescription
	r.Active = h.Active
	r.UpdatedBy = *h.UpdatedBy
}
