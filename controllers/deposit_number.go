package controllers

import (
	"database/sql"
	"dg-datamaster-service/helpers"
	"dg-datamaster-service/models"
	"dg-datamaster-service/services/logging"
	"encoding/json"
	"errors"
	"math"
	"net/http"

	"github.com/julienschmidt/httprouter"
)

type DepositNumberCreateRequest struct {
	CreatedBy      string `json:"created_by"`
	DepositTypeId  int64  `json:"deposit_type_id"`
	BranchOfficeId int64  `json:"branch_office_id"`
	DepositNo      string `json:"deposit_no"`
}

type DepositNumberCreateResponse struct {
	Id             int64  `json:"id"`
	DepositTypeId  int64  `json:"deposit_type_id"`
	BranchOfficeId int64  `json:"branch_office_id"`
	DepositNo      string `json:"deposit_no"`
	IsUsed         bool   `json:"is_used"`
}

type DepositNumberUpdateRequest struct {
	DepositTypeId  int64  `json:"deposit_type_id"`
	BranchOfficeId int64  `json:"branch_office_id"`
	DepositNo      string `json:"deposit_no"`
	IsUsed         bool   `json:"is_used"`
	UpdatedBy      string `json:"updated_by"`
}

type DepositNumberUpdateResponse struct {
	Id             int64  `json:"id"`
	DepositTypeId  int64  `json:"deposit_type_id"`
	BranchOfficeId int64  `json:"branch_office_id"`
	DepositNo      string `json:"deposit_no"`
	IsUsed         bool   `json:"is_used"`
}

type DepositNumberGetResponse struct {
	Id             int64  `json:"id"`
	DepositTypeId  int64  `json:"deposit_type_id"`
	BranchOfficeId int64  `json:"branch_office_id"`
	DepositNo      string `json:"deposit_no"`
	IsUsed         bool   `json:"is_used"`
}

func DepositNumberCreate(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	var req DepositNumberCreateRequest
	err := json.NewDecoder(r.Body).Decode(&req)
	if err != nil {
		helpers.WriteResponse(r, w, helpers.BadRequestResponse())
		return
	}

	depositNumber := req.ToModel()
	err = depositNumber.Create()
	if err != nil {
		logging.Error("deposit_number", "DepositNumberCreate: error create", logging.Data{
			"error": err.Error(),
			"body":  req,
		})

		helpers.WriteResponse(r, w, helpers.ServerError("Failed to save deposit number data"))
		return
	}

	var rsp DepositNumberCreateResponse
	rsp.FromModel(depositNumber)

	helpers.WriteResponse(r, w, helpers.SuccessResponse(rsp))
}

func DepositNumberGet(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	id := ps.ByName("id")

	var depositNumber models.DepositNumber
	err := depositNumber.Get(id)
	if errors.Is(err, sql.ErrNoRows) {
		helpers.WriteResponse(r, w, helpers.ClientErrorResponse("Deposit number data not found"))
		return
	} else if err != nil {
		logging.Error("deposit_number", "DepositNumberGet: error get", logging.Data{
			"id":    id,
			"error": err.Error(),
		})

		helpers.WriteResponse(r, w, helpers.ServerError("Failed to retrieve deposit number data"))
		return
	}

	var rsp DepositNumberGetResponse
	rsp.FromModel(depositNumber)

	helpers.WriteResponse(r, w, helpers.SuccessResponse(rsp))
}

func DepositNumberList(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	var pr models.PaginationRequest
	err := pr.FromRequest(r)
	if err != nil {
		helpers.WriteResponse(r, w, helpers.BadRequestResponse())
		return
	}

	var f models.DepositNumberFilter
	f.FromRequest(r)

	var depositNumber models.DepositNumber
	depositNumbers, total, err := depositNumber.List(pr, &f)
	if err != nil {
		logging.Error("deposit_number", "DepositNumberList: error list", logging.Data{
			"error": err.Error(),
		})

		helpers.WriteResponse(r, w, helpers.ServerError("Failed to retrieve deposit number data"))
		return
	}

	var tmp DepositNumberGetResponse
	rsp := tmp.FromModels(depositNumbers)

	totalPage := math.Ceil(float64(total) / float64(pr.PerPage))
	helpers.WriteResponse(r, w, helpers.PaginatedSuccessResponse(rsp, int64(pr.Page), int64(pr.PerPage), total,
		int64(totalPage)))
}

func DepositNumberUpdate(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	id := ps.ByName("id")

	var req DepositNumberUpdateRequest
	err := json.NewDecoder(r.Body).Decode(&req)
	if err != nil {
		helpers.WriteResponse(r, w, helpers.BadRequestResponse())
		return
	}

	var depositNumber models.DepositNumber
	err = depositNumber.Get(id)
	if errors.Is(err, sql.ErrNoRows) {
		helpers.WriteResponse(r, w, helpers.ClientErrorResponse("Deposit number data not found"))
		return
	} else if err != nil {
		logging.Error("deposit_number", "DepositNumberUpdate: error get", logging.Data{
			"id":    id,
			"error": err.Error(),
		})

		helpers.WriteResponse(r, w, helpers.ServerError("Failed to retrieve deposit number data"))
		return
	}

	req.FillModel(&depositNumber)
	err = depositNumber.Update()
	if err != nil {
		logging.Error("deposit_number", "DepositNumberUpdate: error update", logging.Data{
			"error": err.Error(),
			"body":  req,
		})

		helpers.WriteResponse(r, w, helpers.ServerError("Failed to save deposit number data"))
		return
	}

	var rsp DepositNumberUpdateResponse
	rsp.FromModel(depositNumber)
	helpers.WriteResponse(r, w, helpers.SuccessResponse(rsp))

}

func DepositNumberDelete(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	id := ps.ByName("id")

	var depositNumber models.DepositNumber
	err := depositNumber.Get(id)
	if errors.Is(err, sql.ErrNoRows) {
		helpers.WriteResponse(r, w, helpers.ClientErrorResponse("Deposit number data not found"))
		return
	} else if err != nil {
		logging.Error("deposit_number", "DepositNumberDelete: error get", logging.Data{
			"id":    id,
			"error": err.Error(),
		})

		helpers.WriteResponse(r, w, helpers.ServerError("Failed to retrieve deposit number data"))
		return
	}

	err = depositNumber.Delete()
	if err != nil {
		logging.Error("deposit_number", "DepositNumberDelete: error delete", logging.Data{
			"error": err.Error(),
			"id":    id,
		})

		helpers.WriteResponse(r, w, helpers.ServerError("Failed to delete deposit number data"))
		return
	}

	helpers.WriteResponse(r, w, helpers.SuccessResponse(nil))
}

func (r *DepositNumberCreateRequest) ToModel() models.DepositNumber {
	return models.DepositNumber{
		CreatedBy:      r.CreatedBy,
		DepositTypeId:  r.DepositTypeId,
		BranchOfficeId: r.BranchOfficeId,
		DepositNo:      r.DepositNo,
	}
}

func (r *DepositNumberUpdateRequest) FillModel(d *models.DepositNumber) {
	d.DepositTypeId = r.DepositTypeId
	d.BranchOfficeId = r.BranchOfficeId
	d.DepositNo = r.DepositNo
	d.IsUsed = r.IsUsed
	d.UpdatedBy = &r.UpdatedBy
}

func (r *DepositNumberCreateResponse) FromModel(d models.DepositNumber) {
	r.Id = d.Id
	r.DepositTypeId = d.DepositTypeId
	r.BranchOfficeId = d.BranchOfficeId
	r.DepositNo = d.DepositNo
	r.IsUsed = d.IsUsed
}

func (r *DepositNumberGetResponse) FromModel(d models.DepositNumber) {
	r.Id = d.Id
	r.DepositTypeId = d.DepositTypeId
	r.BranchOfficeId = d.BranchOfficeId
	r.DepositNo = d.DepositNo
	r.IsUsed = d.IsUsed
}

func (r *DepositNumberGetResponse) FromModels(ds []models.Model) []DepositNumberGetResponse {
	var rs []DepositNumberGetResponse
	for _, m := range ds {
		var r DepositNumberGetResponse

		depositNumber := m.(*models.DepositNumber)
		r.FromModel(*depositNumber)
		rs = append(rs, r)
	}

	return rs
}

func (r *DepositNumberUpdateResponse) FromModel(d models.DepositNumber) {
	r.Id = d.Id
	r.DepositTypeId = d.DepositTypeId
	r.BranchOfficeId = d.BranchOfficeId
	r.DepositNo = d.DepositNo
	r.IsUsed = d.IsUsed
}
