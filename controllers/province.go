package controllers

import (
	"database/sql"
	"dg-datamaster-service/helpers"
	"dg-datamaster-service/models"
	"dg-datamaster-service/services/logging"
	"encoding/json"
	"errors"
	"github.com/julienschmidt/httprouter"
	"math"
	"net/http"
)

type ProvinceCreateRequest struct {
	CreatedBy    string `json:"created_by"`
	ProvinceCode string `json:"province_code"`
	ProvinceName string `json:"province_name"`
}

type ProvinceCreateResponse struct {
	Id           int64  `json:"id"`
	ProvinceCode string `json:"province_code"`
	ProvinceName string `json:"province_name"`
}

type ProvinceUpdateRequest struct {
	ProvinceCode string `json:"province_code"`
	ProvinceName string `json:"province_name"`
	Active       bool   `json:"is_active"`
	UpdatedBy    string `json:"updated_by"`
}

type ProvinceUpdateResponse struct {
	Id           int64  `json:"id"`
	ProvinceCode string `json:"province_code"`
	ProvinceName string `json:"province_name"`
	Active       bool   `json:"is_active"`
	UpdatedBy    string `json:"updated_by"`
}

type ProvinceGetResponse struct {
	Id           int64  `json:"id"`
	ProvinceCode string `json:"province_code"`
	ProvinceName string `json:"province_name"`
	Active       bool   `json:"is_active"`
}

func ProvinceCreate(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	var req ProvinceCreateRequest
	err := json.NewDecoder(r.Body).Decode(&req)
	if err != nil {
		helpers.WriteResponse(r, w, helpers.BadRequestResponse())
		return
	}

	province := req.ToModel()
	err = province.Create()
	if err != nil {
		logging.Error("province", "ProvinceCreate: error create", logging.Data{
			"error": err.Error(),
			"body":  req,
		})

		helpers.WriteResponse(r, w, helpers.ServerError("Failed to save province data"))
		return
	}

	var rsp ProvinceCreateResponse
	rsp.FromModel(province)

	helpers.WriteResponse(r, w, helpers.SuccessResponse(rsp))
}

func ProvinceGet(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	id := ps.ByName("id")

	var province models.Province
	err := province.Get(id)
	if errors.Is(err, sql.ErrNoRows) {
		helpers.WriteResponse(r, w, helpers.ClientErrorResponse("Province data not found"))
		return
	} else if err != nil {
		logging.Error("province", "ProvinceGet: error get", logging.Data{
			"id":    id,
			"error": err.Error(),
		})

		helpers.WriteResponse(r, w, helpers.ServerError("Failed to retrieve province data"))
		return
	}

	var rsp ProvinceGetResponse
	rsp.FromModel(province)

	helpers.WriteResponse(r, w, helpers.SuccessResponse(rsp))
}

func ProvinceList(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	var pr models.PaginationRequest
	err := pr.FromRequest(r)
	if err != nil {
		helpers.WriteResponse(r, w, helpers.BadRequestResponse())
		return
	}

	var f models.ProvinceFilter
	f.FromRequest(r)

	var province models.Province
	provinces, total, err := province.List(pr, &f)
	if err != nil {
		logging.Error("province", "ProvinceList: error list", logging.Data{
			"error": err.Error(),
		})

		helpers.WriteResponse(r, w, helpers.ServerError("Failed to retrieve province data"))
		return
	}

	var tmp ProvinceGetResponse
	rsp := tmp.FromModels(provinces)

	totalPage := math.Ceil(float64(total) / float64(pr.PerPage))
	helpers.WriteResponse(r, w, helpers.PaginatedSuccessResponse(rsp, int64(pr.Page), int64(pr.PerPage), total,
		int64(totalPage)))
}

func ProvinceUpdate(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	id := ps.ByName("id")

	var req ProvinceUpdateRequest
	err := json.NewDecoder(r.Body).Decode(&req)
	if err != nil {
		helpers.WriteResponse(r, w, helpers.BadRequestResponse())
		return
	}

	var province models.Province
	err = province.Get(id)
	if errors.Is(err, sql.ErrNoRows) {
		helpers.WriteResponse(r, w, helpers.ClientErrorResponse("Province data not found"))
		return
	} else if err != nil {
		logging.Error("province", "ProvinceUpdate: error get", logging.Data{
			"id":    id,
			"error": err.Error(),
		})

		helpers.WriteResponse(r, w, helpers.ServerError("Failed to retrieve province data"))
		return
	}

	req.FillModel(&province)
	err = province.Update()
	if err != nil {
		logging.Error("province", "ProvinceUpdate: error update", logging.Data{
			"error": err.Error(),
			"body":  req,
		})

		helpers.WriteResponse(r, w, helpers.ServerError("Failed to save province data"))
		return
	}

	var rsp ProvinceUpdateResponse
	rsp.FromModel(province)
	helpers.WriteResponse(r, w, helpers.SuccessResponse(rsp))

}

func ProvinceDelete(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	id := ps.ByName("id")

	var province models.Province
	err := province.Get(id)
	if errors.Is(err, sql.ErrNoRows) {
		helpers.WriteResponse(r, w, helpers.ClientErrorResponse("Province data not found"))
		return
	} else if err != nil {
		logging.Error("province", "ProvinceDelete: error get", logging.Data{
			"id":    id,
			"error": err.Error(),
		})

		helpers.WriteResponse(r, w, helpers.ServerError("Failed to retrieve province data"))
		return
	}

	err = province.Delete()
	if err != nil {
		logging.Error("province", "ProvinceDelete: error delete", logging.Data{
			"error": err.Error(),
			"id":    id,
		})

		helpers.WriteResponse(r, w, helpers.ServerError("Failed to delete province data"))
		return
	}

	helpers.WriteResponse(r, w, helpers.SuccessResponse(nil))
}

func (r *ProvinceCreateRequest) ToModel() models.Province {
	return models.Province{
		CreatedBy:    r.CreatedBy,
		ProvinceCode: r.ProvinceCode,
		ProvinceName: r.ProvinceName,
	}
}

func (r *ProvinceUpdateRequest) FillModel(p *models.Province) {
	p.ProvinceCode = r.ProvinceCode
	p.ProvinceName = r.ProvinceName
	p.Active = r.Active
	p.UpdatedBy = &r.UpdatedBy
}

func (r *ProvinceCreateResponse) FromModel(p models.Province) {
	r.Id = p.Id
	r.ProvinceCode = p.ProvinceCode
	r.ProvinceName = p.ProvinceName
}

func (r *ProvinceGetResponse) FromModel(p models.Province) {
	r.Id = p.Id
	r.ProvinceCode = p.ProvinceCode
	r.ProvinceName = p.ProvinceName
	r.Active = p.Active
}

func (r *ProvinceGetResponse) FromModels(ps []models.Model) []ProvinceGetResponse {
	var rs []ProvinceGetResponse
	for _, m := range ps {
		var r ProvinceGetResponse

		province := m.(*models.Province)
		r.FromModel(*province)
		rs = append(rs, r)
	}

	return rs
}

func (r *ProvinceUpdateResponse) FromModel(p models.Province) {
	r.Id = p.Id
	r.ProvinceCode = p.ProvinceCode
	r.ProvinceName = p.ProvinceName
	r.Active = p.Active
	r.UpdatedBy = *p.UpdatedBy
}
