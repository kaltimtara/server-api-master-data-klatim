package controllers

import (
	"database/sql"
	"dg-datamaster-service/helpers"
	"dg-datamaster-service/models"
	"dg-datamaster-service/services/logging"
	"encoding/json"
	"errors"
	"github.com/julienschmidt/httprouter"
	"math"
	"net/http"
)

type TransactionCutoffCreateRequest struct {
	CreatedBy       string `json:"created_by"`
	TrxTypeId       int64  `json:"trx_type_id"`
	StartTime       string `json:"start_time"`
	EndTime         string `json:"end_time"`
	BusinessDayOnly bool   `json:"is_business_day_only"`
}

type TransactionCutoffCreateResponse struct {
	Id              int64  `json:"id"`
	TrxTypeId       int64  `json:"trx_type_id"`
	StartTime       string `json:"start_time"`
	EndTime         string `json:"end_time"`
	BusinessDayOnly bool   `json:"is_business_day_only"`
}

type TransactionCutoffUpdateRequest struct {
	TrxTypeId       int64  `json:"trx_type_id"`
	StartTime       string `json:"start_time"`
	EndTime         string `json:"end_time"`
	BusinessDayOnly bool   `json:"is_business_day_only"`
	Active          bool   `json:"is_active"`
	UpdatedBy       string `json:"updated_by"`
}

type TransactionCutoffUpdateResponse struct {
	Id              int64  `json:"id"`
	TrxTypeId       int64  `json:"trx_type_id"`
	StartTime       string `json:"start_time"`
	EndTime         string `json:"end_time"`
	BusinessDayOnly bool   `json:"is_business_day_only"`
	Active          bool   `json:"is_active"`
	UpdatedBy       string `json:"updated_by"`
}

type TransactionCutoffGetResponse struct {
	Id              int64  `json:"id"`
	TrxTypeId       int64  `json:"trx_type_id"`
	StartTime       string `json:"start_time"`
	EndTime         string `json:"end_time"`
	BusinessDayOnly bool   `json:"is_business_day_only"`
	Active          bool   `json:"is_active"`
}

func TransactionCutoffCreate(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	var req TransactionCutoffCreateRequest
	err := json.NewDecoder(r.Body).Decode(&req)
	if err != nil {
		helpers.WriteResponse(r, w, helpers.BadRequestResponse())
		return
	}

	transactionCutoff := req.ToModel()
	err = transactionCutoff.Create()
	if err != nil {
		logging.Error("transaction_cutoff", "TransactionCutoffCreate: error create", logging.Data{
			"error": err.Error(),
			"body":  req,
		})

		helpers.WriteResponse(r, w, helpers.ServerError("Failed to save transaction cutoff data"))
		return
	}

	var rsp TransactionCutoffCreateResponse
	rsp.FromModel(transactionCutoff)

	helpers.WriteResponse(r, w, helpers.SuccessResponse(rsp))
}

func TransactionCutoffGet(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	id := ps.ByName("id")

	var transactionCutoff models.TransactionCutoff
	err := transactionCutoff.Get(id)
	if errors.Is(err, sql.ErrNoRows) {
		helpers.WriteResponse(r, w, helpers.ClientErrorResponse("Transaction cutoff data not found"))
		return
	} else if err != nil {
		logging.Error("transaction_cutoff", "TransactionCutoffGet: error get", logging.Data{
			"id":    id,
			"error": err.Error(),
		})

		helpers.WriteResponse(r, w, helpers.ServerError("Failed to retrieve transaction cutoff data"))
		return
	}

	var rsp TransactionCutoffGetResponse
	rsp.FromModel(transactionCutoff)

	helpers.WriteResponse(r, w, helpers.SuccessResponse(rsp))
}

func TransactionCutoffList(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	var pr models.PaginationRequest
	err := pr.FromRequest(r)
	if err != nil {
		helpers.WriteResponse(r, w, helpers.BadRequestResponse())
		return
	}

	var f models.TransactionCutoffFilter
	f.FromRequest(r)

	var transactionCutoff models.TransactionCutoff
	transactionCutoffs, total, err := transactionCutoff.List(pr, &f)
	if err != nil {
		logging.Error("transaction_cutoff", "TransactionCutoffList: error list", logging.Data{
			"error": err.Error(),
		})

		helpers.WriteResponse(r, w, helpers.ServerError("Failed to retrieve transaction cutoff data"))
		return
	}

	var tmp TransactionCutoffGetResponse
	rsp := tmp.FromModels(transactionCutoffs)

	totalPage := math.Ceil(float64(total) / float64(pr.PerPage))
	helpers.WriteResponse(r, w, helpers.PaginatedSuccessResponse(rsp, int64(pr.Page), int64(pr.PerPage), total,
		int64(totalPage)))
}

func TransactionCutoffUpdate(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	id := ps.ByName("id")

	var req TransactionCutoffUpdateRequest
	err := json.NewDecoder(r.Body).Decode(&req)
	if err != nil {
		helpers.WriteResponse(r, w, helpers.BadRequestResponse())
		return
	}

	var transactionCutoff models.TransactionCutoff
	err = transactionCutoff.Get(id)
	if errors.Is(err, sql.ErrNoRows) {
		helpers.WriteResponse(r, w, helpers.ClientErrorResponse("Transaction cutoff data not found"))
		return
	} else if err != nil {
		logging.Error("transaction_cutoff", "TransactionCutoffUpdate: error get", logging.Data{
			"id":    id,
			"error": err.Error(),
		})

		helpers.WriteResponse(r, w, helpers.ServerError("Failed to retrieve transaction cutoff data"))
		return
	}

	req.FillModel(&transactionCutoff)
	err = transactionCutoff.Update()
	if err != nil {
		logging.Error("transaction_cutoff", "TransactionCutoffUpdate: error update", logging.Data{
			"error": err.Error(),
			"body":  req,
		})

		helpers.WriteResponse(r, w, helpers.ServerError("Failed to save transaction cutoff data"))
		return
	}

	var rsp TransactionCutoffUpdateResponse
	rsp.FromModel(transactionCutoff)
	helpers.WriteResponse(r, w, helpers.SuccessResponse(rsp))

}

func TransactionCutoffDelete(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	id := ps.ByName("id")

	var transactionCutoff models.TransactionCutoff
	err := transactionCutoff.Get(id)
	if errors.Is(err, sql.ErrNoRows) {
		helpers.WriteResponse(r, w, helpers.ClientErrorResponse("Transaction cutoff data not found"))
		return
	} else if err != nil {
		logging.Error("transaction_cutoff", "TransactionCutoffDelete: error get", logging.Data{
			"id":    id,
			"error": err.Error(),
		})

		helpers.WriteResponse(r, w, helpers.ServerError("Failed to retrieve transaction cutoff data"))
		return
	}

	err = transactionCutoff.Delete()
	if err != nil {
		logging.Error("transaction_cutoff", "TransactionCutoffDelete: error delete", logging.Data{
			"error": err.Error(),
			"id":    id,
		})

		helpers.WriteResponse(r, w, helpers.ServerError("Failed to delete transaction cutoff data"))
		return
	}

	helpers.WriteResponse(r, w, helpers.SuccessResponse(nil))
}

func (r *TransactionCutoffCreateRequest) ToModel() models.TransactionCutoff {
	return models.TransactionCutoff{
		CreatedBy:       r.CreatedBy,
		TrxTypeId:       r.TrxTypeId,
		StartTime:       r.StartTime,
		EndTime:         r.EndTime,
		BusinessDayOnly: r.BusinessDayOnly,
	}
}

func (r *TransactionCutoffUpdateRequest) FillModel(b *models.TransactionCutoff) {
	b.TrxTypeId = r.TrxTypeId
	b.StartTime = r.StartTime
	b.EndTime = r.EndTime
	b.BusinessDayOnly = r.BusinessDayOnly
	b.Active = r.Active
	b.UpdatedBy = &r.UpdatedBy
}

func (r *TransactionCutoffCreateResponse) FromModel(b models.TransactionCutoff) {
	r.Id = b.Id
	r.TrxTypeId = b.TrxTypeId
	r.StartTime = b.StartTime
	r.EndTime = b.EndTime
	r.BusinessDayOnly = b.BusinessDayOnly
}

func (r *TransactionCutoffGetResponse) FromModel(b models.TransactionCutoff) {
	r.Id = b.Id
	r.TrxTypeId = b.TrxTypeId
	r.StartTime = b.StartTime
	r.EndTime = b.EndTime
	r.BusinessDayOnly = b.BusinessDayOnly
	r.Active = b.Active
}

func (r *TransactionCutoffGetResponse) FromModels(ms []models.Model) []TransactionCutoffGetResponse {
	var rs []TransactionCutoffGetResponse
	for _, m := range ms {
		var r TransactionCutoffGetResponse

		transactionCutoff := m.(*models.TransactionCutoff)
		r.FromModel(*transactionCutoff)
		rs = append(rs, r)
	}

	return rs
}

func (r *TransactionCutoffUpdateResponse) FromModel(b models.TransactionCutoff) {
	r.Id = b.Id
	r.TrxTypeId = b.TrxTypeId
	r.StartTime = b.StartTime
	r.EndTime = b.EndTime
	r.BusinessDayOnly = b.BusinessDayOnly
	r.Active = b.Active
	r.UpdatedBy = *b.UpdatedBy
}
