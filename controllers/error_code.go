package controllers

import (
	"database/sql"
	"dg-datamaster-service/helpers"
	"dg-datamaster-service/models"
	"dg-datamaster-service/services/logging"
	"encoding/json"
	"errors"
	"math"
	"net/http"

	"github.com/julienschmidt/httprouter"
)

type ErrorCodeCreateRequest struct {
	CreatedBy        string `json:"created_by"`
	TrxTypeId        int    `json:"trx_type_id"`
	ErrorCode        string `json:"error_code"`
	ErrorDescription string `json:"error_description"`
}

type ErrorCodeCreateResponse struct {
	Id               int64  `json:"id"`
	TrxTypeId        int    `json:"trx_type_id"`
	ErrorCode        string `json:"error_code"`
	ErrorDescription string `json:"error_description"`
}

type ErrorCodeUpdateRequest struct {
	TrxTypeId        int    `json:"trx_type_id"`
	ErrorCode        string `json:"error_code"`
	ErrorDescription string `json:"error_description"`
	Active           bool   `json:"is_active"`
	UpdatedBy        string `json:"updated_by"`
}

type ErrorCodeUpdateResponse struct {
	Id               int64  `json:"id"`
	TrxTypeId        int    `json:"trx_type_id"`
	ErrorCode        string `json:"error_code"`
	ErrorDescription string `json:"error_description"`
	Active           bool   `json:"is_active"`
	UpdatedBy        string `json:"updated_by"`
}

type ErrorCodeGetResponse struct {
	Id               int64  `json:"id"`
	TrxTypeId        int    `json:"trx_type_id"`
	ErrorCode        string `json:"error_code"`
	ErrorDescription string `json:"error_description"`
	Active           bool   `json:"is_active"`
}

func ErrorCodeCreate(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	var req ErrorCodeCreateRequest
	err := json.NewDecoder(r.Body).Decode(&req)
	if err != nil {
		helpers.WriteResponse(r, w, helpers.BadRequestResponse())
		return
	}

	errorCode := req.ToModel()
	err = errorCode.Create()
	if err != nil {
		logging.Error("error_code", "ErrorCodeCreate: error create", logging.Data{
			"error": err.Error(),
			"body":  req,
		})

		helpers.WriteResponse(r, w, helpers.ServerError("Failed to save error code data"))
		return
	}

	var rsp ErrorCodeCreateResponse
	rsp.FromModel(errorCode)

	helpers.WriteResponse(r, w, helpers.SuccessResponse(rsp))
}

func ErrorCodeGet(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	id := ps.ByName("id")

	var errorCode models.ErrorCode
	err := errorCode.Get(id)
	if errors.Is(err, sql.ErrNoRows) {
		helpers.WriteResponse(r, w, helpers.ClientErrorResponse("Error code data not found"))
		return
	} else if err != nil {
		logging.Error("error_code", "ErrorCodeGet: error get", logging.Data{
			"id":    id,
			"error": err.Error(),
		})

		helpers.WriteResponse(r, w, helpers.ServerError("Failed to retrieve error code data"))
		return
	}

	var rsp ErrorCodeGetResponse
	rsp.FromModel(errorCode)

	helpers.WriteResponse(r, w, helpers.SuccessResponse(rsp))
}

func ErrorCodeList(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	var pr models.PaginationRequest
	err := pr.FromRequest(r)
	if err != nil {
		helpers.WriteResponse(r, w, helpers.BadRequestResponse())
		return
	}

	var f models.ErrorCodeFilter
	f.FromRequest(r)

	var errorCode models.ErrorCode
	errorCodes, total, err := errorCode.List(pr, &f)
	if err != nil {
		logging.Error("error_code", "ErrorCodeList: error list", logging.Data{
			"error": err.Error(),
		})

		helpers.WriteResponse(r, w, helpers.ServerError("Failed to retrieve error code data"))
		return
	}

	var tmp ErrorCodeGetResponse
	rsp := tmp.FromModels(errorCodes)

	totalPage := math.Ceil(float64(total) / float64(pr.PerPage))
	helpers.WriteResponse(r, w, helpers.PaginatedSuccessResponse(rsp, int64(pr.Page), int64(pr.PerPage), total,
		int64(totalPage)))
}

func ErrorCodeUpdate(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	id := ps.ByName("id")

	var req ErrorCodeUpdateRequest
	err := json.NewDecoder(r.Body).Decode(&req)
	if err != nil {
		helpers.WriteResponse(r, w, helpers.BadRequestResponse())
		return
	}

	var errorCode models.ErrorCode
	err = errorCode.Get(id)
	if errors.Is(err, sql.ErrNoRows) {
		helpers.WriteResponse(r, w, helpers.ClientErrorResponse("Error code data not found"))
		return
	} else if err != nil {
		logging.Error("error_code", "ErrorCodeUpdate: error get", logging.Data{
			"id":    id,
			"error": err.Error(),
		})

		helpers.WriteResponse(r, w, helpers.ServerError("Failed to retrieve error code data"))
		return
	}

	req.FillModel(&errorCode)
	err = errorCode.Update()
	if err != nil {
		logging.Error("error_code", "ErrorCodeUpdate: error update", logging.Data{
			"error": err.Error(),
			"body":  req,
		})

		helpers.WriteResponse(r, w, helpers.ServerError("Failed to save error code data"))
		return
	}

	var rsp ErrorCodeUpdateResponse
	rsp.FromModel(errorCode)
	helpers.WriteResponse(r, w, helpers.SuccessResponse(rsp))

}

func ErrorCodeDelete(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	id := ps.ByName("id")

	var errorCode models.ErrorCode
	err := errorCode.Get(id)
	if errors.Is(err, sql.ErrNoRows) {
		helpers.WriteResponse(r, w, helpers.ClientErrorResponse("Error code data not found"))
		return
	} else if err != nil {
		logging.Error("error_code", "ErrorCodeDelete: error get", logging.Data{
			"id":    id,
			"error": err.Error(),
		})

		helpers.WriteResponse(r, w, helpers.ServerError("Failed to retrieve error code data"))
		return
	}

	err = errorCode.Delete()
	if err != nil {
		logging.Error("error_code", "ErrorCodeDelete: error delete", logging.Data{
			"error": err.Error(),
			"id":    id,
		})

		helpers.WriteResponse(r, w, helpers.ServerError("Failed to delete error code data"))
		return
	}

	helpers.WriteResponse(r, w, helpers.SuccessResponse(nil))
}

func (r *ErrorCodeCreateRequest) ToModel() models.ErrorCode {
	return models.ErrorCode{
		CreatedBy:        r.CreatedBy,
		TrxTypeId:        r.TrxTypeId,
		ErrorCode:        r.ErrorCode,
		ErrorDescription: r.ErrorDescription,
	}
}

func (r *ErrorCodeUpdateRequest) FillModel(b *models.ErrorCode) {
	b.TrxTypeId = r.TrxTypeId
	b.ErrorCode = r.ErrorCode
	b.ErrorDescription = r.ErrorDescription
	b.Active = r.Active
	b.UpdatedBy = &r.UpdatedBy
}

func (r *ErrorCodeCreateResponse) FromModel(b models.ErrorCode) {
	r.Id = b.Id
	r.TrxTypeId = b.TrxTypeId
	r.ErrorCode = b.ErrorCode
	r.ErrorDescription = b.ErrorDescription
}

func (r *ErrorCodeGetResponse) FromModel(b models.ErrorCode) {
	r.Id = b.Id
	r.TrxTypeId = b.TrxTypeId
	r.ErrorCode = b.ErrorCode
	r.ErrorDescription = b.ErrorDescription
	r.Active = b.Active
}

func (r *ErrorCodeGetResponse) FromModels(ms []models.Model) []ErrorCodeGetResponse {
	var rs []ErrorCodeGetResponse
	for _, m := range ms {
		var r ErrorCodeGetResponse

		errorCode := m.(*models.ErrorCode)
		r.FromModel(*errorCode)
		rs = append(rs, r)
	}

	return rs
}

func (r *ErrorCodeUpdateResponse) FromModel(b models.ErrorCode) {
	r.Id = b.Id
	r.TrxTypeId = b.TrxTypeId
	r.ErrorCode = b.ErrorCode
	r.ErrorDescription = b.ErrorDescription
	r.Active = b.Active
	r.UpdatedBy = *b.UpdatedBy
}
