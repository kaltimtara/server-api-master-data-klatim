package controllers

import (
	"database/sql"
	"dg-datamaster-service/helpers"
	"dg-datamaster-service/models"
	"dg-datamaster-service/services/logging"
	"encoding/json"
	"errors"
	"math"
	"net/http"

	"github.com/julienschmidt/httprouter"
)

type LanguageCreateRequest struct {
	CreatedBy    string `json:"created_by"`
	Language     string `json:"language"`
	LanguageCode string `json:"language_code"`
}

type LanguageCreateResponse struct {
	Id           int64  `json:"id"`
	Language     string `json:"language"`
	LanguageCode string `json:"language_code"`
}

type LanguageUpdateRequest struct {
	Language     string `json:"language"`
	LanguageCode string `json:"language_code"`
	IsActive     bool   `json:"is_active"`
	UpdatedBy    string `json:"updated_by"`
}

type LanguageUpdateResponse struct {
	Id           int64  `json:"id"`
	Language     string `json:"language"`
	LanguageCode string `json:"language_code"`
	IsActive     bool   `json:"is_active"`
}

type LanguageGetResponse struct {
	Id           int64  `json:"id"`
	Language     string `json:"language"`
	LanguageCode string `json:"language_code"`
	IsActive     bool   `json:"is_active"`
}

func LanguageCreate(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	var req LanguageCreateRequest
	err := json.NewDecoder(r.Body).Decode(&req)
	if err != nil {
		helpers.WriteResponse(r, w, helpers.BadRequestResponse())
		return
	}

	language := req.ToModel()
	err = language.Create()
	if err != nil {
		logging.Error("language", "LanguageCreate: error create", logging.Data{
			"error": err.Error(),
			"body":  req,
		})

		helpers.WriteResponse(r, w, helpers.ServerError("Failed to save language data"))
		return
	}

	var rsp LanguageCreateResponse
	rsp.FromModel(language)

	helpers.WriteResponse(r, w, helpers.SuccessResponse(rsp))
}

func LanguageGet(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	id := ps.ByName("id")

	var language models.Language
	err := language.Get(id)
	if errors.Is(err, sql.ErrNoRows) {
		helpers.WriteResponse(r, w, helpers.ClientErrorResponse("Language data not found"))
		return
	} else if err != nil {
		logging.Error("language", "LanguageGet: error get", logging.Data{
			"id":    id,
			"error": err.Error(),
		})

		helpers.WriteResponse(r, w, helpers.ServerError("Failed to retrieve language data"))
		return
	}

	var rsp LanguageGetResponse
	rsp.FromModel(language)

	helpers.WriteResponse(r, w, helpers.SuccessResponse(rsp))
}

func LanguageList(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	var pr models.PaginationRequest
	err := pr.FromRequest(r)
	if err != nil {
		helpers.WriteResponse(r, w, helpers.BadRequestResponse())
		return
	}

	var f models.LanguageFilter
	f.FromRequest(r)

	var language models.Language
	languages, total, err := language.List(pr, &f)
	if err != nil {
		logging.Error("language", "LanguageList: error list", logging.Data{
			"error": err.Error(),
		})

		helpers.WriteResponse(r, w, helpers.ServerError("Failed to retrieve language data"))
		return
	}

	var tmp LanguageGetResponse
	rsp := tmp.FromModels(languages)

	totalPage := math.Ceil(float64(total) / float64(pr.PerPage))
	helpers.WriteResponse(r, w, helpers.PaginatedSuccessResponse(rsp, int64(pr.Page), int64(pr.PerPage), total,
		int64(totalPage)))
}

func LanguageUpdate(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	id := ps.ByName("id")

	var req LanguageUpdateRequest
	err := json.NewDecoder(r.Body).Decode(&req)
	if err != nil {
		helpers.WriteResponse(r, w, helpers.BadRequestResponse())
		return
	}

	var language models.Language
	err = language.Get(id)
	if errors.Is(err, sql.ErrNoRows) {
		helpers.WriteResponse(r, w, helpers.ClientErrorResponse("Language data not found"))
		return
	} else if err != nil {
		logging.Error("language", "LanguageUpdate: error get", logging.Data{
			"id":    id,
			"error": err.Error(),
		})

		helpers.WriteResponse(r, w, helpers.ServerError("Failed to retrieve language data"))
		return
	}

	req.FillModel(&language)
	err = language.Update()
	if err != nil {
		logging.Error("language", "LanguageUpdate: error update", logging.Data{
			"error": err.Error(),
			"body":  req,
		})

		helpers.WriteResponse(r, w, helpers.ServerError("Failed to save language data"))
		return
	}

	var rsp LanguageUpdateResponse
	rsp.FromModel(language)
	helpers.WriteResponse(r, w, helpers.SuccessResponse(rsp))

}

func LanguageDelete(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	id := ps.ByName("id")

	var language models.Language
	err := language.Get(id)
	if errors.Is(err, sql.ErrNoRows) {
		helpers.WriteResponse(r, w, helpers.ClientErrorResponse("Language data not found"))
		return
	} else if err != nil {
		logging.Error("language", "LanguageDelete: error get", logging.Data{
			"id":    id,
			"error": err.Error(),
		})

		helpers.WriteResponse(r, w, helpers.ServerError("Failed to retrieve language data"))
		return
	}

	err = language.Delete()
	if err != nil {
		logging.Error("language", "LanguageDelete: error delete", logging.Data{
			"error": err.Error(),
			"id":    id,
		})

		helpers.WriteResponse(r, w, helpers.ServerError("Failed to delete language data"))
		return
	}

	helpers.WriteResponse(r, w, helpers.SuccessResponse(nil))
}

func (r *LanguageCreateRequest) ToModel() models.Language {
	return models.Language{
		CreatedBy:    r.CreatedBy,
		Language:     r.Language,
		LanguageCode: r.LanguageCode,
	}
}

func (r *LanguageUpdateRequest) FillModel(l *models.Language) {
	l.Language = r.Language
	l.LanguageCode = r.LanguageCode
	l.IsActive = r.IsActive
	l.UpdatedBy = &r.UpdatedBy
}

func (r *LanguageCreateResponse) FromModel(l models.Language) {
	r.Id = l.Id
	r.Language = l.Language
	r.LanguageCode = l.LanguageCode
}

func (r *LanguageGetResponse) FromModel(l models.Language) {
	r.Id = l.Id
	r.Language = l.Language
	r.LanguageCode = l.LanguageCode
	r.IsActive = l.IsActive
}

func (r *LanguageGetResponse) FromModels(ls []models.Model) []LanguageGetResponse {
	var rs []LanguageGetResponse
	for _, m := range ls {
		var r LanguageGetResponse

		language := m.(*models.Language)
		r.FromModel(*language)
		rs = append(rs, r)
	}

	return rs
}

func (r *LanguageUpdateResponse) FromModel(l models.Language) {
	r.Id = l.Id
	r.Language = l.Language
	r.LanguageCode = l.LanguageCode
	r.IsActive = l.IsActive
}
