package controllers

import (
	"database/sql"
	"dg-datamaster-service/helpers"
	"dg-datamaster-service/models"
	"dg-datamaster-service/services/logging"
	"encoding/json"
	"errors"
	"math"
	"net/http"

	"github.com/julienschmidt/httprouter"
)

type SettingCreateRequest struct {
	CreatedBy     string `json:"created_by"`
	SettingTypeId int64  `json:"setting_type_id"`
	VariableName  string `json:"variable_name"`
	VariableValue string `json:"variable_value"`
}

type SettingCreateResponse struct {
	Id            int64  `json:"id"`
	SettingTypeId int64  `json:"setting_type_id"`
	VariableName  string `json:"variable_name"`
	VariableValue string `json:"variable_value"`
}

type SettingUpdateRequest struct {
	SettingTypeId int64  `json:"setting_type_id"`
	VariableName  string `json:"variable_name"`
	VariableValue string `json:"variable_value"`
	Active        bool   `json:"is_active"`
	UpdatedBy     string `json:"updated_by"`
}

type SettingUpdateResponse struct {
	Id            int64  `json:"id"`
	SettingTypeId int64  `json:"setting_type_id"`
	VariableName  string `json:"variable_name"`
	VariableValue string `json:"variable_value"`
	Active        bool   `json:"is_active"`
	UpdatedBy     string `json:"updated_by"`
}

type SettingGetResponse struct {
	Id            int64  `json:"id"`
	SettingTypeId int64  `json:"setting_type_id"`
	VariableName  string `json:"variable_name"`
	VariableValue string `json:"variable_value"`
	Active        bool   `json:"is_active"`
}

func SettingCreate(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	var req SettingCreateRequest
	err := json.NewDecoder(r.Body).Decode(&req)
	if err != nil {
		helpers.WriteResponse(r, w, helpers.BadRequestResponse())
		return
	}

	setting := req.ToModel()
	err = setting.Create()
	if err != nil {
		logging.Error("setting", "SettingCreate: error create", logging.Data{
			"error": err.Error(),
			"body":  req,
		})

		helpers.WriteResponse(r, w, helpers.ServerError("Failed to save setting data"))
		return
	}

	var rsp SettingCreateResponse
	rsp.FromModel(setting)

	helpers.WriteResponse(r, w, helpers.SuccessResponse(rsp))
}

func SettingGet(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	id := ps.ByName("id")

	var setting models.Setting
	err := setting.Get(id)
	if errors.Is(err, sql.ErrNoRows) {
		helpers.WriteResponse(r, w, helpers.ClientErrorResponse("Setting data not found"))
		return
	} else if err != nil {
		logging.Error("setting", "SettingGet: error get", logging.Data{
			"id":    id,
			"error": err.Error(),
		})

		helpers.WriteResponse(r, w, helpers.ServerError("Failed to retrieve setting data"))
		return
	}

	var rsp SettingGetResponse
	rsp.FromModel(setting)

	helpers.WriteResponse(r, w, helpers.SuccessResponse(rsp))
}

func SettingList(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	var pr models.PaginationRequest
	err := pr.FromRequest(r)
	if err != nil {
		helpers.WriteResponse(r, w, helpers.BadRequestResponse())
		return
	}

	var f models.SettingFilter
	f.FromRequest(r)

	var setting models.Setting
	settings, total, err := setting.List(pr, &f)
	if err != nil {
		logging.Error("setting", "SettingList: error list", logging.Data{
			"error": err.Error(),
		})

		helpers.WriteResponse(r, w, helpers.ServerError("Failed to retrieve setting data"))
		return
	}

	var tmp SettingGetResponse
	rsp := tmp.FromModels(settings)

	totalPage := math.Ceil(float64(total) / float64(pr.PerPage))
	helpers.WriteResponse(r, w, helpers.PaginatedSuccessResponse(rsp, int64(pr.Page), int64(pr.PerPage), total,
		int64(totalPage)))
}

func SettingUpdate(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	id := ps.ByName("id")

	var req SettingUpdateRequest
	err := json.NewDecoder(r.Body).Decode(&req)
	if err != nil {
		helpers.WriteResponse(r, w, helpers.BadRequestResponse())
		return
	}

	var setting models.Setting
	err = setting.Get(id)
	if errors.Is(err, sql.ErrNoRows) {
		helpers.WriteResponse(r, w, helpers.ClientErrorResponse("Setting data not found"))
		return
	} else if err != nil {
		logging.Error("setting", "SettingUpdate: error get", logging.Data{
			"id":    id,
			"error": err.Error(),
		})

		helpers.WriteResponse(r, w, helpers.ServerError("Failed to retrieve setting data"))
		return
	}

	req.FillModel(&setting)
	err = setting.Update()
	if err != nil {
		logging.Error("setting", "SettingUpdate: error update", logging.Data{
			"error": err.Error(),
			"body":  req,
		})

		helpers.WriteResponse(r, w, helpers.ServerError("Failed to save setting data"))
		return
	}

	var rsp SettingUpdateResponse
	rsp.FromModel(setting)
	helpers.WriteResponse(r, w, helpers.SuccessResponse(rsp))

}

func SettingDelete(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	id := ps.ByName("id")

	var setting models.Setting
	err := setting.Get(id)
	if errors.Is(err, sql.ErrNoRows) {
		helpers.WriteResponse(r, w, helpers.ClientErrorResponse("Setting data not found"))
		return
	} else if err != nil {
		logging.Error("setting", "SettingDelete: error get", logging.Data{
			"id":    id,
			"error": err.Error(),
		})

		helpers.WriteResponse(r, w, helpers.ServerError("Failed to retrieve setting data"))
		return
	}

	err = setting.Delete()
	if err != nil {
		logging.Error("setting", "SettingDelete: error delete", logging.Data{
			"error": err.Error(),
			"id":    id,
		})

		helpers.WriteResponse(r, w, helpers.ServerError("Failed to delete setting data"))
		return
	}

	helpers.WriteResponse(r, w, helpers.SuccessResponse(nil))
}

func (r *SettingCreateRequest) ToModel() models.Setting {
	return models.Setting{
		CreatedBy:     r.CreatedBy,
		SettingTypeId: r.SettingTypeId,
		VariableName:  r.VariableName,
		VariableValue: r.VariableValue,
	}
}

func (r *SettingUpdateRequest) FillModel(b *models.Setting) {
	b.SettingTypeId = r.SettingTypeId
	b.VariableName = r.VariableName
	b.VariableValue = r.VariableValue
	b.Active = r.Active
	b.UpdatedBy = &r.UpdatedBy
}

func (r *SettingCreateResponse) FromModel(b models.Setting) {
	r.Id = b.Id
	r.SettingTypeId = b.SettingTypeId
	r.VariableName = b.VariableName
	r.VariableValue = b.VariableValue
}

func (r *SettingGetResponse) FromModel(b models.Setting) {
	r.Id = b.Id
	r.SettingTypeId = b.SettingTypeId
	r.VariableName = b.VariableName
	r.VariableValue = b.VariableValue
	r.Active = b.Active
}

func (r *SettingGetResponse) FromModels(ms []models.Model) []SettingGetResponse {
	var rs []SettingGetResponse
	for _, m := range ms {
		var r SettingGetResponse

		setting := m.(*models.Setting)
		r.FromModel(*setting)
		rs = append(rs, r)
	}

	return rs
}

func (r *SettingUpdateResponse) FromModel(b models.Setting) {
	r.Id = b.Id
	r.SettingTypeId = b.SettingTypeId
	r.VariableName = b.VariableName
	r.VariableValue = b.VariableValue
	r.Active = b.Active
	r.UpdatedBy = *b.UpdatedBy
}
