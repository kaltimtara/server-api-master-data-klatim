package controllers

import (
	"database/sql"
	"dg-datamaster-service/helpers"
	"dg-datamaster-service/models"
	"dg-datamaster-service/services/logging"
	"encoding/json"
	"errors"
	"github.com/julienschmidt/httprouter"
	"math"
	"net/http"
)

type BranchOfficeCreateRequest struct {
	CreatedBy        string `json:"created_by"`
	BranchCode       string `json:"branch_code"`
	ShariaBranchCode string `json:"sharia_branch_code"`
	OfficeName       string `json:"office_name"`
	OfficeAddress    string `json:"office_address"`
}

type BranchOfficeCreateResponse struct {
	Id               int64  `json:"id"`
	BranchCode       string `json:"branch_code"`
	ShariaBranchCode string `json:"sharia_branch_code"`
	OfficeName       string `json:"office_name"`
	OfficeAddress    string `json:"office_address"`
}

type BranchOfficeUpdateRequest struct {
	BranchCode       string `json:"branch_code"`
	ShariaBranchCode string `json:"sharia_branch_code"`
	OfficeName       string `json:"office_name"`
	OfficeAddress    string `json:"office_address"`
	IsActive         bool   `json:"is_active"`
	UpdatedBy        string `json:"updated_by"`
}

type BranchOfficeUpdateResponse struct {
	Id               int64  `json:"id"`
	BranchCode       string `json:"branch_code"`
	ShariaBranchCode string `json:"sharia_branch_code"`
	OfficeName       string `json:"office_name"`
	OfficeAddress    string `json:"office_address"`
	IsActive         bool   `json:"is_active"`
}

type BranchOfficeGetResponse struct {
	Id               int64  `json:"id"`
	BranchCode       string `json:"branch_code"`
	ShariaBranchCode string `json:"sharia_branch_code"`
	OfficeName       string `json:"office_name"`
	OfficeAddress    string `json:"office_address"`
	IsActive         bool   `json:"is_active"`
}

func BranchOfficeCreate(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	var req BranchOfficeCreateRequest
	err := json.NewDecoder(r.Body).Decode(&req)
	if err != nil {
		helpers.WriteResponse(r, w, helpers.BadRequestResponse())
		return
	}

	branchOffice := req.ToModel()
	err = branchOffice.Create()
	if err != nil {
		logging.Error("branchOffice", "BranchOfficeCreate: error create", logging.Data{
			"error": err.Error(),
			"body":  req,
		})

		helpers.WriteResponse(r, w, helpers.ServerError("Failed to save branch office data"))
		return
	}

	var rsp BranchOfficeCreateResponse
	rsp.FromModel(branchOffice)

	helpers.WriteResponse(r, w, helpers.SuccessResponse(rsp))
}

func BranchOfficeGet(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	id := ps.ByName("id")

	var branchOffice models.BranchOffice
	err := branchOffice.Get(id)
	if errors.Is(err, sql.ErrNoRows) {
		helpers.WriteResponse(r, w, helpers.ClientErrorResponse("Branch office data not found"))
		return
	} else if err != nil {
		logging.Error("branchOffice", "BranchOfficeGet: error get", logging.Data{
			"id":    id,
			"error": err.Error(),
		})

		helpers.WriteResponse(r, w, helpers.ServerError("Failed to retrieve branch office data"))
		return
	}

	var rsp BranchOfficeGetResponse
	rsp.FromModel(branchOffice)

	helpers.WriteResponse(r, w, helpers.SuccessResponse(rsp))
}

func BranchOfficeList(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	var pr models.PaginationRequest
	err := pr.FromRequest(r)
	if err != nil {
		helpers.WriteResponse(r, w, helpers.BadRequestResponse())
		return
	}

	var f models.BranchOfficeFilter
	f.FromRequest(r)

	var branchOffice models.BranchOffice
	branchOffices, total, err := branchOffice.List(pr, &f)
	if err != nil {
		logging.Error("branchOffice", "BranchOfficeList: error list", logging.Data{
			"error": err.Error(),
		})

		helpers.WriteResponse(r, w, helpers.ServerError("Failed to retrieve branch office data"))
		return
	}

	var tmp BranchOfficeGetResponse
	rsp := tmp.FromModels(branchOffices)

	totalPage := math.Ceil(float64(total) / float64(pr.PerPage))
	helpers.WriteResponse(r, w, helpers.PaginatedSuccessResponse(rsp, int64(pr.Page), int64(pr.PerPage), total,
		int64(totalPage)))
}

func BranchOfficeUpdate(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	id := ps.ByName("id")

	var req BranchOfficeUpdateRequest
	err := json.NewDecoder(r.Body).Decode(&req)
	if err != nil {
		helpers.WriteResponse(r, w, helpers.BadRequestResponse())
		return
	}

	var branchOffice models.BranchOffice
	err = branchOffice.Get(id)
	if errors.Is(err, sql.ErrNoRows) {
		helpers.WriteResponse(r, w, helpers.ClientErrorResponse("Branch office data not found"))
		return
	} else if err != nil {
		logging.Error("branchOffice", "BranchOfficeUpdate: error get", logging.Data{
			"id":    id,
			"error": err.Error(),
		})

		helpers.WriteResponse(r, w, helpers.ServerError("Failed to retrieve branch office data"))
		return
	}

	req.FillModel(&branchOffice)
	err = branchOffice.Update()
	if err != nil {
		logging.Error("branchOffice", "BranchOfficeUpdate: error update", logging.Data{
			"error": err.Error(),
			"body":  req,
		})

		helpers.WriteResponse(r, w, helpers.ServerError("Failed to save branch office data"))
		return
	}

	var rsp BranchOfficeUpdateResponse
	rsp.FromModel(branchOffice)
	helpers.WriteResponse(r, w, helpers.SuccessResponse(rsp))

}

func BranchOfficeDelete(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	id := ps.ByName("id")

	var branchOffice models.BranchOffice
	err := branchOffice.Get(id)
	if errors.Is(err, sql.ErrNoRows) {
		helpers.WriteResponse(r, w, helpers.ClientErrorResponse("Branch office data not found"))
		return
	} else if err != nil {
		logging.Error("branchOffice", "BranchOfficeDelete: error get", logging.Data{
			"id":    id,
			"error": err.Error(),
		})

		helpers.WriteResponse(r, w, helpers.ServerError("Failed to retrieve branch office data"))
		return
	}

	err = branchOffice.Delete()
	if err != nil {
		logging.Error("branchOffice", "BranchOfficeDelete: error delete", logging.Data{
			"error": err.Error(),
			"id":    id,
		})

		helpers.WriteResponse(r, w, helpers.ServerError("Failed to delete branch office data"))
		return
	}

	helpers.WriteResponse(r, w, helpers.SuccessResponse(nil))
}

func (r *BranchOfficeCreateRequest) ToModel() models.BranchOffice {
	m := models.BranchOffice{
		CreatedBy:     r.CreatedBy,
		BranchCode:    r.BranchCode,
		OfficeName:    r.OfficeName,
		OfficeAddress: r.OfficeAddress,
	}

	if r.ShariaBranchCode != "" {
		m.ShariaBranchCode = &r.ShariaBranchCode
	}

	return m
}

func (r *BranchOfficeUpdateRequest) FillModel(b *models.BranchOffice) {
	b.BranchCode = r.BranchCode
	b.OfficeName = r.OfficeName
	b.OfficeAddress = r.OfficeAddress
	b.IsActive = r.IsActive
	b.UpdatedBy = &r.UpdatedBy

	if r.ShariaBranchCode != "" {
		b.ShariaBranchCode = &r.ShariaBranchCode
	}
}

func (r *BranchOfficeCreateResponse) FromModel(b models.BranchOffice) {
	r.Id = b.Id
	r.BranchCode = b.BranchCode
	r.OfficeName = b.OfficeName
	r.OfficeAddress = b.OfficeAddress

	if b.ShariaBranchCode != nil {
		r.ShariaBranchCode = *b.ShariaBranchCode
	}
}

func (r *BranchOfficeGetResponse) FromModel(b models.BranchOffice) {
	r.Id = b.Id
	r.BranchCode = b.BranchCode
	r.OfficeName = b.OfficeName
	r.OfficeAddress = b.OfficeAddress
	r.IsActive = b.IsActive

	if b.ShariaBranchCode != nil {
		r.ShariaBranchCode = *b.ShariaBranchCode
	}
}

func (r *BranchOfficeGetResponse) FromModels(bs []models.Model) []BranchOfficeGetResponse {
	var rs []BranchOfficeGetResponse
	for _, m := range bs {
		var r BranchOfficeGetResponse

		branchOffice := m.(*models.BranchOffice)
		r.FromModel(*branchOffice)
		rs = append(rs, r)
	}

	return rs
}

func (r *BranchOfficeUpdateResponse) FromModel(b models.BranchOffice) {
	r.Id = b.Id
	r.BranchCode = b.BranchCode
	r.OfficeName = b.OfficeName
	r.OfficeAddress = b.OfficeAddress
	r.IsActive = b.IsActive

	if b.ShariaBranchCode != nil {
		r.ShariaBranchCode = *b.ShariaBranchCode
	}
}
