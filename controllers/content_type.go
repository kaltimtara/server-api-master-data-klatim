package controllers

import (
	"database/sql"
	"dg-datamaster-service/helpers"
	"dg-datamaster-service/models"
	"dg-datamaster-service/services/logging"
	"encoding/json"
	"errors"
	"math"
	"net/http"

	"github.com/julienschmidt/httprouter"
)

type ContentTypeCreateRequest struct {
	CreatedBy       string `json:"created_by"`
	ContentTypeCode string `json:"content_type_code"`
	ContentType     string `json:"content_type"`
}

type ContentTypeCreateResponse struct {
	Id              int64  `json:"id"`
	ContentTypeCode string `json:"content_type_code"`
	ContentType     string `json:"content_type"`
}

type ContentTypeUpdateRequest struct {
	ContentTypeCode string `json:"content_type_code"`
	ContentType     string `json:"content_type"`
	IsActive        bool   `json:"is_active"`
	UpdatedBy       string `json:"updated_by"`
}

type ContentTypeUpdateResponse struct {
	Id              int64  `json:"id"`
	ContentTypeCode string `json:"content_type_code"`
	ContentType     string `json:"content_type"`
	IsActive        bool   `json:"is_active"`
}

type ContentTypeGetResponse struct {
	Id              int64  `json:"id"`
	ContentTypeCode string `json:"content_type_code"`
	ContentType     string `json:"content_type"`
	IsActive        bool   `json:"is_active"`
}

func ContentTypeCreate(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	var req ContentTypeCreateRequest
	err := json.NewDecoder(r.Body).Decode(&req)
	if err != nil {
		helpers.WriteResponse(r, w, helpers.BadRequestResponse())
		return
	}

	contentType := req.ToModel()
	err = contentType.Create()
	if err != nil {
		logging.Error("contentType", "ContentTypeCreate: error create", logging.Data{
			"error": err.Error(),
			"body":  req,
		})

		helpers.WriteResponse(r, w, helpers.ServerError("Failed to save content type data"))
		return
	}

	var rsp ContentTypeCreateResponse
	rsp.FromModel(contentType)

	helpers.WriteResponse(r, w, helpers.SuccessResponse(rsp))
}

func ContentTypeGet(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	id := ps.ByName("id")

	var contentType models.ContentType
	err := contentType.Get(id)
	if errors.Is(err, sql.ErrNoRows) {
		helpers.WriteResponse(r, w, helpers.ClientErrorResponse("Content type data not found"))
		return
	} else if err != nil {
		logging.Error("contentType", "ContentTypeGet: error get", logging.Data{
			"id":    id,
			"error": err.Error(),
		})

		helpers.WriteResponse(r, w, helpers.ServerError("Failed to retrieve content type data"))
		return
	}

	var rsp ContentTypeGetResponse
	rsp.FromModel(contentType)

	helpers.WriteResponse(r, w, helpers.SuccessResponse(rsp))
}

func ContentTypeList(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	var pr models.PaginationRequest
	err := pr.FromRequest(r)
	if err != nil {
		helpers.WriteResponse(r, w, helpers.BadRequestResponse())
		return
	}

	var f models.ContentTypeFilter
	f.FromRequest(r)

	var contentType models.ContentType
	contentTypes, total, err := contentType.List(pr, &f)
	if err != nil {
		logging.Error("contentType", "ContentTypeList: error list", logging.Data{
			"error": err.Error(),
		})

		helpers.WriteResponse(r, w, helpers.ServerError("Failed to retrieve content type data"))
		return
	}

	var tmp ContentTypeGetResponse
	rsp := tmp.FromModels(contentTypes)

	totalPage := math.Ceil(float64(total) / float64(pr.PerPage))
	helpers.WriteResponse(r, w, helpers.PaginatedSuccessResponse(rsp, int64(pr.Page), int64(pr.PerPage), total,
		int64(totalPage)))
}

func ContentTypeUpdate(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	id := ps.ByName("id")

	var req ContentTypeUpdateRequest
	err := json.NewDecoder(r.Body).Decode(&req)
	if err != nil {
		helpers.WriteResponse(r, w, helpers.BadRequestResponse())
		return
	}

	var contentType models.ContentType
	err = contentType.Get(id)
	if errors.Is(err, sql.ErrNoRows) {
		helpers.WriteResponse(r, w, helpers.ClientErrorResponse("Content type data not found"))
		return
	} else if err != nil {
		logging.Error("contentType", "ContentTypeUpdate: error get", logging.Data{
			"id":    id,
			"error": err.Error(),
		})

		helpers.WriteResponse(r, w, helpers.ServerError("Failed to retrieve content type data"))
		return
	}

	req.FillModel(&contentType)
	err = contentType.Update()
	if err != nil {
		logging.Error("contentType", "ContentTypeUpdate: error update", logging.Data{
			"error": err.Error(),
			"body":  req,
		})

		helpers.WriteResponse(r, w, helpers.ServerError("Failed to save content type data"))
		return
	}

	var rsp ContentTypeUpdateResponse
	rsp.FromModel(contentType)
	helpers.WriteResponse(r, w, helpers.SuccessResponse(rsp))

}

func ContentTypeDelete(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	id := ps.ByName("id")

	var contentType models.ContentType
	err := contentType.Get(id)
	if errors.Is(err, sql.ErrNoRows) {
		helpers.WriteResponse(r, w, helpers.ClientErrorResponse("Content type data not found"))
		return
	} else if err != nil {
		logging.Error("contentType", "ContentTypeDelete: error get", logging.Data{
			"id":    id,
			"error": err.Error(),
		})

		helpers.WriteResponse(r, w, helpers.ServerError("Failed to retrieve content type data"))
		return
	}

	err = contentType.Delete()
	if err != nil {
		logging.Error("contentType", "ContentTypeDelete: error delete", logging.Data{
			"error": err.Error(),
			"id":    id,
		})

		helpers.WriteResponse(r, w, helpers.ServerError("Failed to delete content type data"))
		return
	}

	helpers.WriteResponse(r, w, helpers.SuccessResponse(nil))
}

func (r *ContentTypeCreateRequest) ToModel() models.ContentType {
	return models.ContentType{
		CreatedBy:       r.CreatedBy,
		ContentTypeCode: r.ContentTypeCode,
		ContentType:     r.ContentType,
	}
}

func (r *ContentTypeUpdateRequest) FillModel(c *models.ContentType) {
	c.ContentTypeCode = r.ContentTypeCode
	c.ContentType = r.ContentType

	c.IsActive = r.IsActive
	c.UpdatedBy = &r.UpdatedBy
}

func (r *ContentTypeCreateResponse) FromModel(c models.ContentType) {
	r.Id = c.Id
	r.ContentTypeCode = c.ContentTypeCode
	r.ContentType = c.ContentType
}

func (r *ContentTypeGetResponse) FromModel(c models.ContentType) {
	r.Id = c.Id
	r.ContentTypeCode = c.ContentTypeCode
	r.ContentType = c.ContentType
	r.IsActive = c.IsActive
}

func (r *ContentTypeGetResponse) FromModels(cs []models.Model) []ContentTypeGetResponse {
	var rs []ContentTypeGetResponse
	for _, m := range cs {
		var r ContentTypeGetResponse

		contentType := m.(*models.ContentType)
		r.FromModel(*contentType)
		rs = append(rs, r)
	}

	return rs
}

func (r *ContentTypeUpdateResponse) FromModel(c models.ContentType) {
	r.Id = c.Id
	r.ContentTypeCode = c.ContentTypeCode
	r.ContentType = c.ContentType
	r.IsActive = c.IsActive
}
