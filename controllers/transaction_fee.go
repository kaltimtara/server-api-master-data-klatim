package controllers

import (
	"database/sql"
	"dg-datamaster-service/helpers"
	"dg-datamaster-service/models"
	"dg-datamaster-service/services/logging"
	"encoding/json"
	"errors"
	"math"
	"net/http"

	"github.com/julienschmidt/httprouter"
)

type TransactionFeeCreateRequest struct {
	CreatedBy   string  `json:"created_by"`
	TrxTypeId   int64   `json:"trx_type_id"`
	FixedAmount bool    `json:"is_fixed_amount"`
	FeeAmount   float64 `json:"fee_amount"`
}

type TransactionFeeCreateResponse struct {
	Id          int64   `json:"id"`
	TrxTypeId   int64   `json:"trx_type_id"`
	FixedAmount bool    `json:"is_fixed_amount"`
	FeeAmount   float64 `json:"fee_amount"`
}

type TransactionFeeUpdateRequest struct {
	TrxTypeId   int64   `json:"trx_type_id"`
	FixedAmount bool    `json:"is_fixed_amount"`
	FeeAmount   float64 `json:"fee_amount"`
	Active      bool    `json:"is_active"`
	UpdatedBy   string  `json:"updated_by"`
}

type TransactionFeeUpdateResponse struct {
	Id          int64   `json:"id"`
	TrxTypeId   int64   `json:"trx_type_id"`
	FixedAmount bool    `json:"is_fixed_amount"`
	FeeAmount   float64 `json:"fee_amount"`
	Active      bool    `json:"is_active"`
	UpdatedBy   string  `json:"updated_by"`
}

type TransactionFeeGetResponse struct {
	Id          int64   `json:"id"`
	TrxTypeId   int64   `json:"trx_type_id"`
	FixedAmount bool    `json:"is_fixed_amount"`
	FeeAmount   float64 `json:"fee_amount"`
	Active      bool    `json:"is_active"`
}

func TransactionFeeCreate(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	var req TransactionFeeCreateRequest
	err := json.NewDecoder(r.Body).Decode(&req)
	if err != nil {
		helpers.WriteResponse(r, w, helpers.BadRequestResponse())
		return
	}

	transactionFee := req.ToModel()
	err = transactionFee.Create()
	if err != nil {
		logging.Error("transaction_fee", "TransactionFeeCreate: error create", logging.Data{
			"error": err.Error(),
			"body":  req,
		})

		helpers.WriteResponse(r, w, helpers.ServerError("Failed to save transaction fee data"))
		return
	}

	var rsp TransactionFeeCreateResponse
	rsp.FromModel(transactionFee)

	helpers.WriteResponse(r, w, helpers.SuccessResponse(rsp))
}

func TransactionFeeGet(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	id := ps.ByName("id")

	var transactionFee models.TransactionFee
	err := transactionFee.Get(id)
	if errors.Is(err, sql.ErrNoRows) {
		helpers.WriteResponse(r, w, helpers.ClientErrorResponse("Transaction fee data not found"))
		return
	} else if err != nil {
		logging.Error("transaction_fee", "TransactionFeeGet: error get", logging.Data{
			"id":    id,
			"error": err.Error(),
		})

		helpers.WriteResponse(r, w, helpers.ServerError("Failed to retrieve transaction fee data"))
		return
	}

	var rsp TransactionFeeGetResponse
	rsp.FromModel(transactionFee)

	helpers.WriteResponse(r, w, helpers.SuccessResponse(rsp))
}

func TransactionFeeList(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	var pr models.PaginationRequest
	err := pr.FromRequest(r)
	if err != nil {
		helpers.WriteResponse(r, w, helpers.BadRequestResponse())
		return
	}

	var f models.TransactionFeeFilter
	f.FromRequest(r)

	var transactionFee models.TransactionFee
	transactionFees, total, err := transactionFee.List(pr, &f)
	if err != nil {
		logging.Error("transaction_fee", "TransactionFeeList: error list", logging.Data{
			"error": err.Error(),
		})

		helpers.WriteResponse(r, w, helpers.ServerError("Failed to retrieve transaction fee data"))
		return
	}

	var tmp TransactionFeeGetResponse
	rsp := tmp.FromModels(transactionFees)

	totalPage := math.Ceil(float64(total) / float64(pr.PerPage))
	helpers.WriteResponse(r, w, helpers.PaginatedSuccessResponse(rsp, int64(pr.Page), int64(pr.PerPage), total,
		int64(totalPage)))
}

func TransactionFeeUpdate(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	id := ps.ByName("id")

	var req TransactionFeeUpdateRequest
	err := json.NewDecoder(r.Body).Decode(&req)
	if err != nil {
		helpers.WriteResponse(r, w, helpers.BadRequestResponse())
		return
	}

	var transactionFee models.TransactionFee
	err = transactionFee.Get(id)
	if errors.Is(err, sql.ErrNoRows) {
		helpers.WriteResponse(r, w, helpers.ClientErrorResponse("Transaction fee data not found"))
		return
	} else if err != nil {
		logging.Error("transaction_fee", "TransactionFeeUpdate: error get", logging.Data{
			"id":    id,
			"error": err.Error(),
		})

		helpers.WriteResponse(r, w, helpers.ServerError("Failed to retrieve transaction fee data"))
		return
	}

	req.FillModel(&transactionFee)
	err = transactionFee.Update()
	if err != nil {
		logging.Error("transaction_fee", "TransactionFeeUpdate: error update", logging.Data{
			"error": err.Error(),
			"body":  req,
		})

		helpers.WriteResponse(r, w, helpers.ServerError("Failed to save transaction fee data"))
		return
	}

	var rsp TransactionFeeUpdateResponse
	rsp.FromModel(transactionFee)
	helpers.WriteResponse(r, w, helpers.SuccessResponse(rsp))

}

func TransactionFeeDelete(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	id := ps.ByName("id")

	var transactionFee models.TransactionFee
	err := transactionFee.Get(id)
	if errors.Is(err, sql.ErrNoRows) {
		helpers.WriteResponse(r, w, helpers.ClientErrorResponse("Transaction fee data not found"))
		return
	} else if err != nil {
		logging.Error("transaction_fee", "TransactionFeeDelete: error get", logging.Data{
			"id":    id,
			"error": err.Error(),
		})

		helpers.WriteResponse(r, w, helpers.ServerError("Failed to retrieve transaction fee data"))
		return
	}

	err = transactionFee.Delete()
	if err != nil {
		logging.Error("transaction_fee", "TransactionFeeDelete: error delete", logging.Data{
			"error": err.Error(),
			"id":    id,
		})

		helpers.WriteResponse(r, w, helpers.ServerError("Failed to delete transaction fee data"))
		return
	}

	helpers.WriteResponse(r, w, helpers.SuccessResponse(nil))
}

func (r *TransactionFeeCreateRequest) ToModel() models.TransactionFee {
	return models.TransactionFee{
		CreatedBy:   r.CreatedBy,
		TrxTypeId:   r.TrxTypeId,
		FixedAmount: r.FixedAmount,
		FeeAmount:   r.FeeAmount,
	}
}

func (r *TransactionFeeUpdateRequest) FillModel(b *models.TransactionFee) {
	b.TrxTypeId = r.TrxTypeId
	b.FixedAmount = r.FixedAmount
	b.FeeAmount = r.FeeAmount
	b.Active = r.Active
	b.UpdatedBy = &r.UpdatedBy
}

func (r *TransactionFeeCreateResponse) FromModel(b models.TransactionFee) {
	r.Id = b.Id
	r.TrxTypeId = b.TrxTypeId
	r.FixedAmount = b.FixedAmount
	r.FeeAmount = b.FeeAmount
}

func (r *TransactionFeeGetResponse) FromModel(b models.TransactionFee) {
	r.Id = b.Id
	r.TrxTypeId = b.TrxTypeId
	r.FixedAmount = b.FixedAmount
	r.FeeAmount = b.FeeAmount
	r.Active = b.Active
}

func (r *TransactionFeeGetResponse) FromModels(ms []models.Model) []TransactionFeeGetResponse {
	var rs []TransactionFeeGetResponse
	for _, m := range ms {
		var r TransactionFeeGetResponse

		transactionFee := m.(*models.TransactionFee)
		r.FromModel(*transactionFee)
		rs = append(rs, r)
	}

	return rs
}

func (r *TransactionFeeUpdateResponse) FromModel(b models.TransactionFee) {
	r.Id = b.Id
	r.TrxTypeId = b.TrxTypeId
	r.FixedAmount = b.FixedAmount
	r.FeeAmount = b.FeeAmount
	r.Active = b.Active
	r.UpdatedBy = *b.UpdatedBy
}
