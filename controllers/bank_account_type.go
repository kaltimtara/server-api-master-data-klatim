package controllers

import (
	"database/sql"
	"dg-datamaster-service/helpers"
	"dg-datamaster-service/models"
	"dg-datamaster-service/services/logging"
	"encoding/json"
	"errors"
	"math"
	"net/http"

	"github.com/julienschmidt/httprouter"
)

type BankAccountTypeCreateRequest struct {
	CreatedBy       string  `json:"created_by"`
	BankAccountType string  `json:"bank_account_type"`
	BankAccountCode string  `json:"bank_account_code"`
	Description     string  `json:"description"`
	MinBalance      float64 `json:"min_balance"`
	MaxBalance      float64 `json:"max_balance"`
	Sharia          bool    `json:"is_sharia"`
}

type BankAccountTypeCreateResponse struct {
	Id              int64   `json:"id"`
	BankAccountType string  `json:"bank_account_type"`
	BankAccountCode string  `json:"bank_account_code"`
	Description     string  `json:"description"`
	MinBalance      float64 `json:"min_balance"`
	MaxBalance      float64 `json:"max_balance"`
	Sharia          bool    `json:"is_sharia"`
}

type BankAccountTypeUpdateRequest struct {
	BankAccountType string  `json:"bank_account_type"`
	BankAccountCode string  `json:"bank_account_code"`
	MinBalance      float64 `json:"min_balance"`
	MaxBalance      float64 `json:"max_balance"`
	Description     string  `json:"description"`
	Sharia          bool    `json:"is_sharia"`
	Active          bool    `json:"is_active"`
	UpdatedBy       string  `json:"updated_by"`
}

type BankAccountTypeUpdateResponse struct {
	Id              int64   `json:"id"`
	BankAccountType string  `json:"bank_account_type"`
	BankAccountCode string  `json:"bank_account_code"`
	MinBalance      float64 `json:"min_balance"`
	MaxBalance      float64 `json:"max_balance"`
	Description     string  `json:"description"`
	Sharia          bool    `json:"is_sharia"`
	Active          bool    `json:"is_active"`
	UpdatedBy       string  `json:"updated_by"`
}

type BankAccountTypeGetResponse struct {
	Id              int64   `json:"id"`
	BankAccountType string  `json:"bank_account_type"`
	BankAccountCode string  `json:"bank_account_code"`
	MinBalance      float64 `json:"min_balance"`
	MaxBalance      float64 `json:"max_balance"`
	Description     string  `json:"description"`
	Sharia          bool    `json:"is_sharia"`
	Active          bool    `json:"is_active"`
}

func BankAccountTypeCreate(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	var req BankAccountTypeCreateRequest
	err := json.NewDecoder(r.Body).Decode(&req)
	if err != nil {
		helpers.WriteResponse(r, w, helpers.BadRequestResponse())
		return
	}

	bankAccountType := req.ToModel()
	err = bankAccountType.Create()
	if err != nil {
		logging.Error("bank_account_type", "BankAccountTypeCreate: error create", logging.Data{
			"error": err.Error(),
			"body":  req,
		})

		helpers.WriteResponse(r, w, helpers.ServerError("Failed to save bank account type data"))
		return
	}

	var rsp BankAccountTypeCreateResponse
	rsp.FromModel(bankAccountType)

	helpers.WriteResponse(r, w, helpers.SuccessResponse(rsp))
}

func BankAccountTypeGet(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	id := ps.ByName("id")

	var bankAccountType models.BankAccountType
	err := bankAccountType.Get(id)
	if errors.Is(err, sql.ErrNoRows) {
		helpers.WriteResponse(r, w, helpers.ClientErrorResponse("Bank account type data not found"))
		return
	} else if err != nil {
		logging.Error("bank_account_type", "BankAccountTypeGet: error get", logging.Data{
			"id":    id,
			"error": err.Error(),
		})

		helpers.WriteResponse(r, w, helpers.ServerError("Failed to retrieve bank account type data"))
		return
	}

	var rsp BankAccountTypeGetResponse
	rsp.FromModel(bankAccountType)

	helpers.WriteResponse(r, w, helpers.SuccessResponse(rsp))
}

func BankAccountTypeList(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	var pr models.PaginationRequest
	err := pr.FromRequest(r)
	if err != nil {
		helpers.WriteResponse(r, w, helpers.BadRequestResponse())
		return
	}

	var f models.BankAccountTypeFilter
	f.FromRequest(r)

	var bankAccountType models.BankAccountType
	bankAccountTypes, total, err := bankAccountType.List(pr, &f)
	if err != nil {
		logging.Error("bank_account_type", "BankAccountTypeList: error list", logging.Data{
			"error": err.Error(),
		})

		helpers.WriteResponse(r, w, helpers.ServerError("Failed to retrieve bank account type data"))
		return
	}

	var tmp BankAccountTypeGetResponse
	rsp := tmp.FromModels(bankAccountTypes)

	totalPage := math.Ceil(float64(total) / float64(pr.PerPage))
	helpers.WriteResponse(r, w, helpers.PaginatedSuccessResponse(rsp, int64(pr.Page), int64(pr.PerPage), total,
		int64(totalPage)))
}

func BankAccountTypeUpdate(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	id := ps.ByName("id")

	var req BankAccountTypeUpdateRequest
	err := json.NewDecoder(r.Body).Decode(&req)
	if err != nil {
		helpers.WriteResponse(r, w, helpers.BadRequestResponse())
		return
	}

	var bankAccountType models.BankAccountType
	err = bankAccountType.Get(id)
	if errors.Is(err, sql.ErrNoRows) {
		helpers.WriteResponse(r, w, helpers.ClientErrorResponse("Bank account type data not found"))
		return
	} else if err != nil {
		logging.Error("bank_account_type", "BankAccountTypeUpdate: error get", logging.Data{
			"id":    id,
			"error": err.Error(),
		})

		helpers.WriteResponse(r, w, helpers.ServerError("Failed to retrieve bank account type data"))
		return
	}

	req.FillModel(&bankAccountType)
	err = bankAccountType.Update()
	if err != nil {
		logging.Error("bank_account_type", "BankAccountTypeUpdate: error update", logging.Data{
			"error": err.Error(),
			"body":  req,
		})

		helpers.WriteResponse(r, w, helpers.ServerError("Failed to save bank account type data"))
		return
	}

	var rsp BankAccountTypeUpdateResponse
	rsp.FromModel(bankAccountType)
	helpers.WriteResponse(r, w, helpers.SuccessResponse(rsp))

}

func BankAccountTypeDelete(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	id := ps.ByName("id")

	var bankAccountType models.BankAccountType
	err := bankAccountType.Get(id)
	if errors.Is(err, sql.ErrNoRows) {
		helpers.WriteResponse(r, w, helpers.ClientErrorResponse("Bank account type data not found"))
		return
	} else if err != nil {
		logging.Error("bank_account_type", "BankAccountTypeDelete: error get", logging.Data{
			"id":    id,
			"error": err.Error(),
		})

		helpers.WriteResponse(r, w, helpers.ServerError("Failed to retrieve bank account type data"))
		return
	}

	err = bankAccountType.Delete()
	if err != nil {
		logging.Error("bank_account_type", "BankAccountTypeDelete: error delete", logging.Data{
			"error": err.Error(),
			"id":    id,
		})

		helpers.WriteResponse(r, w, helpers.ServerError("Failed to delete bank account type data"))
		return
	}

	helpers.WriteResponse(r, w, helpers.SuccessResponse(nil))
}

func (r *BankAccountTypeCreateRequest) ToModel() models.BankAccountType {
	return models.BankAccountType{
		CreatedBy:       r.CreatedBy,
		BankAccountType: r.BankAccountType,
		BankAccountCode: r.BankAccountCode,
		MinBalance:      r.MinBalance,
		MaxBalance:      r.MaxBalance,
		Description:     r.Description,
		Sharia:          r.Sharia,
	}
}

func (r *BankAccountTypeUpdateRequest) FillModel(b *models.BankAccountType) {
	b.BankAccountType = r.BankAccountType
	b.BankAccountCode = r.BankAccountCode
	b.MinBalance = r.MinBalance
	b.MaxBalance = r.MaxBalance
	b.Description = r.Description
	b.Sharia = r.Sharia
	b.Active = r.Active
	b.UpdatedBy = &r.UpdatedBy
}

func (r *BankAccountTypeCreateResponse) FromModel(b models.BankAccountType) {
	r.Id = b.Id
	r.BankAccountType = b.BankAccountType
	r.BankAccountCode = b.BankAccountCode
	r.MinBalance = b.MinBalance
	r.MaxBalance = b.MaxBalance
	r.Description = b.Description
	r.Sharia = b.Sharia
}

func (r *BankAccountTypeGetResponse) FromModel(b models.BankAccountType) {
	r.Id = b.Id
	r.BankAccountType = b.BankAccountType
	r.BankAccountCode = b.BankAccountCode
	r.MinBalance = b.MinBalance
	r.MaxBalance = b.MaxBalance
	r.Description = b.Description
	r.Sharia = b.Sharia
	r.Active = b.Active
}

func (r *BankAccountTypeGetResponse) FromModels(ms []models.Model) []BankAccountTypeGetResponse {
	var rs []BankAccountTypeGetResponse
	for _, m := range ms {
		var r BankAccountTypeGetResponse

		bankAccountType := m.(*models.BankAccountType)
		r.FromModel(*bankAccountType)
		rs = append(rs, r)
	}

	return rs
}

func (r *BankAccountTypeUpdateResponse) FromModel(b models.BankAccountType) {
	r.Id = b.Id
	r.BankAccountType = b.BankAccountType
	r.BankAccountCode = b.BankAccountCode
	r.MinBalance = b.MinBalance
	r.MaxBalance = b.MaxBalance
	r.Description = b.Description
	r.Sharia = b.Sharia
	r.Active = b.Active
	r.UpdatedBy = *b.UpdatedBy
}
