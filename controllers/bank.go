package controllers

import (
	"database/sql"
	"dg-datamaster-service/helpers"
	"dg-datamaster-service/models"
	"dg-datamaster-service/services/logging"
	"encoding/json"
	"errors"
	"math"
	"net/http"

	"github.com/julienschmidt/httprouter"
)

type BankCreateRequest struct {
	CreatedBy    string `json:"created_by"`
	Name         string `json:"bank_name"`
	Alias        string `json:"bank_name_alias"`
	BICode       string `json:"bi_bank_code"`
	ClearingCode string `json:"clearing_bank_code" db:"clearing_bank_code"`
	RTGSCode     string `json:"rtgs_bank_code" db:"rtgs_bank_code"`
	CityCode     string `json:"bank_city_code" db:"bank_city_code"`
	Sharia       bool   `json:"is_sharia" db:"is_sharia"`
}

type BankCreateResponse struct {
	Id           int64  `json:"id"`
	Name         string `json:"bank_name"`
	Alias        string `json:"bank_name_alias"`
	BICode       string `json:"bi_bank_code"`
	ClearingCode string `json:"clearing_bank_code"`
	RTGSCode     string `json:"rtgs_bank_code"`
	CityCode     string `json:"bank_city_code"`
	Sharia       bool   `json:"is_sharia"`
}

type BankGetResponse struct {
	Id           int64  `json:"id"`
	Name         string `json:"bank_name"`
	Alias        string `json:"bank_name_alias"`
	BICode       string `json:"bi_bank_code"`
	ClearingCode string `json:"clearing_bank_code"`
	RTGSCode     string `json:"rtgs_bank_code"`
	CityCode     string `json:"bank_city_code"`
	Sharia       bool   `json:"is_sharia"`
	Active       bool   `json:"is_active"`
}

type BankUpdateRequest struct {
	Name         string `json:"bank_name"`
	Alias        string `json:"bank_name_alias"`
	BICode       string `json:"bi_bank_code"`
	ClearingCode string `json:"clearing_bank_code"`
	RTGSCode     string `json:"rtgs_bank_code"`
	CityCode     string `json:"bank_city_code"`
	Sharia       bool   `json:"is_sharia"`
	Active       bool   `json:"is_active"`
	UpdatedBy    string `json:"updated_by"`
}

type BankUpdateResponse struct {
	Id           int64  `json:"id"`
	Name         string `json:"bank_name"`
	Alias        string `json:"bank_name_alias"`
	BICode       string `json:"bi_bank_code"`
	ClearingCode string `json:"clearing_bank_code"`
	RTGSCode     string `json:"rtgs_bank_code"`
	CityCode     string `json:"bank_city_code"`
	Sharia       bool   `json:"is_sharia"`
	Active       bool   `json:"is_active"`
	UpdatedBy    string `json:"updated_by"`
}

func BankCreate(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	var req BankCreateRequest
	err := json.NewDecoder(r.Body).Decode(&req)
	if err != nil {
		helpers.WriteResponse(r, w, helpers.BadRequestResponse())
		return
	}

	bank := req.ToModel()
	err = bank.Create()
	if err != nil {
		logging.Error("bank", "BankCreate: error create", logging.Data{
			"error": err.Error(),
			"body":  req,
		})

		helpers.WriteResponse(r, w, helpers.ServerError("Gagal menyimpan data bank"))
		return
	}

	var rsp BankCreateResponse
	rsp.FromModel(bank)

	helpers.WriteResponse(r, w, helpers.SuccessResponse(rsp))
}

func BankGet(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	id := ps.ByName("id")

	var bank models.Bank
	err := bank.Get(id)
	if errors.Is(err, sql.ErrNoRows) {
		helpers.WriteResponse(r, w, helpers.ClientErrorResponse("Data bank tidak ditemukan"))
		return
	} else if err != nil {
		logging.Error("bank", "BankGet: error get", logging.Data{
			"id":    id,
			"error": err.Error(),
		})

		helpers.WriteResponse(r, w, helpers.ServerError("Gagal mengambil data bank"))
		return
	}

	var rsp BankGetResponse
	rsp.FromModel(bank)

	helpers.WriteResponse(r, w, helpers.SuccessResponse(rsp))
}

func BankList(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	var pr models.PaginationRequest
	err := pr.FromRequest(r)
	if err != nil {
		helpers.WriteResponse(r, w, helpers.BadRequestResponse())
		return
	}

	var f models.BankFilter
	f.FromRequest(r)

	var bank models.Bank
	banks, total, err := bank.List(pr, &f)
	if err != nil {
		logging.Error("bank", "BankList: error list", logging.Data{
			"error": err.Error(),
		})

		helpers.WriteResponse(r, w, helpers.ServerError("Gagal mengambil data bank"))
		return
	}

	var tmp BankGetResponse
	rsp := tmp.FromModels(banks)

	totalPage := math.Ceil(float64(total) / float64(pr.PerPage))
	helpers.WriteResponse(r, w, helpers.PaginatedSuccessResponse(rsp, int64(pr.Page), int64(pr.PerPage), total,
		int64(totalPage)))
}

func BankUpdate(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	id := ps.ByName("id")

	var req BankUpdateRequest
	err := json.NewDecoder(r.Body).Decode(&req)
	if err != nil {
		helpers.WriteResponse(r, w, helpers.BadRequestResponse())
		return
	}

	var bank models.Bank
	err = bank.Get(id)
	if errors.Is(err, sql.ErrNoRows) {
		helpers.WriteResponse(r, w, helpers.ClientErrorResponse("Data bank tidak ditemukan"))
		return
	} else if err != nil {
		logging.Error("bank", "BankUpdate: error get", logging.Data{
			"id":    id,
			"error": err.Error(),
		})

		helpers.WriteResponse(r, w, helpers.ServerError("Gagal mengambil data bank"))
		return
	}

	req.FillModel(&bank)
	err = bank.Update()
	if err != nil {
		logging.Error("bank", "BankUpdate: error update", logging.Data{
			"error": err.Error(),
			"body":  req,
		})

		helpers.WriteResponse(r, w, helpers.ServerError("Gagal menyimpan data bank"))
		return
	}

	var rsp BankUpdateResponse
	rsp.FromModel(bank)
	helpers.WriteResponse(r, w, helpers.SuccessResponse(rsp))

}

func BankDelete(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	id := ps.ByName("id")

	var bank models.Bank
	err := bank.Get(id)
	if errors.Is(err, sql.ErrNoRows) {
		helpers.WriteResponse(r, w, helpers.ClientErrorResponse("Data bank tidak ditemukan"))
		return
	} else if err != nil {
		logging.Error("bank", "BankDelete: error get", logging.Data{
			"id":    id,
			"error": err.Error(),
		})

		helpers.WriteResponse(r, w, helpers.ServerError("Gagal mengambil data bank"))
		return
	}

	err = bank.Delete()
	if err != nil {
		logging.Error("bank", "BankDelete: error delete", logging.Data{
			"error": err.Error(),
			"id":    id,
		})

		helpers.WriteResponse(r, w, helpers.ServerError("Gagal menghapus data bank"))
		return
	}

	helpers.WriteResponse(r, w, helpers.SuccessResponse(nil))
}

func (r *BankCreateRequest) ToModel() models.Bank {
	return models.Bank{
		CreatedBy:    r.CreatedBy,
		Name:         r.Name,
		Alias:        r.Alias,
		BICode:       r.BICode,
		ClearingCode: r.ClearingCode,
		RTGSCode:     r.RTGSCode,
		CityCode:     r.CityCode,
		Sharia:       r.Sharia,
	}
}

func (r *BankUpdateRequest) FillModel(b *models.Bank) {
	b.Name = r.Name
	b.Alias = r.Alias
	b.BICode = r.BICode
	b.ClearingCode = r.ClearingCode
	b.RTGSCode = r.RTGSCode
	b.CityCode = r.CityCode
	b.Sharia = r.Sharia
	b.Active = r.Active
	b.UpdatedBy = &r.UpdatedBy
}

func (r *BankCreateResponse) FromModel(b models.Bank) {
	r.Id = b.Id
	r.Name = b.Name
	r.Alias = b.Alias
	r.BICode = b.BICode
	r.ClearingCode = b.ClearingCode
	r.RTGSCode = b.RTGSCode
	r.CityCode = b.CityCode
	r.Sharia = b.Sharia
}

func (r *BankGetResponse) FromModel(b models.Bank) {
	r.Id = b.Id
	r.Name = b.Name
	r.Alias = b.Alias
	r.BICode = b.BICode
	r.ClearingCode = b.ClearingCode
	r.RTGSCode = b.RTGSCode
	r.CityCode = b.CityCode
	r.Sharia = b.Sharia
	r.Active = b.Active
}

func (r *BankGetResponse) FromModels(ms []models.Model) []BankGetResponse {
	var rs []BankGetResponse
	for _, m := range ms {
		var r BankGetResponse

		bank := m.(*models.Bank)

		r.FromModel(*bank)
		rs = append(rs, r)
	}

	return rs
}

func (r *BankUpdateResponse) FromModel(b models.Bank) {
	r.Id = b.Id
	r.Name = b.Name
	r.Alias = b.Alias
	r.BICode = b.BICode
	r.ClearingCode = b.ClearingCode
	r.RTGSCode = b.RTGSCode
	r.CityCode = b.CityCode
	r.Sharia = b.Sharia
	r.Active = b.Active
	r.UpdatedBy = *b.UpdatedBy
}
