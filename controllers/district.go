package controllers

import (
	"database/sql"
	"dg-datamaster-service/helpers"
	"dg-datamaster-service/models"
	"dg-datamaster-service/services/logging"
	"encoding/json"
	"errors"
	"github.com/julienschmidt/httprouter"
	"math"
	"net/http"
)

type DistrictCreateRequest struct {
	CreatedBy    string `json:"created_by"`
	CityId       int64  `json:"city_id"`
	DistrictCode string `json:"district_code"`
	DistrictName string `json:"district_name"`
}

type DistrictCreateResponse struct {
	Id           int64  `json:"id"`
	CityId       int64  `json:"city_id"`
	DistrictCode string `json:"district_code"`
	DistrictName string `json:"district_name"`
}

type DistrictUpdateRequest struct {
	CityId       int64  `json:"city_id"`
	DistrictCode string `json:"district_code"`
	DistrictName string `json:"district_name"`
	IsActive     bool   `json:"is_active"`
	UpdatedBy    string `json:"updated_by"`
}

type DistrictUpdateResponse struct {
	Id           int64  `json:"id"`
	CityId       int64  `json:"city_id"`
	DistrictCode string `json:"district_code"`
	DistrictName string `json:"district_name"`
	IsActive     bool   `json:"is_active"`
}

type DistrictGetResponse struct {
	Id           int64  `json:"id"`
	CityId       int64  `json:"city_id"`
	DistrictCode string `json:"district_code"`
	DistrictName string `json:"district_name"`
	IsActive     bool   `json:"is_active"`
}

func DistrictCreate(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	var req DistrictCreateRequest
	err := json.NewDecoder(r.Body).Decode(&req)
	if err != nil {
		helpers.WriteResponse(r, w, helpers.BadRequestResponse())
		return
	}

	district := req.ToModel()
	err = district.Create()
	if err != nil {
		logging.Error("district", "DistrictCreate: error create", logging.Data{
			"error": err.Error(),
			"body":  req,
		})

		helpers.WriteResponse(r, w, helpers.ServerError("Failed to save district data"))
		return
	}

	var rsp DistrictCreateResponse
	rsp.FromModel(district)

	helpers.WriteResponse(r, w, helpers.SuccessResponse(rsp))
}

func DistrictGet(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	id := ps.ByName("id")

	var district models.District
	err := district.Get(id)
	if errors.Is(err, sql.ErrNoRows) {
		helpers.WriteResponse(r, w, helpers.ClientErrorResponse("District data not found"))
		return
	} else if err != nil {
		logging.Error("district", "DistrictGet: error get", logging.Data{
			"id":    id,
			"error": err.Error(),
		})

		helpers.WriteResponse(r, w, helpers.ServerError("Failed to retrieve district data"))
		return
	}

	var rsp DistrictGetResponse
	rsp.FromModel(district)

	helpers.WriteResponse(r, w, helpers.SuccessResponse(rsp))
}

func DistrictList(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	var pr models.PaginationRequest
	err := pr.FromRequest(r)
	if err != nil {
		helpers.WriteResponse(r, w, helpers.BadRequestResponse())
		return
	}

	var f models.DistrictFilter
	f.FromRequest(r)

	var district models.District
	districts, total, err := district.List(pr, &f)
	if err != nil {
		logging.Error("district", "DistrictList: error list", logging.Data{
			"error": err.Error(),
		})

		helpers.WriteResponse(r, w, helpers.ServerError("Failed to retrieve district data"))
		return
	}

	var tmp DistrictGetResponse
	rsp := tmp.FromModels(districts)

	totalPage := math.Ceil(float64(total) / float64(pr.PerPage))
	helpers.WriteResponse(r, w, helpers.PaginatedSuccessResponse(rsp, int64(pr.Page), int64(pr.PerPage), total,
		int64(totalPage)))
}

func DistrictUpdate(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	id := ps.ByName("id")

	var req DistrictUpdateRequest
	err := json.NewDecoder(r.Body).Decode(&req)
	if err != nil {
		helpers.WriteResponse(r, w, helpers.BadRequestResponse())
		return
	}

	var district models.District
	err = district.Get(id)
	if errors.Is(err, sql.ErrNoRows) {
		helpers.WriteResponse(r, w, helpers.ClientErrorResponse("District data not found"))
		return
	} else if err != nil {
		logging.Error("district", "DistrictUpdate: error get", logging.Data{
			"id":    id,
			"error": err.Error(),
		})

		helpers.WriteResponse(r, w, helpers.ServerError("Failed to retrieve district data"))
		return
	}

	req.FillModel(&district)
	err = district.Update()
	if err != nil {
		logging.Error("district", "DistrictUpdate: error update", logging.Data{
			"error": err.Error(),
			"body":  req,
		})

		helpers.WriteResponse(r, w, helpers.ServerError("Failed to save district data"))
		return
	}

	var rsp DistrictUpdateResponse
	rsp.FromModel(district)
	helpers.WriteResponse(r, w, helpers.SuccessResponse(rsp))

}

func DistrictDelete(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	id := ps.ByName("id")

	var district models.District
	err := district.Get(id)
	if errors.Is(err, sql.ErrNoRows) {
		helpers.WriteResponse(r, w, helpers.ClientErrorResponse("District data not found"))
		return
	} else if err != nil {
		logging.Error("district", "DistrictDelete: error get", logging.Data{
			"id":    id,
			"error": err.Error(),
		})

		helpers.WriteResponse(r, w, helpers.ServerError("Failed to retrieve district data"))
		return
	}

	err = district.Delete()
	if err != nil {
		logging.Error("district", "DistrictDelete: error delete", logging.Data{
			"error": err.Error(),
			"id":    id,
		})

		helpers.WriteResponse(r, w, helpers.ServerError("Failed to delete district data"))
		return
	}

	helpers.WriteResponse(r, w, helpers.SuccessResponse(nil))
}

func (r *DistrictCreateRequest) ToModel() models.District {
	return models.District{
		CreatedBy:    r.CreatedBy,
		CityId:       r.CityId,
		DistrictCode: r.DistrictCode,
		DistrictName: r.DistrictName,
	}
}

func (r *DistrictUpdateRequest) FillModel(d *models.District) {
	d.CityId = r.CityId
	d.DistrictCode = r.DistrictCode
	d.DistrictName = r.DistrictName
	d.IsActive = r.IsActive
	d.UpdatedBy = &r.UpdatedBy
}

func (r *DistrictCreateResponse) FromModel(d models.District) {
	r.Id = d.Id
	r.CityId = d.CityId
	r.DistrictCode = d.DistrictCode
	r.DistrictName = d.DistrictName
}

func (r *DistrictGetResponse) FromModel(d models.District) {
	r.Id = d.Id
	r.CityId = d.CityId
	r.DistrictCode = d.DistrictCode
	r.DistrictName = d.DistrictName
	r.IsActive = d.IsActive
}

func (r *DistrictGetResponse) FromModels(ds []models.Model) []DistrictGetResponse {
	var rs []DistrictGetResponse
	for _, m := range ds {
		var r DistrictGetResponse

		district := m.(*models.District)
		r.FromModel(*district)
		rs = append(rs, r)
	}

	return rs
}

func (r *DistrictUpdateResponse) FromModel(d models.District) {
	r.Id = d.Id
	r.CityId = d.CityId
	r.DistrictCode = d.DistrictCode
	r.DistrictName = d.DistrictName
	r.IsActive = d.IsActive
}
