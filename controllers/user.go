package controllers

import (
	"database/sql"
	"dg-datamaster-service/helpers"
	"dg-datamaster-service/models"
	"dg-datamaster-service/services/auth"
	"dg-datamaster-service/services/logging"
	"encoding/json"
	"errors"
	"github.com/julienschmidt/httprouter"
	"golang.org/x/crypto/bcrypt"
	"net/http"
	"strconv"
)

type LoginRequest struct {
	Username string `json:"username"`
	Password string `json:"password"`
}

type LoginResponse struct {
	Username string `json:"username"`
	Token    string `json:"token"`
}

func Login(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	var req LoginRequest
	err := json.NewDecoder(r.Body).Decode(&req)
	if err != nil {
		helpers.WriteResponse(r, w, helpers.BadRequestResponse())
		return
	}

	var u models.User
	err = u.GetByUsername(req.Username)
	if errors.Is(err, sql.ErrNoRows) {
		helpers.WriteResponse(r, w, helpers.ClientErrorResponse("Username atau password salah"))
		return
	} else if err != nil {
		logging.Error("user", "Login: error get user", logging.Data{
			"error": err.Error(),
		})

		helpers.WriteResponse(r, w, helpers.ServerError("Terjadi kesalahan"))
		return
	}

	err = bcrypt.CompareHashAndPassword([]byte(u.Password), []byte(req.Password))
	if errors.Is(err, bcrypt.ErrMismatchedHashAndPassword) {
		helpers.WriteResponse(r, w, helpers.ClientErrorResponse("Username atau password salah"))

		return
	} else if err != nil {
		logging.Error("user", "Login: error compare password", logging.Data{
			"error": err.Error(),
		})

		helpers.WriteResponse(r, w, helpers.ServerError("Terjadi kesalahan"))
		return
	}

	token, err := auth.GenerateToken(strconv.Itoa(u.Id))
	if err != nil {
		helpers.WriteResponse(r, w, helpers.ServerError("Terjadi kesalahan"))
		return
	}

	rsp := LoginResponse{
		Username: u.Username,
		Token:    token,
	}

	helpers.WriteResponse(r, w, helpers.SuccessResponse(rsp))
}
