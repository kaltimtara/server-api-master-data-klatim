package controllers

import (
	"database/sql"
	"dg-datamaster-service/helpers"
	"dg-datamaster-service/models"
	"dg-datamaster-service/services/logging"
	"encoding/json"
	"errors"
	"github.com/julienschmidt/httprouter"
	"math"
	"net/http"
)

type TransactionOptionCreateRequest struct {
	CreatedBy         string `json:"created_by"`
	TrxTypeId         int64  `json:"trx_type_id"`
	TransactionOption string `json:"transaction_option"`
	Realtime          bool   `json:"is_realtime"`
}

type TransactionOptionCreateResponse struct {
	Id                int64  `json:"id"`
	TrxTypeId         int64  `json:"trx_type_id"`
	TransactionOption string `json:"transaction_option"`
	Realtime          bool   `json:"is_realtime"`
}

type TransactionOptionUpdateRequest struct {
	TrxTypeId         int64  `json:"trx_type_id"`
	TransactionOption string `json:"transaction_option"`
	Realtime          bool   `json:"is_realtime"`
	Active            bool   `json:"is_active"`
	UpdatedBy         string `json:"updated_by"`
}

type TransactionOptionUpdateResponse struct {
	Id                int64  `json:"id"`
	TrxTypeId         int64  `json:"trx_type_id"`
	TransactionOption string `json:"transaction_option"`
	Realtime          bool   `json:"is_realtime"`
	Active            bool   `json:"is_active"`
}

type TransactionOptionGetResponse struct {
	Id                int64  `json:"id"`
	TrxTypeId         int64  `json:"trx_type_id"`
	TransactionOption string `json:"transaction_option"`
	Realtime          bool   `json:"is_realtime"`
	Active            bool   `json:"is_active"`
}

func TransactionOptionCreate(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	var req TransactionOptionCreateRequest
	err := json.NewDecoder(r.Body).Decode(&req)
	if err != nil {
		helpers.WriteResponse(r, w, helpers.BadRequestResponse())
		return
	}

	transactionOption := req.ToModel()
	err = transactionOption.Create()
	if err != nil {
		logging.Error("transaction_option", "TransactionOptionCreate: error create", logging.Data{
			"error": err.Error(),
			"body":  req,
		})

		helpers.WriteResponse(r, w, helpers.ServerError("Failed to save transaction option data"))
		return
	}

	var rsp TransactionOptionCreateResponse
	rsp.FromModel(transactionOption)

	helpers.WriteResponse(r, w, helpers.SuccessResponse(rsp))
}

func TransactionOptionGet(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	id := ps.ByName("id")

	var transactionOption models.TransactionOption
	err := transactionOption.Get(id)
	if errors.Is(err, sql.ErrNoRows) {
		helpers.WriteResponse(r, w, helpers.ClientErrorResponse("Transaction option data not found"))
		return
	} else if err != nil {
		logging.Error("transaction_option", "TransactionOptionGet: error get", logging.Data{
			"id":    id,
			"error": err.Error(),
		})

		helpers.WriteResponse(r, w, helpers.ServerError("Failed to retrieve transaction option data"))
		return
	}

	var rsp TransactionOptionGetResponse
	rsp.FromModel(transactionOption)

	helpers.WriteResponse(r, w, helpers.SuccessResponse(rsp))
}

func TransactionOptionList(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	var pr models.PaginationRequest
	err := pr.FromRequest(r)
	if err != nil {
		helpers.WriteResponse(r, w, helpers.BadRequestResponse())
		return
	}

	var f models.TransactionOptionFilter
	f.FromRequest(r)

	var transactionOption models.TransactionOption
	transactionOptions, total, err := transactionOption.List(pr, &f)
	if err != nil {
		logging.Error("transaction_option", "TransactionOptionList: error list", logging.Data{
			"error": err.Error(),
		})

		helpers.WriteResponse(r, w, helpers.ServerError("Failed to retrieve transaction option data"))
		return
	}

	var tmp TransactionOptionGetResponse
	rsp := tmp.FromModels(transactionOptions)

	totalPage := math.Ceil(float64(total) / float64(pr.PerPage))
	helpers.WriteResponse(r, w, helpers.PaginatedSuccessResponse(rsp, int64(pr.Page), int64(pr.PerPage), total,
		int64(totalPage)))
}

func TransactionOptionUpdate(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	id := ps.ByName("id")

	var req TransactionOptionUpdateRequest
	err := json.NewDecoder(r.Body).Decode(&req)
	if err != nil {
		helpers.WriteResponse(r, w, helpers.BadRequestResponse())
		return
	}

	var transactionOption models.TransactionOption
	err = transactionOption.Get(id)
	if errors.Is(err, sql.ErrNoRows) {
		helpers.WriteResponse(r, w, helpers.ClientErrorResponse("Transaction option data not found"))
		return
	} else if err != nil {
		logging.Error("transaction_option", "TransactionOptionUpdate: error get", logging.Data{
			"id":    id,
			"error": err.Error(),
		})

		helpers.WriteResponse(r, w, helpers.ServerError("Failed to retrieve transaction option data"))
		return
	}

	req.FillModel(&transactionOption)
	err = transactionOption.Update()
	if err != nil {
		logging.Error("transaction_option", "TransactionOptionUpdate: error update", logging.Data{
			"error": err.Error(),
			"body":  req,
		})

		helpers.WriteResponse(r, w, helpers.ServerError("Failed to save transaction option data"))
		return
	}

	var rsp TransactionOptionUpdateResponse
	rsp.FromModel(transactionOption)
	helpers.WriteResponse(r, w, helpers.SuccessResponse(rsp))

}

func TransactionOptionDelete(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	id := ps.ByName("id")

	var transactionOption models.TransactionOption
	err := transactionOption.Get(id)
	if errors.Is(err, sql.ErrNoRows) {
		helpers.WriteResponse(r, w, helpers.ClientErrorResponse("Transaction option data not found"))
		return
	} else if err != nil {
		logging.Error("transaction_option", "TransactionOptionDelete: error get", logging.Data{
			"id":    id,
			"error": err.Error(),
		})

		helpers.WriteResponse(r, w, helpers.ServerError("Failed to retrieve transaction option data"))
		return
	}

	err = transactionOption.Delete()
	if err != nil {
		logging.Error("transaction_option", "TransactionOptionDelete: error delete", logging.Data{
			"error": err.Error(),
			"id":    id,
		})

		helpers.WriteResponse(r, w, helpers.ServerError("Failed to delete transaction option data"))
		return
	}

	helpers.WriteResponse(r, w, helpers.SuccessResponse(nil))
}

func (r *TransactionOptionCreateRequest) ToModel() models.TransactionOption {
	return models.TransactionOption{
		CreatedBy:         r.CreatedBy,
		TrxTypeId:         r.TrxTypeId,
		TransactionOption: r.TransactionOption,
		Realtime:          r.Realtime,
	}
}

func (r *TransactionOptionUpdateRequest) FillModel(b *models.TransactionOption) {
	b.TrxTypeId = r.TrxTypeId
	b.TransactionOption = r.TransactionOption
	b.Realtime = r.Realtime
	b.Active = r.Active
	b.UpdatedBy = &r.UpdatedBy
}

func (r *TransactionOptionCreateResponse) FromModel(b models.TransactionOption) {
	r.Id = b.Id
	r.TrxTypeId = b.TrxTypeId
	r.TransactionOption = b.TransactionOption
	r.Realtime = b.Realtime
}

func (r *TransactionOptionGetResponse) FromModel(b models.TransactionOption) {
	r.Id = b.Id
	r.TrxTypeId = b.TrxTypeId
	r.TransactionOption = b.TransactionOption
	r.Realtime = b.Realtime
	r.Active = b.Active
}

func (r *TransactionOptionGetResponse) FromModels(ms []models.Model) []TransactionOptionGetResponse {
	var rs []TransactionOptionGetResponse
	for _, m := range ms {
		var r TransactionOptionGetResponse

		transactionOption := m.(*models.TransactionOption)
		r.FromModel(*transactionOption)
		rs = append(rs, r)
	}

	return rs
}

func (r *TransactionOptionUpdateResponse) FromModel(b models.TransactionOption) {
	r.Id = b.Id
	r.TrxTypeId = b.TrxTypeId
	r.TransactionOption = b.TransactionOption
	r.Realtime = b.Realtime
	r.Active = b.Active
}
