package controllers

import (
	"database/sql"
	"dg-datamaster-service/helpers"
	"dg-datamaster-service/models"
	"dg-datamaster-service/services/logging"
	"encoding/json"
	"errors"
	"github.com/julienschmidt/httprouter"
	"math"
	"net/http"
)

type DepositTypeCreateRequest struct {
	CreatedBy               string   `json:"created_by"`
	DepositCode             string   `json:"deposit_code"`
	DepositDescription      string   `json:"deposit_description"`
	Currency                string   `json:"currency"`
	MinAmount               float64  `json:"min_amount"`
	MaxAmount               float64  `json:"max_amount"`
	AroRate                 float64  `json:"aro_rate"`
	DepositPeriod           int      `json:"deposit_period"`
	TaxFreq                 string   `json:"tax_freq"`
	DepositTax              float64  `json:"deposit_tax"`
	TaxRestitution          float64  `json:"tax_restitution"`
	TaxRate                 float64  `json:"tax_rate"`
	TaxDivider              int      `json:"tax_divider"`
	AroFlag                 string   `json:"aro_flag"`
	PeriodCode              string   `json:"period_code"`
	ShariaNisbahForBank     *float64 `json:"sharia_nisbah_for_bank"`
	ShariaNisbahForCustomer *float64 `json:"sharia_nisbah_for_customer"`
	IsSharia                bool     `json:"is_sharia"`
}

type DepositTypeCreateResponse struct {
	Id                      int64    `json:"id"`
	DepositCode             string   `json:"deposit_code"`
	DepositDescription      string   `json:"deposit_description"`
	Currency                string   `json:"currency"`
	MinAmount               float64  `json:"min_amount"`
	MaxAmount               float64  `json:"max_amount"`
	AroRate                 float64  `json:"aro_rate"`
	DepositPeriod           int      `json:"deposit_period"`
	TaxFreq                 string   `json:"tax_freq"`
	DepositTax              float64  `json:"deposit_tax"`
	TaxRestitution          float64  `json:"tax_restitution"`
	TaxRate                 float64  `json:"tax_rate"`
	TaxDivider              int      `json:"tax_divider"`
	AroFlag                 string   `json:"aro_flag"`
	PeriodCode              string   `json:"period_code"`
	ShariaNisbahForBank     *float64 `json:"sharia_nisbah_for_bank"`
	ShariaNisbahForCustomer *float64 `json:"sharia_nisbah_for_customer"`
	IsSharia                bool     `json:"is_sharia"`
	IsActive                bool     `json:"is_active"`
}

type DepositTypeUpdateRequest struct {
	DepositCode             string   `json:"deposit_code"`
	DepositDescription      string   `json:"deposit_description"`
	Currency                string   `json:"currency"`
	MinAmount               float64  `json:"min_amount"`
	MaxAmount               float64  `json:"max_amount"`
	AroRate                 float64  `json:"aro_rate"`
	DepositPeriod           int      `json:"deposit_period"`
	TaxFreq                 string   `json:"tax_freq"`
	DepositTax              float64  `json:"deposit_tax"`
	TaxRestitution          float64  `json:"tax_restitution"`
	TaxRate                 float64  `json:"tax_rate"`
	TaxDivider              int      `json:"tax_divider"`
	AroFlag                 string   `json:"aro_flag"`
	PeriodCode              string   `json:"period_code"`
	ShariaNisbahForBank     *float64 `json:"sharia_nisbah_for_bank"`
	ShariaNisbahForCustomer *float64 `json:"sharia_nisbah_for_customer"`
	IsSharia                bool     `json:"is_sharia"`
	IsActive                bool     `json:"is_active"`
	UpdatedBy               string   `json:"updated_by"`
}

type DepositTypeUpdateResponse struct {
	Id                      int64    `json:"id"`
	DepositCode             string   `json:"deposit_code"`
	DepositDescription      string   `json:"deposit_description"`
	Currency                string   `json:"currency"`
	MinAmount               float64  `json:"min_amount"`
	MaxAmount               float64  `json:"max_amount"`
	AroRate                 float64  `json:"aro_rate"`
	DepositPeriod           int      `json:"deposit_period"`
	TaxFreq                 string   `json:"tax_freq"`
	DepositTax              float64  `json:"deposit_tax"`
	TaxRestitution          float64  `json:"tax_restitution"`
	TaxRate                 float64  `json:"tax_rate"`
	TaxDivider              int      `json:"tax_divider"`
	AroFlag                 string   `json:"aro_flag"`
	PeriodCode              string   `json:"period_code"`
	ShariaNisbahForBank     *float64 `json:"sharia_nisbah_for_bank"`
	ShariaNisbahForCustomer *float64 `json:"sharia_nisbah_for_customer"`
	IsSharia                bool     `json:"is_sharia"`
	IsActive                bool     `json:"is_active"`
}

type DepositTypeGetResponse struct {
	Id                      int64    `json:"id"`
	DepositCode             string   `json:"deposit_code"`
	DepositDescription      string   `json:"deposit_description"`
	Currency                string   `json:"currency"`
	MinAmount               float64  `json:"min_amount"`
	MaxAmount               float64  `json:"max_amount"`
	AroRate                 float64  `json:"aro_rate"`
	DepositPeriod           int      `json:"deposit_period"`
	TaxFreq                 string   `json:"tax_freq"`
	DepositTax              float64  `json:"deposit_tax"`
	TaxRestitution          float64  `json:"tax_restitution"`
	TaxRate                 float64  `json:"tax_rate"`
	TaxDivider              int      `json:"tax_divider"`
	AroFlag                 string   `json:"aro_flag"`
	PeriodCode              string   `json:"period_code"`
	ShariaNisbahForBank     *float64 `json:"sharia_nisbah_for_bank"`
	ShariaNisbahForCustomer *float64 `json:"sharia_nisbah_for_customer"`
	IsSharia                bool     `json:"is_sharia"`
	IsActive                bool     `json:"is_active"`
}

type DepositTypeListResponse struct {
	Data      []DepositTypeGetResponse `json:"data"`
	Page      int64                    `json:"page"`
	PerPage   int64                    `json:"per_page"`
	Total     int64                    `json:"total"`
	TotalPage int64                    `json:"total_page"`
}

func DepositTypeCreate(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	var req DepositTypeCreateRequest
	err := json.NewDecoder(r.Body).Decode(&req)
	if err != nil {
		helpers.WriteResponse(r, w, helpers.BadRequestResponse())
		return
	}

	depositType := req.ToModel()
	err = depositType.Create()
	if err != nil {
		logging.Error("depositType", "DepositTypeCreate: error create", logging.Data{
			"error": err.Error(),
			"body":  req,
		})

		helpers.WriteResponse(r, w, helpers.ServerError("Failed to save deposit type data"))
		return
	}

	var rsp DepositTypeCreateResponse
	rsp.FromModel(depositType)

	helpers.WriteResponse(r, w, helpers.SuccessResponse(rsp))
}

func DepositTypeGet(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	id := ps.ByName("id")

	var depositType models.DepositType
	err := depositType.Get(id)
	if errors.Is(err, sql.ErrNoRows) {
		helpers.WriteResponse(r, w, helpers.ClientErrorResponse("Deposit type data not found"))
		return
	} else if err != nil {
		logging.Error("depositType", "DepositTypeGet: error get", logging.Data{
			"id":    id,
			"error": err.Error(),
		})

		helpers.WriteResponse(r, w, helpers.ServerError("Failed to retrieve deposit type data"))
		return
	}

	var rsp DepositTypeGetResponse
	rsp.FromModel(depositType)

	helpers.WriteResponse(r, w, helpers.SuccessResponse(rsp))
}

func DepositTypeList(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	var pr models.PaginationRequest
	err := pr.FromRequest(r)
	if err != nil {
		helpers.WriteResponse(r, w, helpers.BadRequestResponse())
		return
	}

	var f models.DepositTypeFilter
	f.FromRequest(r)

	var depositType models.DepositType
	depositTypes, total, err := depositType.List(pr, &f)
	if err != nil {
		logging.Error("depositType", "DepositTypeList: error list", logging.Data{
			"error": err.Error(),
		})

		helpers.WriteResponse(r, w, helpers.ServerError("Failed to retrieve deposit type data"))
		return
	}

	var tmp DepositTypeGetResponse
	rsp := tmp.FromModels(depositTypes)

	totalPage := math.Ceil(float64(total) / float64(pr.PerPage))
	helpers.WriteResponse(r, w, helpers.PaginatedSuccessResponse(rsp, int64(pr.Page), int64(pr.PerPage), total,
		int64(totalPage)))
}

func DepositTypeUpdate(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	id := ps.ByName("id")

	var req DepositTypeUpdateRequest
	err := json.NewDecoder(r.Body).Decode(&req)
	if err != nil {
		helpers.WriteResponse(r, w, helpers.BadRequestResponse())
		return
	}

	var depositType models.DepositType
	err = depositType.Get(id)
	if errors.Is(err, sql.ErrNoRows) {
		helpers.WriteResponse(r, w, helpers.ClientErrorResponse("Deposit type data not found"))
		return
	} else if err != nil {
		logging.Error("depositType", "DepositTypeUpdate: error get", logging.Data{
			"id":    id,
			"error": err.Error(),
		})

		helpers.WriteResponse(r, w, helpers.ServerError("Failed to retrieve deposit type data"))
		return
	}

	req.FillModel(&depositType)
	err = depositType.Update()
	if err != nil {
		logging.Error("depositType", "DepositTypeUpdate: error update", logging.Data{
			"error": err.Error(),
			"body":  req,
		})

		helpers.WriteResponse(r, w, helpers.ServerError("Failed to save deposit type data"))
		return
	}

	var rsp DepositTypeUpdateResponse
	rsp.FromModel(depositType)
	helpers.WriteResponse(r, w, helpers.SuccessResponse(rsp))

}

func DepositTypeDelete(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	id := ps.ByName("id")

	var depositType models.DepositType
	err := depositType.Get(id)
	if errors.Is(err, sql.ErrNoRows) {
		helpers.WriteResponse(r, w, helpers.ClientErrorResponse("Deposit type data not found"))
		return
	} else if err != nil {
		logging.Error("depositType", "DepositTypeDelete: error get", logging.Data{
			"id":    id,
			"error": err.Error(),
		})

		helpers.WriteResponse(r, w, helpers.ServerError("Failed to retrieve deposit type data"))
		return
	}

	err = depositType.Delete()
	if err != nil {
		logging.Error("depositType", "DepositTypeDelete: error delete", logging.Data{
			"error": err.Error(),
			"id":    id,
		})

		helpers.WriteResponse(r, w, helpers.ServerError("Failed to delete deposit type data"))
		return
	}

	helpers.WriteResponse(r, w, helpers.SuccessResponse(nil))
}

func (r *DepositTypeCreateRequest) ToModel() models.DepositType {
	return models.DepositType{
		CreatedBy:               r.CreatedBy,
		DepositCode:             r.DepositCode,
		DepositDescription:      r.DepositDescription,
		Currency:                r.Currency,
		MinAmount:               r.MinAmount,
		MaxAmount:               r.MaxAmount,
		AroRate:                 r.AroRate,
		DepositPeriod:           r.DepositPeriod,
		TaxFreq:                 r.TaxFreq,
		DepositTax:              r.DepositTax,
		TaxRestitution:          r.TaxRestitution,
		TaxRate:                 r.TaxRate,
		TaxDivider:              r.TaxDivider,
		AroFlag:                 r.AroFlag,
		PeriodCode:              r.PeriodCode,
		ShariaNisbahForBank:     r.ShariaNisbahForBank,
		ShariaNisbahForCustomer: r.ShariaNisbahForCustomer,
		IsSharia:                r.IsSharia,
	}
}

func (r *DepositTypeCreateResponse) FromModel(d models.DepositType) {
	r.Id = d.Id
	r.DepositCode = d.DepositCode
	r.DepositDescription = d.DepositDescription
	r.Currency = d.Currency
	r.MinAmount = d.MinAmount
	r.MaxAmount = d.MaxAmount
	r.AroRate = d.AroRate
	r.DepositPeriod = d.DepositPeriod
	r.TaxFreq = d.TaxFreq
	r.DepositTax = d.DepositTax
	r.TaxRestitution = d.TaxRestitution
	r.TaxRate = d.TaxRate
	r.TaxDivider = d.TaxDivider
	r.AroFlag = d.AroFlag
	r.PeriodCode = d.PeriodCode
	r.ShariaNisbahForBank = d.ShariaNisbahForBank
	r.ShariaNisbahForCustomer = d.ShariaNisbahForCustomer
	r.IsSharia = d.IsSharia
	r.IsActive = d.IsActive
}

func (r *DepositTypeUpdateRequest) FillModel(d *models.DepositType) {
	d.DepositCode = r.DepositCode
	d.DepositDescription = r.DepositDescription
	d.Currency = r.Currency
	d.MinAmount = r.MinAmount
	d.MaxAmount = r.MaxAmount
	d.AroRate = r.AroRate
	d.DepositPeriod = r.DepositPeriod
	d.TaxFreq = r.TaxFreq
	d.DepositTax = r.DepositTax
	d.TaxRestitution = r.TaxRestitution
	d.TaxRate = r.TaxRate
	d.TaxDivider = r.TaxDivider
	d.AroFlag = r.AroFlag
	d.PeriodCode = r.PeriodCode
	d.ShariaNisbahForBank = r.ShariaNisbahForBank
	d.ShariaNisbahForCustomer = r.ShariaNisbahForCustomer
	d.IsSharia = r.IsSharia
	d.IsActive = r.IsActive
	d.UpdatedBy = &r.UpdatedBy
}

func (r *DepositTypeUpdateResponse) FromModel(d models.DepositType) {
	r.Id = d.Id
	r.DepositCode = d.DepositCode
	r.DepositDescription = d.DepositDescription
	r.Currency = d.Currency
	r.MinAmount = d.MinAmount
	r.MaxAmount = d.MaxAmount
	r.AroRate = d.AroRate
	r.DepositPeriod = d.DepositPeriod
	r.TaxFreq = d.TaxFreq
	r.DepositTax = d.DepositTax
	r.TaxRestitution = d.TaxRestitution
	r.TaxRate = d.TaxRate
	r.TaxDivider = d.TaxDivider
	r.AroFlag = d.AroFlag
	r.PeriodCode = d.PeriodCode
	r.ShariaNisbahForBank = d.ShariaNisbahForBank
	r.ShariaNisbahForCustomer = d.ShariaNisbahForCustomer
	r.IsSharia = d.IsSharia
	r.IsActive = d.IsActive
}

func (r *DepositTypeGetResponse) FromModel(d models.DepositType) {
	r.Id = d.Id
	r.DepositCode = d.DepositCode
	r.DepositDescription = d.DepositDescription
	r.Currency = d.Currency
	r.MinAmount = d.MinAmount
	r.MaxAmount = d.MaxAmount
	r.AroRate = d.AroRate
	r.DepositPeriod = d.DepositPeriod
	r.TaxFreq = d.TaxFreq
	r.DepositTax = d.DepositTax
	r.TaxRestitution = d.TaxRestitution
	r.TaxRate = d.TaxRate
	r.TaxDivider = d.TaxDivider
	r.AroFlag = d.AroFlag
	r.PeriodCode = d.PeriodCode
	r.ShariaNisbahForBank = d.ShariaNisbahForBank
	r.ShariaNisbahForCustomer = d.ShariaNisbahForCustomer
	r.IsSharia = d.IsSharia
	r.IsActive = d.IsActive
}

func (r *DepositTypeGetResponse) FromModels(ds []models.Model) []DepositTypeGetResponse {
	var rs []DepositTypeGetResponse
	for _, m := range ds {
		var r DepositTypeGetResponse

		depositType := m.(*models.DepositType)
		r.FromModel(*depositType)
		rs = append(rs, r)
	}

	return rs
}
