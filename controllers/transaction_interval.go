package controllers

import (
	"database/sql"
	"dg-datamaster-service/helpers"
	"dg-datamaster-service/models"
	"dg-datamaster-service/services/logging"
	"encoding/json"
	"errors"
	"github.com/julienschmidt/httprouter"
	"math"
	"net/http"
)

type TransactionIntervalCreateRequest struct {
	CreatedBy           string `json:"created_by"`
	TrxTypeId           int64  `json:"trx_type_id"`
	TransactionInterval string `json:"transaction_interval"`
}

type TransactionIntervalCreateResponse struct {
	Id                  int64  `json:"id"`
	TrxTypeId           int64  `json:"trx_type_id"`
	TransactionInterval string `json:"transaction_interval"`
}

type TransactionIntervalUpdateRequest struct {
	TrxTypeId           int64  `json:"trx_type_id"`
	TransactionInterval string `json:"transaction_interval"`
	Active              bool   `json:"is_active"`
	UpdatedBy           string `json:"updated_by"`
}

type TransactionIntervalUpdateResponse struct {
	Id                  int64  `json:"id"`
	TrxTypeId           int64  `json:"trx_type_id"`
	TransactionInterval string `json:"transaction_interval"`
	Active              bool   `json:"is_active"`
}

type TransactionIntervalGetResponse struct {
	Id                  int64  `json:"id"`
	TrxTypeId           int64  `json:"trx_type_id"`
	TransactionInterval string `json:"transaction_interval"`
	Active              bool   `json:"is_active"`
}

func TransactionIntervalCreate(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	var req TransactionIntervalCreateRequest
	err := json.NewDecoder(r.Body).Decode(&req)
	if err != nil {
		helpers.WriteResponse(r, w, helpers.BadRequestResponse())
		return
	}

	transactionInterval := req.ToModel()
	err = transactionInterval.Create()
	if err != nil {
		logging.Error("transaction_interval", "TransactionIntervalCreate: error create", logging.Data{
			"error": err.Error(),
			"body":  req,
		})

		helpers.WriteResponse(r, w, helpers.ServerError("Failed to save transaction interval data"))
		return
	}

	var rsp TransactionIntervalCreateResponse
	rsp.FromModel(transactionInterval)

	helpers.WriteResponse(r, w, helpers.SuccessResponse(rsp))
}

func TransactionIntervalGet(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	id := ps.ByName("id")

	var transactionInterval models.TransactionInterval
	err := transactionInterval.Get(id)
	if errors.Is(err, sql.ErrNoRows) {
		helpers.WriteResponse(r, w, helpers.ClientErrorResponse("Transaction interval data not found"))
		return
	} else if err != nil {
		logging.Error("transaction_interval", "TransactionIntervalGet: error get", logging.Data{
			"id":    id,
			"error": err.Error(),
		})

		helpers.WriteResponse(r, w, helpers.ServerError("Failed to retrieve transaction interval data"))
		return
	}

	var rsp TransactionIntervalGetResponse
	rsp.FromModel(transactionInterval)

	helpers.WriteResponse(r, w, helpers.SuccessResponse(rsp))
}

func TransactionIntervalList(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	var pr models.PaginationRequest
	err := pr.FromRequest(r)
	if err != nil {
		helpers.WriteResponse(r, w, helpers.BadRequestResponse())
		return
	}

	var f models.TransactionIntervalFilter
	f.FromRequest(r)

	var transactionInterval models.TransactionInterval
	transactionIntervals, total, err := transactionInterval.List(pr, &f)
	if err != nil {
		logging.Error("transaction_interval", "TransactionIntervalList: error list", logging.Data{
			"error": err.Error(),
		})

		helpers.WriteResponse(r, w, helpers.ServerError("Failed to retrieve transaction interval data"))
		return
	}

	var tmp TransactionIntervalGetResponse
	rsp := tmp.FromModels(transactionIntervals)

	totalPage := math.Ceil(float64(total) / float64(pr.PerPage))
	helpers.WriteResponse(r, w, helpers.PaginatedSuccessResponse(rsp, int64(pr.Page), int64(pr.PerPage), total,
		int64(totalPage)))
}

func TransactionIntervalUpdate(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	id := ps.ByName("id")

	var req TransactionIntervalUpdateRequest
	err := json.NewDecoder(r.Body).Decode(&req)
	if err != nil {
		helpers.WriteResponse(r, w, helpers.BadRequestResponse())
		return
	}

	var transactionInterval models.TransactionInterval
	err = transactionInterval.Get(id)
	if errors.Is(err, sql.ErrNoRows) {
		helpers.WriteResponse(r, w, helpers.ClientErrorResponse("Transaction interval data not found"))
		return
	} else if err != nil {
		logging.Error("transaction_interval", "TransactionIntervalUpdate: error get", logging.Data{
			"id":    id,
			"error": err.Error(),
		})

		helpers.WriteResponse(r, w, helpers.ServerError("Failed to retrieve transaction interval data"))
		return
	}

	req.FillModel(&transactionInterval)
	err = transactionInterval.Update()
	if err != nil {
		logging.Error("transaction_interval", "TransactionIntervalUpdate: error update", logging.Data{
			"error": err.Error(),
			"body":  req,
		})

		helpers.WriteResponse(r, w, helpers.ServerError("Failed to save transaction interval data"))
		return
	}

	var rsp TransactionIntervalUpdateResponse
	rsp.FromModel(transactionInterval)
	helpers.WriteResponse(r, w, helpers.SuccessResponse(rsp))
}

func TransactionIntervalDelete(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	id := ps.ByName("id")

	var transactionInterval models.TransactionInterval
	err := transactionInterval.Get(id)
	if errors.Is(err, sql.ErrNoRows) {
		helpers.WriteResponse(r, w, helpers.ClientErrorResponse("Transaction interval data not found"))
		return
	} else if err != nil {
		logging.Error("transaction_interval", "TransactionIntervalDelete: error get", logging.Data{
			"id":    id,
			"error": err.Error(),
		})

		helpers.WriteResponse(r, w, helpers.ServerError("Failed to retrieve transaction interval data"))
		return
	}

	err = transactionInterval.Delete()
	if err != nil {
		logging.Error("transaction_interval", "TransactionIntervalDelete: error delete", logging.Data{
			"error": err.Error(),
			"id":    id,
		})

		helpers.WriteResponse(r, w, helpers.ServerError("Failed to delete transaction interval data"))
		return
	}

	helpers.WriteResponse(r, w, helpers.SuccessResponse(nil))
}

func (r *TransactionIntervalCreateRequest) ToModel() models.TransactionInterval {
	return models.TransactionInterval{
		CreatedBy:           r.CreatedBy,
		TrxTypeId:           r.TrxTypeId,
		TransactionInterval: r.TransactionInterval,
	}
}

func (r *TransactionIntervalUpdateRequest) FillModel(b *models.TransactionInterval) {
	b.TrxTypeId = r.TrxTypeId
	b.TransactionInterval = r.TransactionInterval
	b.Active = r.Active
	b.UpdatedBy = &r.UpdatedBy
}

func (r *TransactionIntervalCreateResponse) FromModel(b models.TransactionInterval) {
	r.Id = b.Id
	r.TrxTypeId = b.TrxTypeId
	r.TransactionInterval = b.TransactionInterval
}

func (r *TransactionIntervalGetResponse) FromModel(b models.TransactionInterval) {
	r.Id = b.Id
	r.TrxTypeId = b.TrxTypeId
	r.TransactionInterval = b.TransactionInterval
	r.Active = b.Active
}

func (r *TransactionIntervalGetResponse) FromModels(ms []models.Model) []TransactionIntervalGetResponse {
	var rs []TransactionIntervalGetResponse
	for _, m := range ms {
		var r TransactionIntervalGetResponse

		transactionInterval := m.(*models.TransactionInterval)
		r.FromModel(*transactionInterval)
		rs = append(rs, r)
	}

	return rs
}

func (r *TransactionIntervalUpdateResponse) FromModel(b models.TransactionInterval) {
	r.Id = b.Id
	r.TrxTypeId = b.TrxTypeId
	r.TransactionInterval = b.TransactionInterval
	r.Active = b.Active
}
