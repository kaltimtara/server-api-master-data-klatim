package controllers

import (
	"database/sql"
	"dg-datamaster-service/helpers"
	"dg-datamaster-service/models"
	"dg-datamaster-service/services/logging"
	"encoding/json"
	"errors"
	"math"
	"net/http"

	"github.com/julienschmidt/httprouter"
)

type TransactionLimitCreateRequest struct {
	CreatedBy         string  `json:"created_by"`
	TrxTypeId         int64   `json:"trx_type_id"`
	DailyTrxMax       int64   `json:"daily_trx_max"`
	DailyTrxAmountMax float64 `json:"daily_trx_amount_max"`
	TrxAmountMin      float64 `json:"trx_amount_min"`
	TrxAmountMax      float64 `json:"trx_amount_max"`
}

type TransactionLimitCreateResponse struct {
	Id                int64   `json:"id"`
	TrxTypeId         int64   `json:"trx_type_id"`
	DailyTrxMax       int64   `json:"daily_trx_max"`
	DailyTrxAmountMax float64 `json:"daily_trx_amount_max"`
	TrxAmountMin      float64 `json:"trx_amount_min"`
	TrxAmountMax      float64 `json:"trx_amount_max"`
}

type TransactionLimitUpdateRequest struct {
	TrxTypeId         int64   `json:"trx_type_id"`
	DailyTrxMax       int64   `json:"daily_trx_max"`
	DailyTrxAmountMax float64 `json:"daily_trx_amount_max"`
	TrxAmountMin      float64 `json:"trx_amount_min"`
	TrxAmountMax      float64 `json:"trx_amount_max"`
	Active            bool    `json:"is_active"`
	UpdatedBy         string  `json:"updated_by"`
}

type TransactionLimitUpdateResponse struct {
	Id                int64   `json:"id"`
	TrxTypeId         int64   `json:"trx_type_id"`
	DailyTrxMax       int64   `json:"daily_trx_max"`
	DailyTrxAmountMax float64 `json:"daily_trx_amount_max"`
	TrxAmountMin      float64 `json:"trx_amount_min"`
	TrxAmountMax      float64 `json:"trx_amount_max"`
	Active            bool    `json:"is_active"`
	UpdatedBy         string  `json:"updated_by"`
}

type TransactionLimitGetResponse struct {
	Id                int64   `json:"id"`
	TrxTypeId         int64   `json:"trx_type_id"`
	DailyTrxMax       int64   `json:"daily_trx_max"`
	DailyTrxAmountMax float64 `json:"daily_trx_amount_max"`
	TrxAmountMin      float64 `json:"trx_amount_min"`
	TrxAmountMax      float64 `json:"trx_amount_max"`
	Active            bool    `json:"is_active"`
}

func TransactionLimitCreate(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	var req TransactionLimitCreateRequest
	err := json.NewDecoder(r.Body).Decode(&req)
	if err != nil {
		helpers.WriteResponse(r, w, helpers.BadRequestResponse())
		return
	}

	transactionLimit := req.ToModel()
	err = transactionLimit.Create()
	if err != nil {
		logging.Error("transaction_limit", "TransactionLimitCreate: error create", logging.Data{
			"error": err.Error(),
			"body":  req,
		})

		helpers.WriteResponse(r, w, helpers.ServerError("Failed to save transaction limit data"))
		return
	}

	var rsp TransactionLimitCreateResponse
	rsp.FromModel(transactionLimit)

	helpers.WriteResponse(r, w, helpers.SuccessResponse(rsp))
}

func TransactionLimitGet(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	id := ps.ByName("id")

	var transactionLimit models.TransactionLimit
	err := transactionLimit.Get(id)
	if errors.Is(err, sql.ErrNoRows) {
		helpers.WriteResponse(r, w, helpers.ClientErrorResponse("Transaction limit data not found"))
		return
	} else if err != nil {
		logging.Error("transaction_limit", "TransactionLimitGet: error get", logging.Data{
			"id":    id,
			"error": err.Error(),
		})

		helpers.WriteResponse(r, w, helpers.ServerError("Failed to retrieve transaction limit data"))
		return
	}

	var rsp TransactionLimitGetResponse
	rsp.FromModel(transactionLimit)

	helpers.WriteResponse(r, w, helpers.SuccessResponse(rsp))
}

func TransactionLimitList(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	var pr models.PaginationRequest
	err := pr.FromRequest(r)
	if err != nil {
		helpers.WriteResponse(r, w, helpers.BadRequestResponse())
		return
	}

	var f models.TransactionLimitFilter
	f.FromRequest(r)

	var transactionLimit models.TransactionLimit
	transactionLimits, total, err := transactionLimit.List(pr, &f)
	if err != nil {
		logging.Error("transaction_limit", "TransactionLimitList: error list", logging.Data{
			"error": err.Error(),
		})

		helpers.WriteResponse(r, w, helpers.ServerError("Failed to retrieve transaction limit data"))
		return
	}

	var tmp TransactionLimitGetResponse
	rsp := tmp.FromModels(transactionLimits)

	totalPage := math.Ceil(float64(total) / float64(pr.PerPage))
	helpers.WriteResponse(r, w, helpers.PaginatedSuccessResponse(rsp, int64(pr.Page), int64(pr.PerPage), total,
		int64(totalPage)))
}

func TransactionLimitUpdate(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	id := ps.ByName("id")

	var req TransactionLimitUpdateRequest
	err := json.NewDecoder(r.Body).Decode(&req)
	if err != nil {
		helpers.WriteResponse(r, w, helpers.BadRequestResponse())
		return
	}

	var transactionLimit models.TransactionLimit
	err = transactionLimit.Get(id)
	if errors.Is(err, sql.ErrNoRows) {
		helpers.WriteResponse(r, w, helpers.ClientErrorResponse("Transaction limit data not found"))
		return
	} else if err != nil {
		logging.Error("transaction_limit", "TransactionLimitUpdate: error get", logging.Data{
			"id":    id,
			"error": err.Error(),
		})

		helpers.WriteResponse(r, w, helpers.ServerError("Failed to retrieve transaction limit data"))
		return
	}

	req.FillModel(&transactionLimit)
	err = transactionLimit.Update()
	if err != nil {
		logging.Error("transaction_limit", "TransactionLimitUpdate: error update", logging.Data{
			"error": err.Error(),
			"body":  req,
		})

		helpers.WriteResponse(r, w, helpers.ServerError("Failed to save transaction limit data"))
		return
	}

	var rsp TransactionLimitUpdateResponse
	rsp.FromModel(transactionLimit)
	helpers.WriteResponse(r, w, helpers.SuccessResponse(rsp))

}

func TransactionLimitDelete(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	id := ps.ByName("id")

	var transactionLimit models.TransactionLimit
	err := transactionLimit.Get(id)
	if errors.Is(err, sql.ErrNoRows) {
		helpers.WriteResponse(r, w, helpers.ClientErrorResponse("Transaction limit data not found"))
		return
	} else if err != nil {
		logging.Error("transaction_limit", "TransactionLimitDelete: error get", logging.Data{
			"id":    id,
			"error": err.Error(),
		})

		helpers.WriteResponse(r, w, helpers.ServerError("Failed to retrieve transaction limit data"))
		return
	}

	err = transactionLimit.Delete()
	if err != nil {
		logging.Error("transaction_limit", "TransactionLimitDelete: error delete", logging.Data{
			"error": err.Error(),
			"id":    id,
		})

		helpers.WriteResponse(r, w, helpers.ServerError("Failed to delete transaction limit data"))
		return
	}

	helpers.WriteResponse(r, w, helpers.SuccessResponse(nil))
}

func (r *TransactionLimitCreateRequest) ToModel() models.TransactionLimit {
	return models.TransactionLimit{
		CreatedBy:         r.CreatedBy,
		TrxTypeId:         r.TrxTypeId,
		DailyTrxMax:       r.DailyTrxMax,
		DailyTrxAmountMax: r.DailyTrxAmountMax,
		TrxAmountMin:      r.TrxAmountMin,
		TrxAmountMax:      r.TrxAmountMax,
	}
}

func (r *TransactionLimitUpdateRequest) FillModel(b *models.TransactionLimit) {
	b.TrxTypeId = r.TrxTypeId
	b.DailyTrxMax = r.DailyTrxMax
	b.DailyTrxAmountMax = r.DailyTrxAmountMax
	b.TrxAmountMin = r.TrxAmountMin
	b.TrxAmountMax = r.TrxAmountMax
	b.Active = r.Active
	b.UpdatedBy = &r.UpdatedBy
}

func (r *TransactionLimitCreateResponse) FromModel(b models.TransactionLimit) {
	r.Id = b.Id
	r.TrxTypeId = b.TrxTypeId
	r.DailyTrxMax = b.DailyTrxMax
	r.DailyTrxAmountMax = b.DailyTrxAmountMax
	r.TrxAmountMin = b.TrxAmountMin
	r.TrxAmountMax = b.TrxAmountMax
}

func (r *TransactionLimitGetResponse) FromModel(b models.TransactionLimit) {
	r.Id = b.Id
	r.TrxTypeId = b.TrxTypeId
	r.DailyTrxMax = b.DailyTrxMax
	r.DailyTrxAmountMax = b.DailyTrxAmountMax
	r.TrxAmountMin = b.TrxAmountMin
	r.TrxAmountMax = b.TrxAmountMax
	r.Active = b.Active
}

func (r *TransactionLimitGetResponse) FromModels(ms []models.Model) []TransactionLimitGetResponse {
	var rs []TransactionLimitGetResponse
	for _, m := range ms {
		var r TransactionLimitGetResponse

		transactionLimit := m.(*models.TransactionLimit)
		r.FromModel(*transactionLimit)
		rs = append(rs, r)
	}

	return rs
}

func (r *TransactionLimitUpdateResponse) FromModel(b models.TransactionLimit) {
	r.Id = b.Id
	r.TrxTypeId = b.TrxTypeId
	r.DailyTrxMax = b.DailyTrxMax
	r.DailyTrxAmountMax = b.DailyTrxAmountMax
	r.TrxAmountMin = b.TrxAmountMin
	r.TrxAmountMax = b.TrxAmountMax
	r.Active = b.Active
	r.UpdatedBy = *b.UpdatedBy
}
