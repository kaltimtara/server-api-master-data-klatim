package controllers

import (
	"database/sql"
	"dg-datamaster-service/helpers"
	"dg-datamaster-service/models"
	"dg-datamaster-service/services/logging"
	"encoding/json"
	"errors"
	"math"
	"net/http"

	"github.com/julienschmidt/httprouter"
)

type TransactionTypeCreateRequest struct {
	CreatedBy       string `json:"created_by"`
	TransactionType string `json:"trx_type"`
}

type TransactionTypeCreateResponse struct {
	Id              int64  `json:"id"`
	TransactionType string `json:"trx_type"`
}

type TransactionTypeUpdateRequest struct {
	TransactionType string `json:"trx_type"`
	Active          bool   `json:"is_active"`
	UpdatedBy       string `json:"updated_by"`
}

type TransactionTypeUpdateResponse struct {
	Id              int64  `json:"id"`
	TransactionType string `json:"trx_type"`
	Active          bool   `json:"is_active"`
	UpdatedBy       string `json:"updated_by"`
}

type TransactionTypeGetResponse struct {
	Id              int64  `json:"id"`
	TransactionType string `json:"trx_type"`
	Active          bool   `json:"is_active"`
}

func TransactionTypeCreate(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	var req TransactionTypeCreateRequest
	err := json.NewDecoder(r.Body).Decode(&req)
	if err != nil {
		helpers.WriteResponse(r, w, helpers.BadRequestResponse())
		return
	}

	transactionType := req.ToModel()
	err = transactionType.Create()
	if err != nil {
		logging.Error("transaction_type", "TransactionTypeCreate: error create", logging.Data{
			"error": err.Error(),
			"body":  req,
		})

		helpers.WriteResponse(r, w, helpers.ServerError("Failed to save transaction type data"))
		return
	}

	var rsp TransactionTypeCreateResponse
	rsp.FromModel(transactionType)

	helpers.WriteResponse(r, w, helpers.SuccessResponse(rsp))
}

func TransactionTypeGet(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	id := ps.ByName("id")

	var transactionType models.TransactionType
	err := transactionType.Get(id)
	if errors.Is(err, sql.ErrNoRows) {
		helpers.WriteResponse(r, w, helpers.ClientErrorResponse("Transaction type data not found"))
		return
	} else if err != nil {
		logging.Error("transaction_type", "TransactionTypeGet: error get", logging.Data{
			"id":    id,
			"error": err.Error(),
		})

		helpers.WriteResponse(r, w, helpers.ServerError("Failed to retrieve transaction type data"))
		return
	}

	var rsp TransactionTypeGetResponse
	rsp.FromModel(transactionType)

	helpers.WriteResponse(r, w, helpers.SuccessResponse(rsp))
}

func TransactionTypeList(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	var pr models.PaginationRequest
	err := pr.FromRequest(r)
	if err != nil {
		helpers.WriteResponse(r, w, helpers.BadRequestResponse())
		return
	}

	var f models.TransactionTypeFilter
	f.FromRequest(r)

	var transactionType models.TransactionType
	transactionTypes, total, err := transactionType.List(pr, &f)
	if err != nil {
		logging.Error("transaction_type", "TransactionTypeList: error list", logging.Data{
			"error": err.Error(),
		})

		helpers.WriteResponse(r, w, helpers.ServerError("Failed to retrieve transaction type data"))
		return
	}

	var tmp TransactionTypeGetResponse
	rsp := tmp.FromModels(transactionTypes)

	totalPage := math.Ceil(float64(total) / float64(pr.PerPage))
	helpers.WriteResponse(r, w, helpers.PaginatedSuccessResponse(rsp, int64(pr.Page), int64(pr.PerPage), total,
		int64(totalPage)))
}

func TransactionTypeUpdate(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	id := ps.ByName("id")

	var req TransactionTypeUpdateRequest
	err := json.NewDecoder(r.Body).Decode(&req)
	if err != nil {
		helpers.WriteResponse(r, w, helpers.BadRequestResponse())
		return
	}

	var transactionType models.TransactionType
	err = transactionType.Get(id)
	if errors.Is(err, sql.ErrNoRows) {
		helpers.WriteResponse(r, w, helpers.ClientErrorResponse("Transaction type data not found"))
		return
	} else if err != nil {
		logging.Error("transaction_type", "TransactionTypeUpdate: error get", logging.Data{
			"id":    id,
			"error": err.Error(),
		})

		helpers.WriteResponse(r, w, helpers.ServerError("Failed to retrieve transaction type data"))
		return
	}

	req.FillModel(&transactionType)
	err = transactionType.Update()
	if err != nil {
		logging.Error("transaction_type", "TransactionTypeUpdate: error update", logging.Data{
			"error": err.Error(),
			"body":  req,
		})

		helpers.WriteResponse(r, w, helpers.ServerError("Failed to save transaction type data"))
		return
	}

	var rsp TransactionTypeUpdateResponse
	rsp.FromModel(transactionType)
	helpers.WriteResponse(r, w, helpers.SuccessResponse(rsp))

}

func TransactionTypeDelete(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	id := ps.ByName("id")

	var transactionType models.TransactionType
	err := transactionType.Get(id)
	if errors.Is(err, sql.ErrNoRows) {
		helpers.WriteResponse(r, w, helpers.ClientErrorResponse("Transaction type data not found"))
		return
	} else if err != nil {
		logging.Error("transaction_type", "TransactionTypeDelete: error get", logging.Data{
			"id":    id,
			"error": err.Error(),
		})

		helpers.WriteResponse(r, w, helpers.ServerError("Failed to retrieve transaction type data"))
		return
	}

	err = transactionType.Delete()
	if err != nil {
		logging.Error("transaction_type", "TransactionTypeDelete: error delete", logging.Data{
			"error": err.Error(),
			"id":    id,
		})

		helpers.WriteResponse(r, w, helpers.ServerError("Failed to delete transaction type data"))
		return
	}

	helpers.WriteResponse(r, w, helpers.SuccessResponse(nil))
}

func (r *TransactionTypeCreateRequest) ToModel() models.TransactionType {
	return models.TransactionType{
		CreatedBy:       r.CreatedBy,
		TransactionType: r.TransactionType,
	}
}

func (r *TransactionTypeUpdateRequest) FillModel(b *models.TransactionType) {
	b.TransactionType = r.TransactionType
	b.Active = r.Active
	b.UpdatedBy = &r.UpdatedBy
}

func (r *TransactionTypeCreateResponse) FromModel(b models.TransactionType) {
	r.Id = b.Id
	r.TransactionType = b.TransactionType
}

func (r *TransactionTypeGetResponse) FromModel(b models.TransactionType) {
	r.Id = b.Id
	r.TransactionType = b.TransactionType
	r.Active = b.Active
}

func (r *TransactionTypeGetResponse) FromModels(ms []models.Model) []TransactionTypeGetResponse {
	var rs []TransactionTypeGetResponse
	for _, m := range ms {
		var r TransactionTypeGetResponse

		transactionType := m.(*models.TransactionType)
		r.FromModel(*transactionType)
		rs = append(rs, r)
	}

	return rs
}

func (r *TransactionTypeUpdateResponse) FromModel(b models.TransactionType) {
	r.Id = b.Id
	r.TransactionType = b.TransactionType
	r.Active = b.Active
	r.UpdatedBy = *b.UpdatedBy
}
