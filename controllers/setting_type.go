package controllers

import (
	"database/sql"
	"dg-datamaster-service/helpers"
	"dg-datamaster-service/models"
	"dg-datamaster-service/services/logging"
	"encoding/json"
	"errors"
	"math"
	"net/http"

	"github.com/julienschmidt/httprouter"
)

type SettingTypeCreateRequest struct {
	CreatedBy   string `json:"created_by"`
	SettingType string `json:"setting_type"`
}

type SettingTypeCreateResponse struct {
	Id          int64  `json:"id"`
	SettingType string `json:"setting_type"`
}

type SettingTypeUpdateRequest struct {
	SettingType string `json:"setting_type"`
	Active      bool   `json:"is_active"`
	UpdatedBy   string `json:"updated_by"`
}

type SettingTypeUpdateResponse struct {
	Id          int64  `json:"id"`
	SettingType string `json:"setting_type"`
	Active      bool   `json:"is_active"`
	UpdatedBy   string `json:"updated_by"`
}

type SettingTypeGetResponse struct {
	Id          int64  `json:"id"`
	SettingType string `json:"setting_type"`
	Active      bool   `json:"is_active"`
}

func SettingTypeCreate(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	var req SettingTypeCreateRequest
	err := json.NewDecoder(r.Body).Decode(&req)
	if err != nil {
		helpers.WriteResponse(r, w, helpers.BadRequestResponse())
		return
	}

	settingType := req.ToModel()
	err = settingType.Create()
	if err != nil {
		logging.Error("setting_type", "SettingTypeCreate: error create", logging.Data{
			"error": err.Error(),
			"body":  req,
		})

		helpers.WriteResponse(r, w, helpers.ServerError("Failed to save setting type data"))
		return
	}

	var rsp SettingTypeCreateResponse
	rsp.FromModel(settingType)

	helpers.WriteResponse(r, w, helpers.SuccessResponse(rsp))
}

func SettingTypeGet(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	id := ps.ByName("id")

	var settingType models.SettingType
	err := settingType.Get(id)
	if errors.Is(err, sql.ErrNoRows) {
		helpers.WriteResponse(r, w, helpers.ClientErrorResponse("Setting type data not found"))
		return
	} else if err != nil {
		logging.Error("setting_type", "SettingTypeGet: error get", logging.Data{
			"id":    id,
			"error": err.Error(),
		})

		helpers.WriteResponse(r, w, helpers.ServerError("Failed to retrieve setting type data"))
		return
	}

	var rsp SettingTypeGetResponse
	rsp.FromModel(settingType)

	helpers.WriteResponse(r, w, helpers.SuccessResponse(rsp))
}

func SettingTypeList(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	var pr models.PaginationRequest
	err := pr.FromRequest(r)
	if err != nil {
		helpers.WriteResponse(r, w, helpers.BadRequestResponse())
		return
	}

	var f models.SettingTypeFilter
	f.FromRequest(r)

	var settingType models.SettingType
	settingTypes, total, err := settingType.List(pr, &f)
	if err != nil {
		logging.Error("setting_type", "SettingTypeList: error list", logging.Data{
			"error": err.Error(),
		})

		helpers.WriteResponse(r, w, helpers.ServerError("Failed to retrieve setting type data"))
		return
	}

	var tmp SettingTypeGetResponse
	rsp := tmp.FromModels(settingTypes)

	totalPage := math.Ceil(float64(total) / float64(pr.PerPage))
	helpers.WriteResponse(r, w, helpers.PaginatedSuccessResponse(rsp, int64(pr.Page), int64(pr.PerPage), total,
		int64(totalPage)))
}

func SettingTypeUpdate(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	id := ps.ByName("id")

	var req SettingTypeUpdateRequest
	err := json.NewDecoder(r.Body).Decode(&req)
	if err != nil {
		helpers.WriteResponse(r, w, helpers.BadRequestResponse())
		return
	}

	var settingType models.SettingType
	err = settingType.Get(id)
	if errors.Is(err, sql.ErrNoRows) {
		helpers.WriteResponse(r, w, helpers.ClientErrorResponse("Setting type data not found"))
		return
	} else if err != nil {
		logging.Error("setting_type", "SettingTypeUpdate: error get", logging.Data{
			"id":    id,
			"error": err.Error(),
		})

		helpers.WriteResponse(r, w, helpers.ServerError("Failed to retrieve setting type data"))
		return
	}

	req.FillModel(&settingType)
	err = settingType.Update()
	if err != nil {
		logging.Error("setting_type", "SettingTypeUpdate: error update", logging.Data{
			"error": err.Error(),
			"body":  req,
		})

		helpers.WriteResponse(r, w, helpers.ServerError("Failed to save setting type data"))
		return
	}

	var rsp SettingTypeUpdateResponse
	rsp.FromModel(settingType)
	helpers.WriteResponse(r, w, helpers.SuccessResponse(rsp))

}

func SettingTypeDelete(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	id := ps.ByName("id")

	var settingType models.SettingType
	err := settingType.Get(id)
	if errors.Is(err, sql.ErrNoRows) {
		helpers.WriteResponse(r, w, helpers.ClientErrorResponse("Setting type data not found"))
		return
	} else if err != nil {
		logging.Error("setting_type", "SettingTypeDelete: error get", logging.Data{
			"id":    id,
			"error": err.Error(),
		})

		helpers.WriteResponse(r, w, helpers.ServerError("Failed to retrieve setting type data"))
		return
	}

	err = settingType.Delete()
	if err != nil {
		logging.Error("setting_type", "SettingTypeDelete: error delete", logging.Data{
			"error": err.Error(),
			"id":    id,
		})

		helpers.WriteResponse(r, w, helpers.ServerError("Failed to delete setting type data"))
		return
	}

	helpers.WriteResponse(r, w, helpers.SuccessResponse(nil))
}

func (r *SettingTypeCreateRequest) ToModel() models.SettingType {
	return models.SettingType{
		CreatedBy:   r.CreatedBy,
		SettingType: r.SettingType,
	}
}

func (r *SettingTypeUpdateRequest) FillModel(b *models.SettingType) {
	b.SettingType = r.SettingType
	b.Active = r.Active
	b.UpdatedBy = &r.UpdatedBy
}

func (r *SettingTypeCreateResponse) FromModel(b models.SettingType) {
	r.Id = b.Id
	r.SettingType = b.SettingType
}

func (r *SettingTypeGetResponse) FromModel(b models.SettingType) {
	r.Id = b.Id
	r.SettingType = b.SettingType
	r.Active = b.Active
}

func (r *SettingTypeGetResponse) FromModels(ms []models.Model) []SettingTypeGetResponse {
	var rs []SettingTypeGetResponse
	for _, m := range ms {
		var r SettingTypeGetResponse

		settingType := m.(*models.SettingType)
		r.FromModel(*settingType)
		rs = append(rs, r)
	}

	return rs
}

func (r *SettingTypeUpdateResponse) FromModel(b models.SettingType) {
	r.Id = b.Id
	r.SettingType = b.SettingType
	r.Active = b.Active
	r.UpdatedBy = *b.UpdatedBy
}
