package controllers

import (
	"database/sql"
	"dg-datamaster-service/helpers"
	"dg-datamaster-service/models"
	"dg-datamaster-service/services/logging"
	"encoding/json"
	"errors"
	"github.com/julienschmidt/httprouter"
	"math"
	"net/http"
)

type ContentCreateRequest struct {
	CreatedBy           string `json:"created_by"`
	ContentTypeId       int    `json:"content_type_id"`
	LanguageId          int    `json:"language_id"`
	ShariaContent       string `json:"sharia_content"`
	ConventionalContent string `json:"conventional_content"`
}

type ContentCreateResponse struct {
	Id                  int64  `json:"id"`
	ContentTypeId       int    `json:"content_type_id"`
	LanguageId          int    `json:"language_id"`
	ShariaContent       string `json:"sharia_content"`
	ConventionalContent string `json:"conventional_content"`
}

type ContentUpdateRequest struct {
	ContentTypeId       int    `json:"content_type_id"`
	LanguageId          int    `json:"language_id"`
	ShariaContent       string `json:"sharia_content"`
	ConventionalContent string `json:"conventional_content"`
	IsActive            bool   `json:"is_active"`
	UpdatedBy           string `json:"updated_by"`
}

type ContentUpdateResponse struct {
	Id                  int64  `json:"id"`
	ContentTypeId       int    `json:"content_type_id"`
	LanguageId          int    `json:"language_id"`
	ShariaContent       string `json:"sharia_content"`
	ConventionalContent string `json:"conventional_content"`
	IsActive            bool   `json:"is_active"`
}

type ContentGetResponse struct {
	Id                  int64  `json:"id"`
	ContentTypeId       int    `json:"content_type_id"`
	LanguageId          int    `json:"language_id"`
	ShariaContent       string `json:"sharia_content"`
	ConventionalContent string `json:"conventional_content"`
	IsActive            bool   `json:"is_active"`
}

func ContentCreate(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	var req ContentCreateRequest
	err := json.NewDecoder(r.Body).Decode(&req)
	if err != nil {
		helpers.WriteResponse(r, w, helpers.BadRequestResponse())
		return
	}

	content := req.ToModel()
	err = content.Create()
	if err != nil {
		logging.Error("content", "ContentCreate: error create", logging.Data{
			"error": err.Error(),
			"body":  req,
		})

		helpers.WriteResponse(r, w, helpers.ServerError("Failed to save content data"))
		return
	}

	var rsp ContentCreateResponse
	rsp.FromModel(content)

	helpers.WriteResponse(r, w, helpers.SuccessResponse(rsp))
}

func ContentGet(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	id := ps.ByName("id")

	var content models.Content
	err := content.Get(id)
	if errors.Is(err, sql.ErrNoRows) {
		helpers.WriteResponse(r, w, helpers.ClientErrorResponse("Content data not found"))
		return
	} else if err != nil {
		logging.Error("content", "ContentGet: error get", logging.Data{
			"id":    id,
			"error": err.Error(),
		})

		helpers.WriteResponse(r, w, helpers.ServerError("Failed to retrieve content data"))
		return
	}

	var rsp ContentGetResponse
	rsp.FromModel(content)

	helpers.WriteResponse(r, w, helpers.SuccessResponse(rsp))
}

func ContentList(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	var pr models.PaginationRequest
	err := pr.FromRequest(r)
	if err != nil {
		helpers.WriteResponse(r, w, helpers.BadRequestResponse())
		return
	}

	var f models.ContentFilter
	f.FromRequest(r)

	var content models.Content
	contents, total, err := content.List(pr, &f)
	if err != nil {
		logging.Error("content", "ContentList: error list", logging.Data{
			"error": err.Error(),
		})

		helpers.WriteResponse(r, w, helpers.ServerError("Failed to retrieve content data"))
		return
	}

	var tmp ContentGetResponse
	rsp := tmp.FromModels(contents)

	totalPage := math.Ceil(float64(total) / float64(pr.PerPage))
	helpers.WriteResponse(r, w, helpers.PaginatedSuccessResponse(rsp, int64(pr.Page), int64(pr.PerPage), total,
		int64(totalPage)))
}

func ContentUpdate(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	id := ps.ByName("id")

	var req ContentUpdateRequest
	err := json.NewDecoder(r.Body).Decode(&req)
	if err != nil {
		helpers.WriteResponse(r, w, helpers.BadRequestResponse())
		return
	}

	var content models.Content
	err = content.Get(id)
	if errors.Is(err, sql.ErrNoRows) {
		helpers.WriteResponse(r, w, helpers.ClientErrorResponse("Content data not found"))
		return
	} else if err != nil {
		logging.Error("content", "ContentUpdate: error get", logging.Data{
			"id":    id,
			"error": err.Error(),
		})

		helpers.WriteResponse(r, w, helpers.ServerError("Failed to retrieve content data"))
		return
	}

	req.FillModel(&content)
	err = content.Update()
	if err != nil {
		logging.Error("content", "ContentUpdate: error update", logging.Data{
			"error": err.Error(),
			"body":  req,
		})

		helpers.WriteResponse(r, w, helpers.ServerError("Failed to save content data"))
		return
	}

	var rsp ContentUpdateResponse
	rsp.FromModel(content)
	helpers.WriteResponse(r, w, helpers.SuccessResponse(rsp))

}

func ContentDelete(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	id := ps.ByName("id")

	var content models.Content
	err := content.Get(id)
	if errors.Is(err, sql.ErrNoRows) {
		helpers.WriteResponse(r, w, helpers.ClientErrorResponse("Content data not found"))
		return
	} else if err != nil {
		logging.Error("content", "ContentDelete: error get", logging.Data{
			"id":    id,
			"error": err.Error(),
		})

		helpers.WriteResponse(r, w, helpers.ServerError("Failed to retrieve content data"))
		return
	}

	err = content.Delete()
	if err != nil {
		logging.Error("content", "ContentDelete: error delete", logging.Data{
			"error": err.Error(),
			"id":    id,
		})

		helpers.WriteResponse(r, w, helpers.ServerError("Failed to delete content data"))
		return
	}

	helpers.WriteResponse(r, w, helpers.SuccessResponse(nil))
}

func (r *ContentCreateRequest) ToModel() models.Content {
	return models.Content{
		CreatedBy:           r.CreatedBy,
		ContentTypeId:       r.ContentTypeId,
		LanguageId:          r.LanguageId,
		ShariaContent:       &r.ShariaContent,
		ConventionalContent: r.ConventionalContent,
	}
}

func (r *ContentUpdateRequest) FillModel(c *models.Content) {
	c.ContentTypeId = r.ContentTypeId
	c.LanguageId = r.LanguageId
	c.ShariaContent = &r.ShariaContent
	c.ConventionalContent = r.ConventionalContent
	c.IsActive = r.IsActive
	c.UpdatedBy = &r.UpdatedBy
}

func (r *ContentCreateResponse) FromModel(c models.Content) {
	r.Id = c.Id
	r.ContentTypeId = c.ContentTypeId
	r.LanguageId = c.LanguageId
	r.ShariaContent = *c.ShariaContent
	r.ConventionalContent = c.ConventionalContent
}

func (r *ContentGetResponse) FromModel(c models.Content) {
	r.Id = c.Id
	r.ContentTypeId = c.ContentTypeId
	r.LanguageId = c.LanguageId
	r.ShariaContent = *c.ShariaContent
	r.ConventionalContent = c.ConventionalContent
	r.IsActive = c.IsActive
}

func (r *ContentGetResponse) FromModels(cs []models.Model) []ContentGetResponse {
	var rs []ContentGetResponse
	for _, m := range cs {
		var r ContentGetResponse

		content := m.(*models.Content)
		r.FromModel(*content)
		rs = append(rs, r)
	}

	return rs
}

func (r *ContentUpdateResponse) FromModel(c models.Content) {
	r.Id = c.Id
	r.ContentTypeId = c.ContentTypeId
	r.LanguageId = c.LanguageId
	r.ShariaContent = *c.ShariaContent
	r.ConventionalContent = c.ConventionalContent
	r.IsActive = c.IsActive
}
