# # Build the binary
# FROM golang:1.22-alpine as builder

# WORKDIR /var/www

# # Copy the go mod and sum files
# COPY go.mod go.sum ./

# # Download all dependencies
# RUN go mod download

# # Copy the source code to /go/src/app
# COPY . .

# # Build the binary
# RUN CGO_ENABLED=0

# # Build the Go app
# RUN go build -o main .

# # Run the binary when the container starts
# CMD ["./main"]


# syntax=docker/dockerfile:1.2

# Build the binary
FROM golang:1.22-alpine as builder

WORKDIR /go/src/app

# Copy the go mod and sum files
COPY go.mod go.sum ./

# Copy .env file
COPY .env.example .env

# Download all dependencies
RUN go mod download

# Copy the source code
COPY . .

# Build the binary
RUN CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -o /go/bin/app -ldflags "-s -w" .

# Create the final image
FROM alpine:3.16

# Copy the binary from the builder stage
COPY --from=builder /go/bin/app /usr/local/bin/app

# Expose the port
EXPOSE 8084

# Run the binary when the container starts
CMD ["app"]

