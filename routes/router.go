package routes

import (
	"dg-datamaster-service/controllers"
	"dg-datamaster-service/middlewares"

	"github.com/julienschmidt/httprouter"
)

func Router() *httprouter.Router {
	router := httprouter.New()

	// Auth
	router.POST("/auth/login", middlewares.Logging(controllers.Login))

	// Bank
	router.POST("/banks", middlewares.Auth(controllers.BankCreate))
	router.GET("/banks/:id", middlewares.Auth(controllers.BankGet))
	router.GET("/banks", middlewares.Auth(controllers.BankList))
	router.PUT("/banks/:id", middlewares.Auth(controllers.BankUpdate))
	router.DELETE("/banks/:id", middlewares.Auth(controllers.BankDelete))

	// Bank Account Type
	router.POST("/bank-account-types", middlewares.Auth(controllers.BankAccountTypeCreate))
	router.GET("/bank-account-types/:id", middlewares.Auth(controllers.BankAccountTypeGet))
	router.GET("/bank-account-types", middlewares.Auth(controllers.BankAccountTypeList))
	router.PUT("/bank-account-types/:id", middlewares.Auth(controllers.BankAccountTypeUpdate))
	router.DELETE("/bank-account-types/:id", middlewares.Auth(controllers.BankAccountTypeDelete))

	// Holiday
	router.POST("/holidays", middlewares.Auth(controllers.HolidayCreate))
	router.GET("/holidays/:id", middlewares.Auth(controllers.HolidayGet))
	router.GET("/holidays", middlewares.Auth(controllers.HolidayList))
	router.PUT("/holidays/:id", middlewares.Auth(controllers.HolidayUpdate))
	router.DELETE("/holidays/:id", middlewares.Auth(controllers.HolidayDelete))

	// Province
	router.POST("/provinces", middlewares.Auth(controllers.ProvinceCreate))
	router.GET("/provinces/:id", middlewares.Auth(controllers.ProvinceGet))
	router.GET("/provinces", middlewares.Auth(controllers.ProvinceList))
	router.PUT("/provinces/:id", middlewares.Auth(controllers.ProvinceUpdate))
	router.DELETE("/provinces/:id", middlewares.Auth(controllers.ProvinceDelete))

	// City
	router.POST("/cities", middlewares.Auth(controllers.CityCreate))
	router.GET("/cities/:id", middlewares.Auth(controllers.CityGet))
	router.GET("/cities", middlewares.Auth(controllers.CityList))
	router.PUT("/cities/:id", middlewares.Auth(controllers.CityUpdate))
	router.DELETE("/cities/:id", middlewares.Auth(controllers.CityDelete))

	// District
	router.POST("/districts", middlewares.Auth(controllers.DistrictCreate))
	router.GET("/districts/:id", middlewares.Auth(controllers.DistrictGet))
	router.GET("/districts", middlewares.Auth(controllers.DistrictList))
	router.PUT("/districts/:id", middlewares.Auth(controllers.DistrictUpdate))
	router.DELETE("/districts/:id", middlewares.Auth(controllers.DistrictDelete))

	// Village
	router.POST("/villages", middlewares.Auth(controllers.VillageCreate))
	router.GET("/villages/:id", middlewares.Auth(controllers.VillageGet))
	router.GET("/villages", middlewares.Auth(controllers.VillageList))
	router.PUT("/villages/:id", middlewares.Auth(controllers.VillageUpdate))
	router.DELETE("/villages/:id", middlewares.Auth(controllers.VillageDelete))

	// Language
	router.POST("/languages", middlewares.Auth(controllers.LanguageCreate))
	router.GET("/languages/:id", middlewares.Auth(controllers.LanguageGet))
	router.GET("/languages", middlewares.Auth(controllers.LanguageList))
	router.PUT("/languages/:id", middlewares.Auth(controllers.LanguageUpdate))
	router.DELETE("/languages/:id", middlewares.Auth(controllers.LanguageDelete))

	// Content Type
	router.POST("/content-types", middlewares.Auth(controllers.ContentTypeCreate))
	router.GET("/content-types/:id", middlewares.Auth(controllers.ContentTypeGet))
	router.GET("/content-types", middlewares.Auth(controllers.ContentTypeList))
	router.PUT("/content-types/:id", middlewares.Auth(controllers.ContentTypeUpdate))
	router.DELETE("/content-types/:id", middlewares.Auth(controllers.ContentTypeDelete))

	// Content
	router.POST("/contents", middlewares.Auth(controllers.ContentCreate))
	router.GET("/contents/:id", middlewares.Auth(controllers.ContentGet))
	router.GET("/contents", middlewares.Auth(controllers.ContentList))
	router.PUT("/contents/:id", middlewares.Auth(controllers.ContentUpdate))
	router.DELETE("/contents/:id", middlewares.Auth(controllers.ContentDelete))

	// Setting Type
	router.POST("/setting-types", middlewares.Auth(controllers.SettingTypeCreate))
	router.GET("/setting-types/:id", middlewares.Auth(controllers.SettingTypeGet))
	router.GET("/setting-types", middlewares.Auth(controllers.SettingTypeList))
	router.PUT("/setting-types/:id", middlewares.Auth(controllers.SettingTypeUpdate))
	router.DELETE("/setting-types/:id", middlewares.Auth(controllers.SettingTypeDelete))

	// Setting
	router.POST("/settings", middlewares.Auth(controllers.SettingCreate))
	router.GET("/settings/:id", middlewares.Auth(controllers.SettingGet))
	router.GET("/settings", middlewares.Auth(controllers.SettingList))
	router.PUT("/settings/:id", middlewares.Auth(controllers.SettingUpdate))
	router.DELETE("/settings/:id", middlewares.Auth(controllers.SettingDelete))

	// Transaction Type
	router.POST("/transaction-types", middlewares.Auth(controllers.TransactionTypeCreate))
	router.GET("/transaction-types/:id", middlewares.Auth(controllers.TransactionTypeGet))
	router.GET("/transaction-types", middlewares.Auth(controllers.TransactionTypeList))
	router.PUT("/transaction-types/:id", middlewares.Auth(controllers.TransactionTypeUpdate))
	router.DELETE("/transaction-types/:id", middlewares.Auth(controllers.TransactionTypeDelete))

	// Transaction Interval
	router.POST("/transaction-intervals", middlewares.Auth(controllers.TransactionIntervalCreate))
	router.GET("/transaction-intervals/:id", middlewares.Auth(controllers.TransactionIntervalGet))
	router.GET("/transaction-intervals", middlewares.Auth(controllers.TransactionIntervalList))
	router.PUT("/transaction-intervals/:id", middlewares.Auth(controllers.TransactionIntervalUpdate))
	router.DELETE("/transaction-intervals/:id", middlewares.Auth(controllers.TransactionIntervalDelete))

	// Transaction Limit
	router.POST("/transaction-limits", middlewares.Auth(controllers.TransactionLimitCreate))
	router.GET("/transaction-limits/:id", middlewares.Auth(controllers.TransactionLimitGet))
	router.GET("/transaction-limits", middlewares.Auth(controllers.TransactionLimitList))
	router.PUT("/transaction-limits/:id", middlewares.Auth(controllers.TransactionLimitUpdate))
	router.DELETE("/transaction-limits/:id", middlewares.Auth(controllers.TransactionLimitDelete))

	// Transaction Cutoff
	router.POST("/transaction-cutoffs", middlewares.Auth(controllers.TransactionCutoffCreate))
	router.GET("/transaction-cutoffs/:id", middlewares.Auth(controllers.TransactionCutoffGet))
	router.GET("/transaction-cutoffs", middlewares.Auth(controllers.TransactionCutoffList))
	router.PUT("/transaction-cutoffs/:id", middlewares.Auth(controllers.TransactionCutoffUpdate))
	router.DELETE("/transaction-cutoffs/:id", middlewares.Auth(controllers.TransactionCutoffDelete))

	// Transaction Fee
	router.POST("/transaction-fees", middlewares.Auth(controllers.TransactionFeeCreate))
	router.GET("/transaction-fees/:id", middlewares.Auth(controllers.TransactionFeeGet))
	router.GET("/transaction-fees", middlewares.Auth(controllers.TransactionFeeList))
	router.PUT("/transaction-fees/:id", middlewares.Auth(controllers.TransactionFeeUpdate))
	router.DELETE("/transaction-fees/:id", middlewares.Auth(controllers.TransactionFeeDelete))

	// Transaction Option
	router.POST("/transaction-options", middlewares.Auth(controllers.TransactionOptionCreate))
	router.GET("/transaction-options/:id", middlewares.Auth(controllers.TransactionOptionGet))
	router.GET("/transaction-options", middlewares.Auth(controllers.TransactionOptionList))
	router.PUT("/transaction-options/:id", middlewares.Auth(controllers.TransactionOptionUpdate))
	router.DELETE("/transaction-options/:id", middlewares.Auth(controllers.TransactionOptionDelete))

	// Error Code
	router.POST("/error-codes", middlewares.Auth(controllers.ErrorCodeCreate))
	router.GET("/error-codes/:id", middlewares.Auth(controllers.ErrorCodeGet))
	router.GET("/error-codes", middlewares.Auth(controllers.ErrorCodeList))
	router.PUT("/error-codes/:id", middlewares.Auth(controllers.ErrorCodeUpdate))
	router.DELETE("/error-codes/:id", middlewares.Auth(controllers.ErrorCodeDelete))

	// Branch Office
	router.POST("/branch-offices", middlewares.Auth(controllers.BranchOfficeCreate))
	router.GET("/branch-offices/:id", middlewares.Auth(controllers.BranchOfficeGet))
	router.GET("/branch-offices", middlewares.Auth(controllers.BranchOfficeList))
	router.PUT("/branch-offices/:id", middlewares.Auth(controllers.BranchOfficeUpdate))
	router.DELETE("/branch-offices/:id", middlewares.Auth(controllers.BranchOfficeDelete))

	// Deposit Type
	router.POST("/deposit-types", middlewares.Auth(controllers.DepositTypeCreate))
	router.GET("/deposit-types/:id", middlewares.Auth(controllers.DepositTypeGet))
	router.GET("/deposit-types", middlewares.Auth(controllers.DepositTypeList))
	router.PUT("/deposit-types/:id", middlewares.Auth(controllers.DepositTypeUpdate))
	router.DELETE("/deposit-types/:id", middlewares.Auth(controllers.DepositTypeDelete))

	// Biller Type
	router.POST("/biller-types", middlewares.Auth(controllers.BillerTypeCreate))
	router.GET("/biller-types/:id", middlewares.Auth(controllers.BillerTypeGet))
	router.GET("/biller-types", middlewares.Auth(controllers.BillerTypeList))
	router.PUT("/biller-types/:id", middlewares.Auth(controllers.BillerTypeUpdate))
	router.DELETE("/biller-types/:id", middlewares.Auth(controllers.BillerTypeDelete))

	// Billing Code Prefix
	router.POST("/billing-code-prefixes", middlewares.Auth(controllers.BillingCodePrefixCreate))
	router.GET("/billing-code-prefixes/:id", middlewares.Auth(controllers.BillingCodePrefixGet))
	router.GET("/billing-code-prefixes", middlewares.Auth(controllers.BillingCodePrefixList))
	router.PUT("/billing-code-prefixes/:id", middlewares.Auth(controllers.BillingCodePrefixUpdate))
	router.DELETE("/billing-code-prefixes/:id", middlewares.Auth(controllers.BillingCodePrefixDelete))

	// Biller Denom
	router.POST("/biller-denoms", middlewares.Auth(controllers.BillerDenomCreate))
	router.GET("/biller-denoms/:id", middlewares.Auth(controllers.BillerDenomGet))
	router.GET("/biller-denoms", middlewares.Auth(controllers.BillerDenomList))
	router.PUT("/biller-denoms/:id", middlewares.Auth(controllers.BillerDenomUpdate))
	router.DELETE("/biller-denoms/:id", middlewares.Auth(controllers.BillerDenomDelete))

	// Deposit Number
	router.POST("/deposit-numbers", middlewares.Auth(controllers.DepositNumberCreate))
	router.GET("/deposit-numbers/:id", middlewares.Auth(controllers.DepositNumberGet))
	router.GET("/deposit-numbers", middlewares.Auth(controllers.DepositNumberList))
	router.PUT("/deposit-numbers/:id", middlewares.Auth(controllers.DepositNumberUpdate))
	router.DELETE("/deposit-numbers/:id", middlewares.Auth(controllers.DepositNumberDelete))

	return router
}
