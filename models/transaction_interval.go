package models

import (
	"net/http"
	"strconv"
	"strings"
	"time"
)

type TransactionInterval struct {
	Id                  int64   `json:"id" db:"id"`
	CreatedAt           string  `json:"created_at" db:"created_at"`
	CreatedBy           string  `json:"created_by" db:"created_by"`
	UpdatedAt           *string `json:"updated_at" db:"updated_at"`
	UpdatedBy           *string `json:"updated_by" db:"updated_by"`
	TrxTypeId           int64   `json:"trx_type_id" db:"trx_type_id"`
	TransactionInterval string  `json:"transaction_interval" db:"transaction_interval"`
	Active              bool    `json:"is_active" db:"is_active"`
}

type TransactionIntervalFilter struct {
	Search              string `json:"search" db:"search"`
	TrxTypeId           string `json:"trx_type_id" db:"trx_type_id"`
	TransactionInterval string `json:"transaction_interval" db:"transaction_interval"`
	Active              string `json:"is_active" db:"is_active"`
}

func (b *TransactionInterval) TableName() string {
	return "m_transaction_intervals"
}

func (b *TransactionInterval) GetKeyColumn() string {
	return "id"
}

func (b *TransactionInterval) GetKey() any {
	return b.Id
}

func (b *TransactionInterval) Get(id any) error {
	b.Id, _ = strconv.ParseInt(id.(string), 10, 64)

	return get(b, []string{"id", "trx_type_id", "transaction_interval", "is_active"})
}

func (b *TransactionInterval) List(pr PaginationRequest, f Filter) ([]Model, int64, error) {
	var transactionTypes []TransactionInterval

	total, err := list(&transactionTypes, b, []string{"id", "trx_type_id", "transaction_interval",
		"is_active"},
		pr, f)
	if err != nil {
		return nil, 0, err
	}

	var models []Model
	for i := range transactionTypes {
		models = append(models, &transactionTypes[i])
	}

	return models, total, nil
}

func (b *TransactionInterval) Create() error {
	b.CreatedAt = time.Now().Format(time.DateTime)

	id, err := create(b, []string{"trx_type_id", "transaction_interval", "created_at", "created_by"},
		nil)
	if err != nil {
		return err
	}

	b.Id = id.(int64)
	return nil
}

func (b *TransactionInterval) Update() error {
	t := time.Now().Format(time.DateTime)
	b.UpdatedAt = &t

	return update(b, []string{"trx_type_id", "transaction_interval", "updated_at", "updated_by"}, nil)
}

func (b *TransactionInterval) Delete() error {
	return destroy(b, nil)
}

func (f *TransactionIntervalFilter) Parse() (string, map[string]any, error) {
	var filters []string
	var err error
	args := make(map[string]any)

	if f.Search != "" {
		filters = append(filters, "LOWER(transaction_interval) LIKE LOWER(:search)")
		args["search"] = "%" + f.Search + "%"
	}

	if f.TrxTypeId != "" {
		filters = append(filters, "trx_type_id=:trx_type_id")
		args["trx_type_id"] = f.TrxTypeId
	}

	if f.TransactionInterval != "" {
		filters = append(filters, "transaction_interval=:transaction_interval")
		args["transaction_interval"] = f.TransactionInterval
	}

	if f.Active != "" {
		filters = append(filters, "is_active=:is_active")
		args["is_active"], err = strconv.ParseBool(f.Active)

		if err != nil {
			return "", nil, err
		}
	}

	return strings.Join(filters, " AND "), args, nil
}

func (f *TransactionIntervalFilter) FromRequest(r *http.Request) {
	f.Search = r.FormValue("search")
	f.TrxTypeId = r.FormValue("trx_type_id")
	f.TransactionInterval = r.FormValue("transaction_interval")
	f.Active = r.FormValue("is_active")
}
