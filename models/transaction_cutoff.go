package models

import (
	"net/http"
	"strconv"
	"strings"
	"time"
)

type TransactionCutoff struct {
	Id              int64   `json:"id" db:"id"`
	CreatedAt       string  `json:"created_at" db:"created_at"`
	CreatedBy       string  `json:"created_by" db:"created_by"`
	UpdatedAt       *string `json:"updated_at" db:"updated_at"`
	UpdatedBy       *string `json:"updated_by" db:"updated_by"`
	TrxTypeId       int64   `json:"trx_type_id" db:"trx_type_id"`
	StartTime       string  `json:"start_time" db:"start_time"`
	EndTime         string  `json:"end_time" db:"end_time"`
	BusinessDayOnly bool    `json:"is_business_day_only" db:"is_business_day_only"`
	Active          bool    `json:"is_active" db:"is_active"`
}

type TransactionCutoffFilter struct {
	TrxTypeId       string `json:"trx_type_id" db:"trx_type_id"`
	StartTime       string `json:"start_time" db:"start_time"`
	EndTime         string `json:"end_time" db:"end_time"`
	BusinessDayOnly string `json:"is_business_day_only" db:"is_business_day_only"`
	Active          string `json:"is_active" db:"is_active"`
}

func (b *TransactionCutoff) TableName() string {
	return "m_transaction_cutoffs"
}

func (b *TransactionCutoff) GetKeyColumn() string {
	return "id"
}

func (b *TransactionCutoff) GetKey() any {
	return b.Id
}

func (b *TransactionCutoff) Get(id any) error {
	b.Id, _ = strconv.ParseInt(id.(string), 10, 64)

	err := get(b, []string{"id", "trx_type_id", "start_time", "end_time", "is_business_day_only", "is_active"})
	if err != nil {
		return err
	}

	return b.parseCutOffTime()
}

func (b *TransactionCutoff) List(pr PaginationRequest, f Filter) ([]Model, int64, error) {
	var transactionTypes []TransactionCutoff

	total, err := list(&transactionTypes, b, []string{"id", "trx_type_id", "start_time", "end_time", "is_business_day_only", "is_active"}, pr, f)
	if err != nil {
		return nil, 0, err
	}

	var models []Model
	for i := range transactionTypes {
		err = transactionTypes[i].parseCutOffTime()
		if err != nil {
			return nil, 0, err
		}

		models = append(models, &transactionTypes[i])
	}

	return models, total, nil
}

func (b *TransactionCutoff) Create() error {
	b.CreatedAt = time.Now().Format(time.DateTime)

	id, err := create(b, []string{"trx_type_id", "start_time", "end_time", "is_business_day_only", "created_at", "created_by"}, nil)
	if err != nil {
		return err
	}

	b.Id = id.(int64)
	return nil
}

func (b *TransactionCutoff) Update() error {
	t := time.Now().Format(time.DateTime)
	b.UpdatedAt = &t

	return update(b, []string{"trx_type_id", "start_time", "end_time", "is_business_day_only", "updated_at", "updated_by"}, nil)
}

func (b *TransactionCutoff) Delete() error {
	return destroy(b, nil)
}

func (b *TransactionCutoff) parseCutOffTime() error {
	t, err := time.Parse(time.RFC3339, b.StartTime)
	if err != nil {
		return err
	}

	b.StartTime = t.Format(time.TimeOnly)

	t, err = time.Parse(time.RFC3339, b.EndTime)
	if err != nil {
		return err
	}

	b.EndTime = t.Format(time.TimeOnly)

	return nil
}

func (f *TransactionCutoffFilter) Parse() (string, map[string]any, error) {
	var filters []string
	var err error
	args := make(map[string]any)

	if f.TrxTypeId != "" {
		filters = append(filters, "trx_type_id=:trx_type_id")
		args["trx_type_id"], _ = strconv.ParseInt(f.TrxTypeId, 10, 64)
	}

	if f.StartTime != "" {
		filters = append(filters, "start_time=:start_time")
		args["start_time"], _ = time.Parse(time.DateTime, f.StartTime)
	}

	if f.EndTime != "" {
		filters = append(filters, "end_time=:end_time")
		args["end_time"], _ = time.Parse(time.DateTime, f.EndTime)
	}

	if f.BusinessDayOnly != "" {
		filters = append(filters, "is_business_day_only=:is_business_day_only")
		args["is_business_day_only"], _ = strconv.ParseBool(f.BusinessDayOnly)
	}

	if f.Active != "" {
		filters = append(filters, "is_active=:is_active")
		args["is_active"], err = strconv.ParseBool(f.Active)

		if err != nil {
			return "", nil, err
		}
	}

	return strings.Join(filters, " AND "), args, nil
}

func (f *TransactionCutoffFilter) FromRequest(r *http.Request) {
	f.TrxTypeId = r.FormValue("trx_type_id")
	f.StartTime = r.FormValue("start_time")
	f.EndTime = r.FormValue("end_time")
	f.BusinessDayOnly = r.FormValue("is_business_day_only")
	f.Active = r.FormValue("is_active")
}
