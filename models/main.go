package models

import (
	"dg-datamaster-service/config"
	"github.com/jmoiron/sqlx"
	"net/http"
	"strconv"
	"strings"
)

// Hindari perubahan pada fungsi-fungsi di bawah.
// Jika anda perlu fungsi khusus/custom untuk model tertentu,
// cukup buat implementasi interface Model yang berbeda untuknya.
// Atau buat fungsi baru untuk model tersebut.

type PaginationRequest struct {
	Page     int    `json:"page"`
	PerPage  int    `json:"per_page"`
	OrderBy  string `json:"order_by"`
	OrderDir string `json:"order_direction"`
}

// Model adalah sebuah interface yang dapat digunakan untuk mempermudah pembuatan model.
// Anda harus mengimplementasikan method-method yang ada di dalam interface ini.
type Model interface {
	List(pr PaginationRequest, f Filter) ([]Model, int64, error)
	Get(id any) error
	Create() error
	Update() error
	Delete() error

	TableName() string
	GetKeyColumn() string
	GetKey() any
}

type Filter interface {
	Parse() (string, map[string]any, error)
}

// get digunakan untuk mengambil data dari database.
// model adalah model yang akan diisi dengan data yang diambil dari database;
// columns adalah kolom-kolom yang akan diambil dari database;
// val adalah nilai dari kunci pencarian.
func get(model Model, columns []string) error {
	db := config.DB()
	query := "SELECT " + strings.Join(columns, ",") + " FROM " + model.TableName() + " WHERE " + model.GetKeyColumn() + " = $1"

	stmt, err := db.Preparex(query)
	if err != nil {
		return err
	}

	err = stmt.Get(model, model.GetKey())
	if err != nil {
		return err
	}

	return nil
}

// create digunakan untuk membuat data baru di database.
// model adalah model yang telah diisi dengan data yang akan dimasukkan ke database;
// columns adalah kolom-kolom yang akan diisi;
// tx adalah transaksi yang sedang berjalan. dapat diisi nil jika tidak ingin menggunakan transaksi;
func create(model Model, columns []string, tx *sqlx.Tx) (any, error) {
	placeholders := make([]string, len(columns))
	for i, column := range columns {
		placeholders[i] = ":" + column
	}

	query := "INSERT INTO " + model.TableName() + " (" + strings.Join(columns, ", "+
		"") + ") VALUES (" + strings.Join(placeholders, ", ") + ") RETURNING id"

	var stmt *sqlx.NamedStmt
	var err error
	if tx != nil {
		stmt, err = tx.PrepareNamed(query)
	} else {
		stmt, err = config.DB().PrepareNamed(query)
	}

	if err != nil {
		return nil, err
	}

	var id any
	err = stmt.QueryRowx(model).Scan(&id)

	return id, err
}

// update digunakan untuk mengubah data di database.
// model adalah model yang telah diisi dengan data yang telah diubah untuk disimpan;
// columns adalah kolom-kolom yang akan diubah;
// key adalah nama kolom yang akan dijadikan sebagai kunci pencarian;
// tx adalah transaksi yang sedang berjalan. dapat diisi nil jika tidak ingin menggunakan transaksi;
func update(model Model, columns []string, tx *sqlx.Tx) error {
	placeholders := make([]string, len(columns))
	for i, column := range columns {
		placeholders[i] = column + " = :" + column
	}

	query := "UPDATE " + model.TableName() + " SET " + strings.Join(placeholders, ", "+
		"") + " WHERE " + model.GetKeyColumn() + " = :" + model.GetKeyColumn()

	var stmt *sqlx.NamedStmt
	var err error
	if tx != nil {
		stmt, err = tx.PrepareNamed(query)
	} else {
		stmt, err = config.DB().PrepareNamed(query)
	}
	if err != nil {
		return err
	}

	_, err = stmt.Exec(model)
	if err != nil {
		return err
	}

	return nil
}

// destroy digunakan untuk menghapus data di database.
// model adalah model yang akan dihapus;
// key adalah nama kolom yang akan dijadikan sebagai kunci pencarian.
// tx adalah transaksi yang sedang berjalan. dapat diisi nil jika tidak ingin menggunakan transaksi;
func destroy(model Model, tx *sqlx.Tx) error {
	query := "DELETE FROM " + model.TableName() + " WHERE " + model.GetKeyColumn() + " = :" + model.GetKeyColumn()

	var stmt *sqlx.NamedStmt
	var err error
	if tx != nil {
		stmt, err = tx.PrepareNamed(query)
	} else {
		stmt, err = config.DB().PrepareNamed(query)
	}

	if err != nil {
		return err
	}

	_, err = stmt.Exec(model)
	return err
}

func list(dest interface{}, model Model, columns []string, pr PaginationRequest, f Filter) (int64, error) {
	query := "SELECT " + strings.Join(columns, ",") + " FROM " + model.TableName()
	where, args, err := f.Parse()
	if err != nil {
		return 0, err
	}

	if where != "" {
		where = " WHERE " + where
		query += where
	}

	var total int64
	stmt, err := config.DB().PrepareNamed("SELECT COUNT(*) FROM " + model.TableName() + where)
	if err != nil {
		return 0, err
	}

	err = stmt.Get(&total, args)
	if err != nil {
		return 0, err
	}

	query += " ORDER BY " + pr.OrderBy + " " + pr.OrderDir + " LIMIT " + strconv.Itoa(pr.PerPage) + " OFFSET " + strconv.Itoa((pr.Page-1)*pr.PerPage)
	stmt, err = config.DB().PrepareNamed(query)
	if err != nil {
		return 0, err
	}

	err = stmt.Select(dest, args)
	if err != nil {
		return 0, err
	}

	return total, nil
}

func (pr *PaginationRequest) FromRequest(r *http.Request) error {
	var err error

	page := r.URL.Query().Get("page")

	if page != "" {
		pr.Page, err = strconv.Atoi(page)
		if err != nil {
			return err
		}
	} else {
		pr.Page = 1
	}

	perPage := r.URL.Query().Get("per_page")
	if perPage != "" {
		pr.PerPage, err = strconv.Atoi(perPage)
		if err != nil {
			return err
		}
	} else {
		pr.PerPage = 10
	}

	pr.OrderBy = r.URL.Query().Get("order_by")
	pr.OrderDir = strings.ToLower(r.URL.Query().Get("order_direction"))

	if pr.OrderDir != "" && pr.OrderDir != "asc" && pr.OrderDir != "desc" {
		pr.OrderDir = "asc"
	}

	return nil
}
