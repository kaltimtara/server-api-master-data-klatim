package models

import (
	"net/http"
	"strconv"
	"strings"
	"time"
)

type Content struct {
	Id                  int64   `json:"id" db:"id"`
	CreatedAt           string  `json:"created_at" db:"created_at"`
	CreatedBy           string  `json:"created_by" db:"created_by"`
	UpdatedAt           *string `json:"updated_at" db:"updated_at"`
	UpdatedBy           *string `json:"updated_by" db:"updated_by"`
	ContentTypeId       int     `json:"content_type_id" db:"content_type_id"`
	LanguageId          int     `json:"language_id" db:"language_id"`
	ShariaContent       *string `json:"sharia_content" db:"sharia_content"`
	ConventionalContent string  `json:"conventional_content" db:"conventional_content"`
	IsActive            bool    `json:"is_active" db:"is_active"`
}

type ContentFilter struct {
	Search        string `json:"search" db:"search"`
	ContentTypeId string `json:"content_type_id" db:"content_type_id"`
	LanguageId    string `json:"language_id" db:"language_id"`
	IsActive      string `json:"is_active" db:"is_active"`
}

func (c *Content) TableName() string {
	return "m_contents"
}

func (c *Content) GetKeyColumn() string {
	return "id"
}

func (c *Content) GetKey() any {
	return c.Id
}

func (c *Content) Get(id any) error {
	c.Id, _ = strconv.ParseInt(id.(string), 10, 64)

	return get(c, []string{"id", "content_type_id", "language_id", "sharia_content", "conventional_content", "is_active"})
}

func (c *Content) List(pr PaginationRequest, f Filter) ([]Model, int64, error) {
	var contents []Content

	total, err := list(&contents, c, []string{"id", "content_type_id", "language_id", "sharia_content", "conventional_content",
		"is_active"}, pr, f)
	if err != nil {
		return nil, 0, err
	}

	var models []Model
	for i, _ := range contents {
		models = append(models, &contents[i])
	}

	return models, total, nil
}

func (c *Content) Create() error {
	c.CreatedAt = time.Now().Format(time.DateTime)

	id, err := create(c, []string{"content_type_id", "language_id", "sharia_content", "conventional_content", "created_at", "created_by"}, nil)
	if err != nil {
		return err
	}

	c.Id = id.(int64)
	return nil
}

func (c *Content) Update() error {
	t := time.Now().Format(time.DateTime)
	c.UpdatedAt = &t

	return update(c, []string{"content_type_id", "language_id", "sharia_content", "conventional_content", "updated_at", "updated_by",
		"is_active"}, nil)
}

func (c *Content) Delete() error {
	return destroy(c, nil)
}

func (f *ContentFilter) Parse() (string, map[string]any, error) {
	var filters []string
	var err error
	args := make(map[string]any)

	if f.Search != "" {
		filters = append(filters, "(LOWER(conventional_content) LIKE LOWER(:conventional_search) OR LOWER("+
			"sharia_content) LIKE LOWER(:sharia_search))")
		args["conventional_search"] = "%" + f.Search + "%"
		args["sharia_search"] = "%" + f.Search + "%"
	}

	if f.ContentTypeId != "" {
		filters = append(filters, "content_type_id=:content_type_id")
		args["content_type_id"], err = strconv.Atoi(f.ContentTypeId)

		if err != nil {
			return "", nil, err
		}
	}

	if f.LanguageId != "" {
		filters = append(filters, "language_id=:language_id")
		args["language_id"], err = strconv.Atoi(f.LanguageId)

		if err != nil {
			return "", nil, err
		}
	}

	if f.IsActive != "" {
		filters = append(filters, "is_active=:is_active")
		args["is_active"], err = strconv.ParseBool(f.IsActive)

		if err != nil {
			return "", nil, err
		}
	}

	return strings.Join(filters, " AND "), args, nil
}

func (f *ContentFilter) FromRequest(r *http.Request) {
	f.Search = r.FormValue("search")
	f.ContentTypeId = r.FormValue("content_type_id")
	f.LanguageId = r.FormValue("language_id")
	f.IsActive = r.FormValue("is_active")
}
