package models

import (
	"net/http"
	"strconv"
	"strings"
	"time"
)

type Village struct {
	Id          int64   `json:"id" db:"id"`
	CreatedAt   string  `json:"created_at" db:"created_at"`
	CreatedBy   string  `json:"created_by" db:"created_by"`
	UpdatedAt   *string `json:"updated_at" db:"updated_at"`
	UpdatedBy   *string `json:"updated_by" db:"updated_by"`
	DistrictId  int64   `json:"district_id" db:"district_id"`
	VillageCode string  `json:"village_code" db:"village_code"`
	Postcode    string  `json:"postcode" db:"postcode"`
	VillageName string  `json:"village_name" db:"village_name"`
	IsActive    bool    `json:"is_active" db:"is_active"`
}

type VillageFilter struct {
	Search      string `json:"search" db:"search"`
	DistrictId  string `json:"district_id" db:"district_id"`
	VillageCode string `json:"village_code" db:"village_code"`
	Postcode    string `json:"postcode" db:"postcode"`
	IsActive    string `json:"is_active" db:"is_active"`
}

func (v *Village) TableName() string {
	return "m_villages"
}

func (v *Village) GetKeyColumn() string {
	return "id"
}

func (v *Village) GetKey() any {
	return v.Id
}

func (v *Village) Get(id any) error {
	v.Id, _ = strconv.ParseInt(id.(string), 10, 64)

	return get(v, []string{"id", "district_id", "village_code", "postcode", "village_name", "is_active"})
}

func (v *Village) List(pr PaginationRequest, f Filter) ([]Model, int64, error) {
	var villages []Village

	total, err := list(&villages, v, []string{"id", "district_id", "village_code", "postcode", "village_name",
		"is_active"}, pr, f)
	if err != nil {
		return nil, 0, err
	}

	var models []Model
	for i, _ := range villages {
		models = append(models, &villages[i])
	}

	return models, total, nil
}

func (v *Village) Create() error {
	v.CreatedAt = time.Now().Format(time.DateTime)

	id, err := create(v, []string{"district_id", "village_code", "postcode", "village_name", "created_at", "created_by"}, nil)
	if err != nil {
		return err
	}

	v.Id = id.(int64)
	return nil
}

func (v *Village) Update() error {
	t := time.Now().Format(time.DateTime)
	v.UpdatedAt = &t

	return update(v, []string{"district_id", "village_code", "postcode", "village_name", "updated_at", "updated_by",
		"is_active"}, nil)
}

func (v *Village) Delete() error {
	return destroy(v, nil)
}

func (f *VillageFilter) Parse() (string, map[string]any, error) {
	var filters []string
	var err error
	args := make(map[string]any)

	if f.Search != "" {
		filters = append(filters, "LOWER(village_name) LIKE LOWER(:search)")
		args["search"] = "%" + f.Search + "%"
	}

	if f.DistrictId != "" {
		filters = append(filters, "district_id=:district_id")
		args["district_id"], err = strconv.Atoi(f.DistrictId)

		if err != nil {
			return "", nil, err
		}
	}

	if f.VillageCode != "" {
		filters = append(filters, "village_code=:village_code")
		args["village_code"] = f.VillageCode
	}

	if f.Postcode != "" {
		filters = append(filters, "postcode=:postcode")
		args["postcode"] = f.Postcode
	}

	if f.IsActive != "" {
		filters = append(filters, "is_active=:is_active")
		args["is_active"], err = strconv.ParseBool(f.IsActive)

		if err != nil {
			return "", nil, err
		}
	}

	return strings.Join(filters, " AND "), args, nil
}

func (f *VillageFilter) FromRequest(r *http.Request) {
	f.Search = r.FormValue("search")
	f.DistrictId = r.FormValue("district_id")
	f.VillageCode = r.FormValue("village_code")
	f.Postcode = r.FormValue("postcode")
	f.IsActive = r.FormValue("is_active")
}
