package models

import (
	"net/http"
	"strconv"
	"strings"
	"time"
)

type DepositType struct {
	Id                      int64      `json:"id" db:"id"`
	CreatedAt               time.Time  `json:"created_at" db:"created_at"`
	CreatedBy               string     `json:"created_by" db:"created_by"`
	UpdatedAt               *time.Time `json:"updated_at" db:"updated_at"`
	UpdatedBy               *string    `json:"updated_by" db:"updated_by"`
	DepositCode             string     `json:"deposit_code" db:"deposit_code"`
	DepositDescription      string     `json:"deposit_description" db:"deposit_description"`
	Currency                string     `json:"currency" db:"currency"`
	MinAmount               float64    `json:"min_amount" db:"min_amount"`
	MaxAmount               float64    `json:"max_amount" db:"max_amount"`
	AroRate                 float64    `json:"aro_rate" db:"aro_rate"`
	DepositPeriod           int        `json:"deposit_period" db:"deposit_period"`
	TaxFreq                 string     `json:"tax_freq" db:"tax_freq"`
	DepositTax              float64    `json:"deposit_tax" db:"deposit_tax"`
	TaxRestitution          float64    `json:"tax_restitution" db:"tax_restitution"`
	TaxRate                 float64    `json:"tax_rate" db:"tax_rate"`
	TaxDivider              int        `json:"tax_divider" db:"tax_divider"`
	AroFlag                 string     `json:"aro_flag" db:"aro_flag"`
	PeriodCode              string     `json:"period_code" db:"period_code"`
	ShariaNisbahForBank     *float64   `json:"sharia_nisbah_for_bank" db:"sharia_nisbah_for_bank"`
	ShariaNisbahForCustomer *float64   `json:"sharia_nisbah_for_customer" db:"sharia_nisbah_for_customer"`
	IsSharia                bool       `json:"is_sharia" db:"is_sharia"`
	IsActive                bool       `json:"is_active" db:"is_active"`
}

type DepositTypeFilter struct {
	Search      string `json:"search" db:"search"`
	DepositCode string `json:"deposit_code" db:"deposit_code"`
	Currency    string `json:"currency" db:"currency"`
	IsSharia    string `json:"is_sharia" db:"is_sharia"`
	IsActive    string `json:"is_active" db:"is_active"`
}

func (d *DepositType) TableName() string {
	return "m_deposit_types"
}

func (d *DepositType) GetKeyColumn() string {
	return "id"
}

func (d *DepositType) GetKey() any {
	return d.Id
}

func (d *DepositType) Get(id any) error {
	d.Id, _ = strconv.ParseInt(id.(string), 10, 64)

	return get(d, []string{"id", "deposit_code", "deposit_description", "currency", "min_amount", "max_amount", "aro_rate", "deposit_period", "tax_freq", "deposit_tax", "tax_restitution", "tax_rate", "tax_divider", "aro_flag", "period_code", "sharia_nisbah_for_bank", "sharia_nisbah_for_customer", "is_sharia", "is_active"})
}

func (d *DepositType) List(pr PaginationRequest, f Filter) ([]Model, int64, error) {
	var depositTypes []DepositType

	total, err := list(&depositTypes, d, []string{"id", "deposit_code", "deposit_description", "currency", "min_amount", "max_amount", "aro_rate", "deposit_period", "tax_freq", "deposit_tax", "tax_restitution", "tax_rate", "tax_divider", "aro_flag", "period_code", "sharia_nisbah_for_bank", "sharia_nisbah_for_customer", "is_sharia", "is_active"}, pr, f)
	if err != nil {
		return nil, 0, err
	}

	var models []Model
	for i, _ := range depositTypes {
		models = append(models, &depositTypes[i])
	}

	return models, total, nil
}

func (d *DepositType) Create() error {
	d.CreatedAt = time.Now()

	id, err := create(d, []string{"deposit_code", "deposit_description", "currency", "min_amount", "max_amount", "aro_rate", "deposit_period", "tax_freq", "deposit_tax", "tax_restitution", "tax_rate", "tax_divider", "aro_flag", "period_code", "sharia_nisbah_for_bank", "sharia_nisbah_for_customer", "is_sharia", "created_at", "created_by"}, nil)
	if err != nil {
		return err
	}

	d.Id = id.(int64)
	return nil
}

func (d *DepositType) Update() error {
	t := time.Now()
	d.UpdatedAt = &t

	return update(d, []string{"deposit_code", "deposit_description", "currency", "min_amount", "max_amount", "aro_rate", "deposit_period", "tax_freq", "deposit_tax", "tax_restitution", "tax_rate", "tax_divider", "aro_flag", "period_code", "sharia_nisbah_for_bank", "sharia_nisbah_for_customer", "is_sharia", "updated_at", "updated_by", "is_active"}, nil)
}

func (d *DepositType) Delete() error {
	return destroy(d, nil)
}

func (f *DepositTypeFilter) Parse() (string, map[string]any, error) {
	var filters []string
	var err error
	args := make(map[string]any)

	if f.Search != "" {
		filters = append(filters, "LOWER(deposit_description) LIKE LOWER(:search)")
		args["search"] = "%" + f.Search + "%"
	}

	if f.DepositCode != "" {
		filters = append(filters, "deposit_code=:deposit_code")
		args["deposit_code"] = f.DepositCode
	}

	if f.Currency != "" {
		filters = append(filters, "currency=:currency")
		args["currency"] = f.Currency
	}

	if f.IsSharia != "" {
		filters = append(filters, "is_sharia=:is_sharia")
		args["is_sharia"], err = strconv.ParseBool(f.IsSharia)

		if err != nil {
			return "", nil, err
		}
	}

	if f.IsActive != "" {
		filters = append(filters, "is_active=:is_active")
		args["is_active"], err = strconv.ParseBool(f.IsActive)

		if err != nil {
			return "", nil, err
		}
	}

	return strings.Join(filters, " AND "), args, nil
}

func (f *DepositTypeFilter) FromRequest(r *http.Request) {
	f.Search = r.FormValue("search")
	f.DepositCode = r.FormValue("deposit_code")
	f.Currency = r.FormValue("currency")
	f.IsSharia = r.FormValue("is_sharia")
	f.IsActive = r.FormValue("is_active")
}
