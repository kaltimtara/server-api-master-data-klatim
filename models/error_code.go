package models

import (
	"net/http"
	"strconv"
	"strings"
	"time"
)

type ErrorCode struct {
	Id               int64   `json:"id" db:"id"`
	CreatedAt        string  `json:"created_at" db:"created_at"`
	CreatedBy        string  `json:"created_by" db:"created_by"`
	UpdatedAt        *string `json:"updated_at" db:"updated_at"`
	UpdatedBy        *string `json:"updated_by" db:"updated_by"`
	TrxTypeId        int     `json:"trx_type_id" db:"trx_type_id"`
	ErrorCode        string  `json:"error_code" db:"error_code"`
	ErrorDescription string  `json:"error_description" db:"error_description"`
	Active           bool    `json:"is_active" db:"is_active"`
}

type ErrorCodeFilter struct {
	Search           string `json:"search" db:"search"`
	TrxTypeId        string `json:"trx_type_id" db:"trx_type_id"`
	ErrorCode        string `json:"error_code" db:"error_code"`
	ErrorDescription string `json:"error_description" db:"error_description"`
	Active           string `json:"is_active" db:"is_active"`
}

func (b *ErrorCode) TableName() string {
	return "m_error_codes"
}

func (b *ErrorCode) GetKeyColumn() string {
	return "id"
}

func (b *ErrorCode) GetKey() any {
	return b.Id
}

func (b *ErrorCode) Get(id any) error {
	b.Id, _ = strconv.ParseInt(id.(string), 10, 64)

	return get(b, []string{"id", "trx_type_id", "error_code", "error_description", "is_active"})
}

func (b *ErrorCode) List(pr PaginationRequest, f Filter) ([]Model, int64, error) {
	var transactionTypes []ErrorCode

	total, err := list(&transactionTypes, b, []string{"id", "trx_type_id", "error_code", "error_description", "is_active"}, pr, f)
	if err != nil {
		return nil, 0, err
	}

	var models []Model
	for i := range transactionTypes {
		models = append(models, &transactionTypes[i])
	}

	return models, total, nil
}

func (b *ErrorCode) Create() error {
	b.CreatedAt = time.Now().Format(time.DateTime)

	id, err := create(b, []string{"trx_type_id", "error_code", "error_description", "created_at", "created_by"}, nil)
	if err != nil {
		return err
	}

	b.Id = id.(int64)
	return nil
}

func (b *ErrorCode) Update() error {
	t := time.Now().Format(time.DateTime)
	b.UpdatedAt = &t

	return update(b, []string{"trx_type_id", "error_code", "error_description", "updated_at", "updated_by"}, nil)
}

func (b *ErrorCode) Delete() error {
	return destroy(b, nil)
}

func (f *ErrorCodeFilter) Parse() (string, map[string]any, error) {
	var filters []string
	var err error
	args := make(map[string]any)

	if f.Search != "" {
		filters = append(filters, "LOWER(error_description) LIKE LOWER(:search)")
		args["search"] = "%" + f.Search + "%"
	}

	if f.TrxTypeId != "" {
		filters = append(filters, "trx_type_id=:trx_type_id")
		args["trx_type_id"], _ = strconv.ParseInt(f.TrxTypeId, 10, 64)
	}

	if f.ErrorCode != "" {
		filters = append(filters, "error_code=:error_code")
		args["error_code"] = f.ErrorCode
	}

	if f.ErrorDescription != "" {
		filters = append(filters, "error_description=:error_description")
		args["error_description"] = f.ErrorDescription
	}

	if f.Active != "" {
		filters = append(filters, "is_active=:is_active")
		args["is_active"], err = strconv.ParseBool(f.Active)

		if err != nil {
			return "", nil, err
		}
	}

	return strings.Join(filters, " AND "), args, nil
}

func (f *ErrorCodeFilter) FromRequest(r *http.Request) {
	f.Search = r.FormValue("search")
	f.TrxTypeId = r.FormValue("trx_type_id")
	f.ErrorCode = r.FormValue("error_code")
	f.ErrorDescription = r.FormValue("error_description")
	f.Active = r.FormValue("is_active")
}
