package models

import (
	"net/http"
	"strconv"
	"strings"
	"time"
)

type City struct {
	Id           int64   `json:"id" db:"id"`
	CreatedAt    string  `json:"created_at" db:"created_at"`
	CreatedBy    string  `json:"created_by" db:"created_by"`
	UpdatedAt    *string `json:"updated_at" db:"updated_at"`
	UpdatedBy    *string `json:"updated_by" db:"updated_by"`
	ProvinceId   int     `json:"province_id" db:"province_id"`
	CityCode     string  `json:"city_code" db:"city_code"`
	BankCityCode string  `json:"bank_city_code" db:"bank_city_code"`
	CityName     string  `json:"city_name" db:"city_name"`
	IsActive     bool    `json:"is_active" db:"is_active"`
}

type CityFilter struct {
	Search       string `json:"search" db:"search"`
	ProvinceId   string `json:"province_id" db:"province_id"`
	CityCode     string `json:"city_code" db:"city_code"`
	BankCityCode string `json:"bank_city_code" db:"bank_city_code"`
	IsActive     string `json:"is_active" db:"is_active"`
}

func (c *City) TableName() string {
	return "m_cities"
}

func (c *City) GetKeyColumn() string {
	return "id"
}

func (c *City) GetKey() any {
	return c.Id
}

func (c *City) Get(id any) error {
	c.Id, _ = strconv.ParseInt(id.(string), 10, 64)

	return get(c, []string{"id", "province_id", "city_code", "bank_city_code", "city_name", "is_active"})
}

func (c *City) List(pr PaginationRequest, f Filter) ([]Model, int64, error) {
	var cities []City

	total, err := list(&cities, c, []string{"id", "province_id", "city_code", "bank_city_code", "city_name",
		"is_active"}, pr, f)
	if err != nil {
		return nil, 0, err
	}

	var models []Model
	for i, _ := range cities {
		models = append(models, &cities[i])
	}

	return models, total, nil
}

func (c *City) Create() error {
	c.CreatedAt = time.Now().Format(time.DateTime)

	id, err := create(c, []string{"city_code", "province_id", "bank_city_code", "city_name", "created_at", "created_by"}, nil)
	if err != nil {
		return err
	}

	c.Id = id.(int64)
	return nil
}

func (c *City) Update() error {
	t := time.Now().Format(time.DateTime)
	c.UpdatedAt = &t

	return update(c, []string{"province_id", "city_code", "bank_city_code", "city_name", "updated_at", "updated_by",
		"is_active"}, nil)
}

func (c *City) Delete() error {
	return destroy(c, nil)
}

func (f *CityFilter) Parse() (string, map[string]any, error) {
	var filters []string
	var err error
	args := make(map[string]any)

	if f.Search != "" {
		filters = append(filters, "LOWER(city_name) LIKE LOWER(:search)")
		args["search"] = "%" + f.Search + "%"
	}

	if f.ProvinceId != "" {
		filters = append(filters, "province_id=:province_id")
		args["province_id"], err = strconv.Atoi(f.ProvinceId)

		if err != nil {
			return "", nil, err
		}
	}

	if f.CityCode != "" {
		filters = append(filters, "city_code=:city_code")
		args["city_code"] = f.CityCode
	}

	if f.BankCityCode != "" {
		filters = append(filters, "bank_city_code=:bank_city_code")
		args["bank_city_code"] = f.BankCityCode
	}

	if f.IsActive != "" {
		filters = append(filters, "is_active=:is_active")
		args["is_active"], err = strconv.ParseBool(f.IsActive)

		if err != nil {
			return "", nil, err
		}
	}

	return strings.Join(filters, " AND "), args, nil
}

func (f *CityFilter) FromRequest(r *http.Request) {
	f.Search = r.FormValue("search")
	f.ProvinceId = r.FormValue("province_id")
	f.CityCode = r.FormValue("city_code")
	f.BankCityCode = r.FormValue("bank_city_code")
	f.IsActive = r.FormValue("is_active")
}
