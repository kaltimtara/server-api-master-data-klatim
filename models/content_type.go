package models

import (
	"net/http"
	"strconv"
	"strings"
	"time"
)

type ContentType struct {
	Id              int64   `json:"id" db:"id"`
	CreatedAt       string  `json:"created_at" db:"created_at"`
	CreatedBy       string  `json:"created_by" db:"created_by"`
	UpdatedAt       *string `json:"updated_at" db:"updated_at"`
	UpdatedBy       *string `json:"updated_by" db:"updated_by"`
	ContentTypeCode string  `json:"content_type_code" db:"content_type_code"`
	ContentType     string  `json:"content_type" db:"content_type"`
	IsActive        bool    `json:"is_active" db:"is_active"`
}

type ContentTypeFilter struct {
	Search          string `json:"search" db:"search"`
	ContentTypeCode string `json:"content_type_code" db:"content_type_code"`
	ContentType     string `json:"content_type" db:"content_type"`
	IsActive        string `json:"is_active" db:"is_active"`
}

func (c *ContentType) TableName() string {
	return "m_content_types"
}

func (c *ContentType) GetKeyColumn() string {
	return "id"
}

func (c *ContentType) GetKey() any {
	return c.Id
}

func (c *ContentType) Get(id any) error {
	c.Id, _ = strconv.ParseInt(id.(string), 10, 64)

	return get(c, []string{"id", "content_type", "content_type_code", "is_active"})
}

func (c *ContentType) List(pr PaginationRequest, f Filter) ([]Model, int64, error) {
	var contentTypes []ContentType

	total, err := list(&contentTypes, c, []string{"id", "content_type", "content_type_code", "is_active"}, pr, f)
	if err != nil {
		return nil, 0, err
	}

	var models []Model
	for i, _ := range contentTypes {
		models = append(models, &contentTypes[i])
	}

	return models, total, nil
}

func (c *ContentType) Create() error {
	c.CreatedAt = time.Now().Format(time.DateTime)

	id, err := create(c, []string{"content_type", "content_type_code", "created_at", "created_by"}, nil)
	if err != nil {
		return err
	}

	c.Id = id.(int64)
	return nil
}

func (c *ContentType) Update() error {
	t := time.Now().Format(time.DateTime)
	c.UpdatedAt = &t

	return update(c, []string{"content_type", "content_type_code", "updated_at", "updated_by", "is_active"}, nil)
}

func (c *ContentType) Delete() error {
	return destroy(c, nil)
}

func (f *ContentTypeFilter) Parse() (string, map[string]any, error) {
	var filters []string
	var err error
	args := make(map[string]any)

	if f.Search != "" {
		filters = append(filters, "LOWER(content_type) LIKE LOWER(:search)")
		args["search"] = "%" + f.Search + "%"
	}

	if f.ContentTypeCode != "" {
		filters = append(filters, "content_type_code=:content_type_code")
		args["content_type_code"] = f.ContentTypeCode
	}

	if f.ContentType != "" {
		filters = append(filters, "content_type=:content_type")
		args["content_type"] = f.ContentType
	}

	if f.IsActive != "" {
		filters = append(filters, "is_active=:is_active")
		args["is_active"], err = strconv.ParseBool(f.IsActive)

		if err != nil {
			return "", nil, err
		}
	}

	return strings.Join(filters, " AND "), args, nil
}

func (f *ContentTypeFilter) FromRequest(r *http.Request) {
	f.Search = r.FormValue("search")
	f.ContentTypeCode = r.FormValue("content_type_code")
	f.ContentType = r.FormValue("content_type")
	f.IsActive = r.FormValue("is_active")
}
