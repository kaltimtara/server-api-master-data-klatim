package models

import (
	"net/http"
	"strconv"
	"strings"
	"time"
)

type TransactionLimit struct {
	Id                int64   `json:"id" db:"id"`
	CreatedAt         string  `json:"created_at" db:"created_at"`
	CreatedBy         string  `json:"created_by" db:"created_by"`
	UpdatedAt         *string `json:"updated_at" db:"updated_at"`
	UpdatedBy         *string `json:"updated_by" db:"updated_by"`
	TrxTypeId         int64   `json:"trx_type_id" db:"trx_type_id"`
	DailyTrxMax       int64   `json:"daily_trx_max" db:"daily_trx_max"`
	DailyTrxAmountMax float64 `json:"daily_trx_amount_max" db:"daily_trx_amount_max"`
	TrxAmountMin      float64 `json:"trx_amount_min" db:"trx_amount_min"`
	TrxAmountMax      float64 `json:"trx_amount_max" db:"trx_amount_max"`
	Active            bool    `json:"is_active" db:"is_active"`
}

type TransactionLimitFilter struct {
	Search            string `json:"search" db:"search"`
	TrxTypeId         string `json:"trx_type_id" db:"trx_type_id"`
	DailyTrxMax       string `json:"daily_trx_max" db:"daily_trx_max"`
	DailyTrxAmountMax string `json:"daily_trx_amount_max" db:"daily_trx_amount_max"`
	TrxAmountMin      string `json:"trx_amount_min" db:"trx_amount_min"`
	TrxAmountMax      string `json:"trx_amount_max" db:"trx_amount_max"`
	Active            string `json:"is_active" db:"is_active"`
}

func (b *TransactionLimit) TableName() string {
	return "m_transaction_limits"
}

func (b *TransactionLimit) GetKeyColumn() string {
	return "id"
}

func (b *TransactionLimit) GetKey() any {
	return b.Id
}

func (b *TransactionLimit) Get(id any) error {
	b.Id, _ = strconv.ParseInt(id.(string), 10, 64)

	return get(b, []string{"id", "trx_type_id", "daily_trx_max", "daily_trx_amount_max", "trx_amount_min", "trx_amount_max", "is_active"})
}

func (b *TransactionLimit) List(pr PaginationRequest, f Filter) ([]Model, int64, error) {
	var transactionTypes []TransactionLimit

	total, err := list(&transactionTypes, b, []string{"id", "trx_type_id", "daily_trx_max", "daily_trx_amount_max", "trx_amount_min", "trx_amount_max", "is_active"}, pr, f)
	if err != nil {
		return nil, 0, err
	}

	var models []Model
	for i := range transactionTypes {
		models = append(models, &transactionTypes[i])
	}

	return models, total, nil
}

func (b *TransactionLimit) Create() error {
	b.CreatedAt = time.Now().Format(time.DateTime)

	id, err := create(b, []string{"trx_type_id", "daily_trx_max", "daily_trx_amount_max", "trx_amount_min", "trx_amount_max", "created_at", "created_by"}, nil)
	if err != nil {
		return err
	}

	b.Id = id.(int64)
	return nil
}

func (b *TransactionLimit) Update() error {
	t := time.Now().Format(time.DateTime)
	b.UpdatedAt = &t

	return update(b, []string{"trx_type_id", "daily_trx_max", "daily_trx_amount_max", "trx_amount_min", "trx_amount_max", "updated_at", "updated_by"}, nil)
}

func (b *TransactionLimit) Delete() error {
	return destroy(b, nil)
}

func (f *TransactionLimitFilter) Parse() (string, map[string]any, error) {
	var filters []string
	var err error
	args := make(map[string]any)

	if f.Search != "" {
		filters = append(filters, "LOWER(trx_type_id) LIKE LOWER(:search)")
		args["search"] = "%" + f.Search + "%"
	}

	if f.TrxTypeId != "" {
		filters = append(filters, "trx_type_id=:trx_type_id")
		args["trx_type_id"], _ = strconv.ParseInt(f.TrxTypeId, 10, 64)
	}

	if f.DailyTrxMax != "" {
		filters = append(filters, "daily_trx_max=:daily_trx_max")
		args["daily_trx_max"], _ = strconv.ParseInt(f.DailyTrxMax, 10, 64)
	}

	if f.DailyTrxAmountMax != "" {
		filters = append(filters, "daily_trx_amount_max=:daily_trx_amount_max")
		args["daily_trx_amount_max"], _ = strconv.ParseFloat(f.DailyTrxAmountMax, 64)
	}

	if f.TrxAmountMin != "" {
		filters = append(filters, "trx_amount_min=:trx_amount_min")
		args["trx_amount_min"], _ = strconv.ParseFloat(f.TrxAmountMin, 64)
	}

	if f.TrxAmountMax != "" {
		filters = append(filters, "trx_amount_max=:trx_amount_max")
		args["trx_amount_max"], _ = strconv.ParseFloat(f.TrxAmountMax, 64)
	}

	if f.Active != "" {
		filters = append(filters, "is_active=:is_active")
		args["is_active"], err = strconv.ParseBool(f.Active)

		if err != nil {
			return "", nil, err
		}
	}

	return strings.Join(filters, " AND "), args, nil
}

func (f *TransactionLimitFilter) FromRequest(r *http.Request) {
	f.Search = r.FormValue("search")
	f.TrxTypeId = r.FormValue("trx_type_id")
	f.DailyTrxMax = r.FormValue("daily_trx_max")
	f.DailyTrxAmountMax = r.FormValue("daily_trx_amount_max")
	f.TrxAmountMin = r.FormValue("trx_amount_min")
	f.TrxAmountMax = r.FormValue("trx_amount_max")
	f.Active = r.FormValue("is_active")
}
