package models

import (
	"net/http"
	"strconv"
	"strings"
	"time"
)

type Language struct {
	Id           int64   `json:"id" db:"id"`
	CreatedAt    string  `json:"created_at" db:"created_at"`
	CreatedBy    string  `json:"created_by" db:"created_by"`
	UpdatedAt    *string `json:"updated_at" db:"updated_at"`
	UpdatedBy    *string `json:"updated_by" db:"updated_by"`
	Language     string  `json:"language" db:"language"`
	LanguageCode string  `json:"language_code" db:"language_code"`
	IsActive     bool    `json:"is_active" db:"is_active"`
}

type LanguageFilter struct {
	Search       string `json:"search" db:"search"`
	Language     string `json:"language" db:"language"`
	LanguageCode string `json:"language_code" db:"language_code"`
	IsActive     string `json:"is_active" db:"is_active"`
}

func (l *Language) TableName() string {
	return "m_languages"
}

func (l *Language) GetKeyColumn() string {
	return "id"
}

func (l *Language) GetKey() any {
	return l.Id
}

func (l *Language) Get(id any) error {
	l.Id, _ = strconv.ParseInt(id.(string), 10, 64)

	return get(l, []string{"id", "language", "language_code", "is_active"})
}

func (l *Language) List(pr PaginationRequest, f Filter) ([]Model, int64, error) {
	var languages []Language

	total, err := list(&languages, l, []string{"id", "language", "language_code", "is_active"}, pr, f)
	if err != nil {
		return nil, 0, err
	}

	var models []Model
	for i, _ := range languages {
		models = append(models, &languages[i])
	}

	return models, total, nil
}

func (l *Language) Create() error {
	l.CreatedAt = time.Now().Format(time.DateTime)

	id, err := create(l, []string{"language", "language_code", "created_at", "created_by"}, nil)
	if err != nil {
		return err
	}

	l.Id = id.(int64)
	return nil
}

func (l *Language) Update() error {
	t := time.Now().Format(time.DateTime)
	l.UpdatedAt = &t

	return update(l, []string{"language", "language_code", "updated_at", "updated_by", "is_active"}, nil)
}

func (l *Language) Delete() error {
	return destroy(l, nil)
}

func (f *LanguageFilter) Parse() (string, map[string]any, error) {
	var filters []string
	var err error
	args := make(map[string]any)

	if f.Search != "" {
		filters = append(filters, "LOWER(language) LIKE LOWER(:search)")
		args["search"] = "%" + f.Search + "%"
	}

	if f.Language != "" {
		filters = append(filters, "language=:language")
		args["language"] = f.Language
	}

	if f.LanguageCode != "" {
		filters = append(filters, "language_code=:language_code")
		args["language_code"] = f.LanguageCode
	}

	if f.IsActive != "" {
		filters = append(filters, "is_active=:is_active")
		args["is_active"], err = strconv.ParseBool(f.IsActive)

		if err != nil {
			return "", nil, err
		}
	}

	return strings.Join(filters, " AND "), args, nil
}

func (f *LanguageFilter) FromRequest(r *http.Request) {
	f.Search = r.FormValue("search")
	f.Language = r.FormValue("language")
	f.LanguageCode = r.FormValue("language_code")
	f.IsActive = r.FormValue("is_active")
}
