package models

import (
	"net/http"
	"strconv"
	"strings"
	"time"
)

type DepositNumber struct {
	Id             int64   `json:"id" db:"id"`
	CreatedAt      string  `json:"created_at" db:"created_at"`
	CreatedBy      string  `json:"created_by" db:"created_by"`
	UpdatedAt      *string `json:"updated_at" db:"updated_at"`
	UpdatedBy      *string `json:"updated_by" db:"updated_by"`
	DepositTypeId  int64   `json:"deposit_type_id" db:"deposit_type_id"`
	BranchOfficeId int64   `json:"branch_office_id" db:"branch_office_id"`
	DepositNo      string  `json:"deposit_no" db:"deposit_no"`
	IsUsed         bool    `json:"is_used" db:"is_used"`
}

type DepositNumberFilter struct {
	Search         string `json:"search" db:"search"`
	DepositTypeId  string `json:"deposit_type_id" db:"deposit_type_id"`
	BranchOfficeId string `json:"branch_office_id" db:"branch_office_id"`
	DepositNo      string `json:"deposit_no" db:"deposit_no"`
	IsUsed         string `json:"is_used" db:"is_used"`
}

func (d *DepositNumber) TableName() string {
	return "m_deposit_numbers"
}

func (d *DepositNumber) GetKeyColumn() string {
	return "id"
}

func (d *DepositNumber) GetKey() any {
	return d.Id
}

func (d *DepositNumber) Get(id any) error {
	d.Id, _ = strconv.ParseInt(id.(string), 10, 64)

	return get(d, []string{"id", "deposit_type_id", "branch_office_id", "deposit_no", "is_used"})
}

func (d *DepositNumber) List(pr PaginationRequest, f Filter) ([]Model, int64, error) {
	var depositNumbers []DepositNumber

	total, err := list(&depositNumbers, d, []string{"id", "deposit_type_id", "branch_office_id", "deposit_no", "is_used"}, pr, f)
	if err != nil {
		return nil, 0, err
	}

	var models []Model
	for i, _ := range depositNumbers {
		models = append(models, &depositNumbers[i])
	}

	return models, total, nil
}

func (d *DepositNumber) Create() error {
	d.CreatedAt = time.Now().Format(time.DateTime)

	id, err := create(d, []string{"deposit_type_id", "branch_office_id", "deposit_no", "created_at", "created_by"}, nil)
	if err != nil {
		return err
	}

	d.Id = id.(int64)
	return nil
}

func (d *DepositNumber) Update() error {
	t := time.Now().Format(time.DateTime)
	d.UpdatedAt = &t

	return update(d, []string{"deposit_type_id", "branch_office_id", "deposit_no", "updated_at", "updated_by", "is_used"}, nil)
}

func (d *DepositNumber) Delete() error {
	return destroy(d, nil)
}

func (f *DepositNumberFilter) Parse() (string, map[string]any, error) {
	var filters []string
	var err error
	args := make(map[string]any)

	if f.Search != "" {
		filters = append(filters, "LOWER(deposit_no) LIKE LOWER(:search)")
		args["search"] = "%" + f.Search + "%"
	}

	if f.DepositTypeId != "" {
		filters = append(filters, "deposit_type_id=:deposit_type_id")
		args["deposit_type_id"] = f.DepositTypeId
	}

	if f.BranchOfficeId != "" {
		filters = append(filters, "branch_office_id=:branch_office_id")
		args["branch_office_id"], err = strconv.Atoi(f.BranchOfficeId)

		if err != nil {
			return "", nil, err
		}
	}

	if f.DepositNo != "" {
		filters = append(filters, "deposit_no=:deposit_no")
		args["deposit_no"] = f.DepositNo
	}

	if f.IsUsed != "" {
		filters = append(filters, "is_used=:is_used")
		args["is_used"], err = strconv.ParseBool(f.IsUsed)

		if err != nil {
			return "", nil, err
		}
	}

	return strings.Join(filters, " AND "), args, nil
}

func (f *DepositNumberFilter) FromRequest(r *http.Request) {
	f.Search = r.FormValue("search")
	f.DepositTypeId = r.FormValue("deposit_type_id")
	f.BranchOfficeId = r.FormValue("branch_office_id")
	f.DepositNo = r.FormValue("deposit_no")
	f.IsUsed = r.FormValue("is_used")
}
