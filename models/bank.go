package models

import (
	"net/http"
	"strconv"
	"strings"
	"time"
)

type Bank struct {
	Id           int64   `json:"id" db:"id"`
	CreatedAt    string  `json:"created_at" db:"created_at"`
	CreatedBy    string  `json:"created_by" db:"created_by"`
	UpdatedAt    *string `json:"updated_at" db:"updated_at"`
	UpdatedBy    *string `json:"updated_by" db:"updated_by"`
	Name         string  `json:"bank_name" db:"bank_name"`
	Alias        string  `json:"bank_name_alias" db:"bank_name_alias"`
	BICode       string  `json:"bi_bank_code" db:"bi_bank_code"`
	ClearingCode string  `json:"clearing_bank_code" db:"clearing_bank_code"`
	RTGSCode     string  `json:"rtgs_bank_code" db:"rtgs_bank_code"`
	CityCode     string  `json:"bank_city_code" db:"bank_city_code"`
	Sharia       bool    `json:"is_sharia" db:"is_sharia"`
	Active       bool    `json:"is_active" db:"is_active"`
}

type BankFilter struct {
	Search string `json:"search" db:"search"`
	Name   string `json:"bank_name" db:"bank_name"`
	Alias  string `json:"bank_name_alias" db:"bank_name_alias"`
	BICode string `json:"bi_bank_code" db:"bi_bank_code"`
	Sharia string `json:"is_sharia" db:"is_sharia"`
	Active string `json:"is_active" db:"is_active"`
}

func (b *Bank) TableName() string {
	return "m_banks"
}

func (b *Bank) GetKeyColumn() string {
	return "id"
}

func (b *Bank) GetKey() any {
	return b.Id
}

func (b *Bank) Get(id any) error {
	b.Id, _ = strconv.ParseInt(id.(string), 10, 64)

	return get(b, []string{"id", "bank_name", "bank_name_alias", "bi_bank_code", "clearing_bank_code", "rtgs_bank_code",
		"bank_city_code", "is_sharia", "is_active"})
}

func (b *Bank) List(pr PaginationRequest, f Filter) ([]Model, int64, error) {
	var banks []Bank

	total, err := list(&banks, b, []string{"id", "bank_name", "bank_name_alias", "bi_bank_code", "clearing_bank_code", "rtgs_bank_code",
		"bank_city_code", "is_sharia", "is_active"}, pr, f)
	if err != nil {
		return nil, 0, err
	}

	var models []Model
	for i, _ := range banks {
		models = append(models, &banks[i])
	}

	return models, total, nil
}

func (b *Bank) Create() error {
	b.CreatedAt = time.Now().Format(time.DateTime)

	id, err := create(b, []string{"bank_name", "bank_name_alias", "bi_bank_code", "clearing_bank_code", "rtgs_bank_code",
		"created_at", "created_by", "bank_city_code", "is_sharia"}, nil)
	if err != nil {
		return err
	}

	b.Id = id.(int64)
	return nil
}

func (b *Bank) Update() error {
	t := time.Now().Format(time.DateTime)
	b.UpdatedAt = &t

	return update(b, []string{"bank_name", "bank_name_alias", "bi_bank_code", "clearing_bank_code", "rtgs_bank_code", "updated_at",
		"updated_by", "bank_city_code", "is_sharia", "is_active"}, nil)
}

func (b *Bank) Delete() error {
	return destroy(b, nil)
}

func (f *BankFilter) Parse() (string, map[string]any, error) {
	var filters []string
	var err error
	args := make(map[string]any)

	if f.Search != "" {
		filters = append(filters, "(LOWER(bank_name) LIKE LOWER(:search_name) OR LOWER(bank_name_alias) LIKE LOWER("+
			":search_alias))")
		args["search_name"] = "%" + f.Search + "%"
		args["search_alias"] = "%" + f.Search + "%"
	}

	if f.Name != "" {
		filters = append(filters, "bank_name=:bank_name")
		args["bank_name"] = f.Name
	}

	if f.Alias != "" {
		filters = append(filters, "bank_name_alias=:bank_name_alias")
		args["bank_name_alias"] = f.Alias
	}

	if f.BICode != "" {
		filters = append(filters, "bi_bank_code=:bi_bank_code")
		args["bi_bank_code"] = f.BICode
	}

	if f.Sharia != "" {
		filters = append(filters, "is_sharia=:is_sharia")
		args["is_sharia"], err = strconv.ParseBool(f.Sharia)

		if err != nil {
			return "", nil, err
		}
	}

	if f.Active != "" {
		filters = append(filters, "is_active=:is_active")
		args["is_active"], err = strconv.ParseBool(f.Active)

		if err != nil {
			return "", nil, err
		}
	}

	return strings.Join(filters, " AND "), args, nil
}

func (f *BankFilter) FromRequest(r *http.Request) {
	f.Search = r.FormValue("search")
	f.Name = r.FormValue("bank_name")
	f.Alias = r.FormValue("bank_name_alias")
	f.BICode = r.FormValue("bi_bank_code")
	f.Sharia = r.FormValue("is_sharia")
	f.Active = r.FormValue("is_active")
}
