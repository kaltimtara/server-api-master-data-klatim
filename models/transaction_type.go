package models

import (
	"net/http"
	"strconv"
	"strings"
	"time"
)

type TransactionType struct {
	Id              int64   `json:"id" db:"id"`
	CreatedAt       string  `json:"created_at" db:"created_at"`
	CreatedBy       string  `json:"created_by" db:"created_by"`
	UpdatedAt       *string `json:"updated_at" db:"updated_at"`
	UpdatedBy       *string `json:"updated_by" db:"updated_by"`
	TransactionType string  `json:"trx_type" db:"trx_type"`
	Active          bool    `json:"is_active" db:"is_active"`
}

type TransactionTypeFilter struct {
	Search          string `json:"search" db:"search"`
	TransactionType string `json:"trx_type" db:"trx_type"`
	Active          string `json:"is_active" db:"is_active"`
}

func (b *TransactionType) TableName() string {
	return "m_transaction_types"
}

func (b *TransactionType) GetKeyColumn() string {
	return "id"
}

func (b *TransactionType) GetKey() any {
	return b.Id
}

func (b *TransactionType) Get(id any) error {
	b.Id, _ = strconv.ParseInt(id.(string), 10, 64)

	return get(b, []string{"id", "trx_type", "is_active"})
}

func (b *TransactionType) List(pr PaginationRequest, f Filter) ([]Model, int64, error) {
	var transactionTypes []TransactionType

	total, err := list(&transactionTypes, b, []string{"id", "trx_type", "is_active"}, pr, f)
	if err != nil {
		return nil, 0, err
	}

	var models []Model
	for i := range transactionTypes {
		models = append(models, &transactionTypes[i])
	}

	return models, total, nil
}

func (b *TransactionType) Create() error {
	b.CreatedAt = time.Now().Format(time.DateTime)

	id, err := create(b, []string{"trx_type", "created_at", "created_by"}, nil)
	if err != nil {
		return err
	}

	b.Id = id.(int64)
	return nil
}

func (b *TransactionType) Update() error {
	t := time.Now().Format(time.DateTime)
	b.UpdatedAt = &t

	return update(b, []string{"trx_type", "updated_at", "updated_by"}, nil)
}

func (b *TransactionType) Delete() error {
	return destroy(b, nil)
}

func (f *TransactionTypeFilter) Parse() (string, map[string]any, error) {
	var filters []string
	var err error
	args := make(map[string]any)

	if f.Search != "" {
		filters = append(filters, "LOWER(trx_type) LIKE LOWER(:search)")
		args["search"] = "%" + f.Search + "%"
	}

	if f.TransactionType != "" {
		filters = append(filters, "trx_type=:trx_type")
		args["trx_type"] = f.TransactionType
	}

	if f.Active != "" {
		filters = append(filters, "is_active=:is_active")
		args["is_active"], err = strconv.ParseBool(f.Active)

		if err != nil {
			return "", nil, err
		}
	}

	return strings.Join(filters, " AND "), args, nil
}

func (f *TransactionTypeFilter) FromRequest(r *http.Request) {
	f.Search = r.FormValue("search")
	f.TransactionType = r.FormValue("trx_type")
	f.Active = r.FormValue("is_active")
}
