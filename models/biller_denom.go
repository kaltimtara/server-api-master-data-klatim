package models

import (
	"net/http"
	"strconv"
	"strings"
	"time"
)

type BillerDenom struct {
	Id           int64   `json:"id" db:"id"`
	CreatedAt    string  `json:"created_at" db:"created_at"`
	CreatedBy    string  `json:"created_by" db:"created_by"`
	UpdatedAt    *string `json:"updated_at" db:"updated_at"`
	UpdatedBy    *string `json:"updated_by" db:"updated_by"`
	BillerTypeId int64   `json:"biller_type_id" db:"biller_type_id"`
	BillerDenom  float64 `json:"biller_denom" db:"biller_denom"`
	Description  string  `json:"description" db:"description"`
	IsActive     bool    `json:"is_active" db:"is_active"`
}

type BillerDenomFilter struct {
	Search       string `json:"search" db:"search"`
	BillerTypeId string `json:"biller_type_id" db:"biller_type_id"`
	BillerDenom  string `json:"biller_denom" db:"biller_denom"`
	Description  string `json:"description" db:"description"`
	IsActive     string `json:"is_active" db:"is_active"`
}

func (b *BillerDenom) TableName() string {
	return "m_biller_denoms"
}

func (b *BillerDenom) GetKeyColumn() string {
	return "id"
}

func (b *BillerDenom) GetKey() interface{} {
	return b.Id
}

func (b *BillerDenom) Get(id interface{}) error {
	b.Id, _ = strconv.ParseInt(id.(string), 10, 64)

	return get(b, []string{"id", "biller_type_id", "biller_denom", "description", "is_active"})
}

func (b *BillerDenom) List(pr PaginationRequest, f Filter) ([]Model, int64, error) {
	var billerDenoms []BillerDenom

	total, err := list(&billerDenoms, b, []string{"id", "biller_type_id", "biller_denom", "description", "is_active"}, pr, f)
	if err != nil {
		return nil, 0, err
	}

	var models []Model
	for i, _ := range billerDenoms {
		models = append(models, &billerDenoms[i])
	}

	return models, total, nil
}

func (b *BillerDenom) Create() error {
	b.CreatedAt = time.Now().Format(time.RFC3339)

	id, err := create(b, []string{"biller_type_id", "biller_denom", "description", "created_at", "created_by"}, nil)
	if err != nil {
		return err
	}

	b.Id = id.(int64)
	return nil
}

func (b *BillerDenom) Update() error {
	t := time.Now().Format(time.RFC3339)
	b.UpdatedAt = &t

	return update(b, []string{"biller_type_id", "biller_denom", "description", "updated_at", "updated_by", "is_active"}, nil)
}

func (b *BillerDenom) Delete() error {
	return destroy(b, nil)
}

func (f *BillerDenomFilter) Parse() (string, map[string]interface{}, error) {
	var filters []string
	var err error
	args := make(map[string]interface{})

	if f.Search != "" {
		filters = append(filters, "LOWER(description) LIKE LOWER(:search)")
		args["search"] = "%" + f.Search + "%"
	}

	if f.BillerTypeId != "" {
		filters = append(filters, "biller_type_id=:biller_type_id")
		args["biller_type_id"] = f.BillerTypeId
	}

	if f.BillerDenom != "" {
		filters = append(filters, "biller_denom=:biller_denom")
		args["biller_denom"], err = strconv.ParseFloat(f.BillerDenom, 64)

		if err != nil {
			return "", nil, err
		}
	}

	if f.Description != "" {
		filters = append(filters, "LOWER(description) LIKE LOWER(:description)")
		args["description"] = "%" + f.Description + "%"
	}

	if f.IsActive != "" {
		filters = append(filters, "is_active=:is_active")
		args["is_active"], err = strconv.ParseBool(f.IsActive)

		if err != nil {
			return "", nil, err
		}
	}

	return strings.Join(filters, " AND "), args, nil
}

func (f *BillerDenomFilter) FromRequest(r *http.Request) {
	f.Search = r.FormValue("search")
	f.BillerTypeId = r.FormValue("biller_type_id")
	f.BillerDenom = r.FormValue("biller_denom")
	f.Description = r.FormValue("description")
	f.IsActive = r.FormValue("is_active")
}
