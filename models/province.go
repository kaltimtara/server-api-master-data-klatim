package models

import (
	"net/http"
	"strconv"
	"strings"
	"time"
)

type Province struct {
	Id           int64   `json:"id" db:"id"`
	CreatedAt    string  `json:"created_at" db:"created_at"`
	CreatedBy    string  `json:"created_by" db:"created_by"`
	UpdatedAt    *string `json:"updated_at" db:"updated_at"`
	UpdatedBy    *string `json:"updated_by" db:"updated_by"`
	ProvinceCode string  `json:"province_code" db:"province_code"`
	ProvinceName string  `json:"province_name" db:"province_name"`
	Active       bool    `json:"is_active" db:"is_active"`
}

type ProvinceFilter struct {
	Search       string `json:"search" db:"search"`
	ProvinceCode string `json:"province_code"`
	ProvinceName string `json:"province_name"`
	Active       string `json:"is_active"`
}

func (p *Province) TableName() string {
	return "m_provinces"
}

func (p *Province) GetKeyColumn() string {
	return "id"
}

func (p *Province) GetKey() any {
	return p.Id
}

func (p *Province) Get(id any) error {
	p.Id, _ = strconv.ParseInt(id.(string), 10, 64)

	return get(p, []string{"id", "province_code", "province_name", "is_active"})
}

func (p *Province) List(pr PaginationRequest, f Filter) ([]Model, int64, error) {
	var provinces []Province

	total, err := list(&provinces, p, []string{"id", "province_code", "province_name", "is_active"}, pr, f)
	if err != nil {
		return nil, 0, err
	}

	var models []Model
	for i, _ := range provinces {
		models = append(models, &provinces[i])
	}

	return models, total, nil
}

func (p *Province) Create() error {
	p.CreatedAt = time.Now().Format(time.DateTime)

	id, err := create(p, []string{"province_code", "province_name", "created_at", "created_by"}, nil)
	if err != nil {
		return err
	}

	p.Id = id.(int64)
	return nil
}

func (p *Province) Update() error {
	t := time.Now().Format(time.DateTime)
	p.UpdatedAt = &t

	return update(p, []string{"province_code", "province_name", "updated_at", "updated_by", "is_active"}, nil)
}

func (p *Province) Delete() error {
	return destroy(p, nil)
}

func (f *ProvinceFilter) Parse() (string, map[string]interface{}, error) {
	var filters []string
	args := make(map[string]interface{})

	if f.Search != "" {
		filters = append(filters, "LOWER(province_name) LIKE LOWER(:search)")
		args["search"] = "%" + f.Search + "%"
	}

	if f.ProvinceCode != "" {
		filters = append(filters, "province_code=:province_code")
		args["province_code"] = f.ProvinceCode
	}

	if f.ProvinceName != "" {
		filters = append(filters, "province_name=:province_name")
		args["province_name"] = f.ProvinceName
	}

	if f.Active != "" {
		active, err := strconv.ParseBool(f.Active)
		if err != nil {
			return "", nil, err
		}
		filters = append(filters, "is_active=:is_active")
		args["is_active"] = active
	}

	return strings.Join(filters, " AND "), args, nil
}

func (f *ProvinceFilter) FromRequest(r *http.Request) {
	f.Search = r.FormValue("search")
	f.ProvinceCode = r.FormValue("province_code")
	f.ProvinceName = r.FormValue("province_name")
	f.Active = r.FormValue("is_active")
}
