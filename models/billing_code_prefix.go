package models

import (
	"net/http"
	"strconv"
	"strings"
	"time"
)

type BillingCodePrefix struct {
	Id           int64   `json:"id" db:"id"`
	CreatedAt    string  `json:"created_at" db:"created_at"`
	CreatedBy    string  `json:"created_by" db:"created_by"`
	UpdatedAt    *string `json:"updated_at" db:"updated_at"`
	UpdatedBy    *string `json:"updated_by" db:"updated_by"`
	BillerTypeId int64   `json:"biller_type_id" db:"biller_type_id"`
	BillerPrefix string  `json:"biller_prefix" db:"biller_prefix"`
	Description  string  `json:"description" db:"description"`
	IsActive     bool    `json:"is_active" db:"is_active"`
}

type BillingCodePrefixFilter struct {
	Search       string `json:"search" db:"search"`
	BillerTypeId string `json:"biller_type_id" db:"biller_type_id"`
	BillerPrefix string `json:"biller_prefix" db:"biller_prefix"`
	Description  string `json:"description" db:"description"`
	IsActive     string `json:"is_active" db:"is_active"`
}

func (b *BillingCodePrefix) TableName() string {
	return "m_billing_code_prefixes"
}

func (b *BillingCodePrefix) GetKeyColumn() string {
	return "id"
}

func (b *BillingCodePrefix) GetKey() interface{} {
	return b.Id
}

func (b *BillingCodePrefix) Get(id interface{}) error {
	b.Id, _ = strconv.ParseInt(id.(string), 10, 64)

	return get(b, []string{"id", "biller_type_id", "biller_prefix", "description", "is_active"})
}

func (b *BillingCodePrefix) List(pr PaginationRequest, f Filter) ([]Model, int64, error) {
	var billingCodePrefixes []BillingCodePrefix

	total, err := list(&billingCodePrefixes, b, []string{"id", "biller_type_id", "biller_prefix", "description", "is_active"}, pr, f)
	if err != nil {
		return nil, 0, err
	}

	var models []Model
	for i, _ := range billingCodePrefixes {
		models = append(models, &billingCodePrefixes[i])
	}

	return models, total, nil
}

func (b *BillingCodePrefix) Create() error {
	b.CreatedAt = time.Now().Format(time.RFC3339)

	id, err := create(b, []string{"biller_type_id", "biller_prefix", "description", "created_at", "created_by"}, nil)
	if err != nil {
		return err
	}

	b.Id = id.(int64)
	return nil
}

func (b *BillingCodePrefix) Update() error {
	t := time.Now().Format(time.RFC3339)
	b.UpdatedAt = &t

	return update(b, []string{"biller_type_id", "biller_prefix", "description", "updated_at", "updated_by", "is_active"}, nil)
}

func (b *BillingCodePrefix) Delete() error {
	return destroy(b, nil)
}

func (f *BillingCodePrefixFilter) Parse() (string, map[string]interface{}, error) {
	var filters []string
	var err error
	args := make(map[string]interface{})

	if f.Search != "" {
		filters = append(filters, "(LOWER(biller_prefix) LIKE LOWER(:search) OR LOWER(description) LIKE LOWER("+
			":search_description))")
		args["search"] = "%" + f.Search + "%"
		args["search_description"] = "%" + f.Search + "%"
	}

	if f.BillerTypeId != "" {
		filters = append(filters, "biller_type_id=:biller_type_id")
		args["biller_type_id"], _ = strconv.ParseInt(f.BillerTypeId, 10, 64)
	}

	if f.BillerPrefix != "" {
		filters = append(filters, "biller_prefix=:biller_prefix")
		args["biller_prefix"] = f.BillerPrefix
	}

	if f.Description != "" {
		filters = append(filters, "description=:description")
		args["description"] = f.Description
	}

	if f.IsActive != "" {
		filters = append(filters, "is_active=:is_active")
		args["is_active"], err = strconv.ParseBool(f.IsActive)

		if err != nil {
			return "", nil, err
		}
	}

	return strings.Join(filters, " AND "), args, nil
}

func (f *BillingCodePrefixFilter) FromRequest(r *http.Request) {
	f.Search = r.FormValue("search")
	f.BillerTypeId = r.FormValue("biller_type_id")
	f.BillerPrefix = r.FormValue("biller_prefix")
	f.Description = r.FormValue("description")
	f.IsActive = r.FormValue("is_active")
}
