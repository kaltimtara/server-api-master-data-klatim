package models

import (
	"net/http"
	"strconv"
	"strings"
	"time"
)

type BillerType struct {
	Id         int64   `json:"id" db:"id"`
	CreatedAt  string  `json:"created_at" db:"created_at"`
	CreatedBy  string  `json:"created_by" db:"created_by"`
	UpdatedAt  *string `json:"updated_at" db:"updated_at"`
	UpdatedBy  *string `json:"updated_by" db:"updated_by"`
	BillerType string  `json:"biller_type" db:"biller_type"`
	TrxTypeId  int64   `json:"trx_type_id" db:"trx_type_id"`
	IsActive   bool    `json:"is_active" db:"is_active"`
}

type BillerTypeFilter struct {
	Search     string `json:"search" db:"search"`
	BillerType string `json:"biller_type" db:"biller_type"`
	TrxTypeId  string `json:"trx_type_id" db:"trx_type_id"`
	IsActive   string `json:"is_active" db:"is_active"`
}

func (b *BillerType) TableName() string {
	return "m_biller_types"
}

func (b *BillerType) GetKeyColumn() string {
	return "id"
}

func (b *BillerType) GetKey() interface{} {
	return b.Id
}

func (b *BillerType) Get(id interface{}) error {
	b.Id, _ = strconv.ParseInt(id.(string), 10, 64)

	return get(b, []string{"id", "biller_type", "trx_type_id", "is_active"})
}

func (b *BillerType) List(pr PaginationRequest, f Filter) ([]Model, int64, error) {
	var billerTypes []BillerType

	total, err := list(&billerTypes, b, []string{"id", "biller_type", "trx_type_id", "is_active"}, pr, f)
	if err != nil {
		return nil, 0, err
	}

	var models []Model
	for i, _ := range billerTypes {
		models = append(models, &billerTypes[i])
	}

	return models, total, nil
}

func (b *BillerType) Create() error {
	b.CreatedAt = time.Now().Format(time.RFC3339)

	id, err := create(b, []string{"biller_type", "trx_type_id", "created_at", "created_by", "is_active"}, nil)
	if err != nil {
		return err
	}

	b.Id = id.(int64)
	return nil
}

func (b *BillerType) Update() error {
	t := time.Now().Format(time.RFC3339)
	b.UpdatedAt = &t

	return update(b, []string{"biller_type", "trx_type_id", "updated_at", "updated_by", "is_active"}, nil)
}

func (b *BillerType) Delete() error {
	return destroy(b, nil)
}

func (f *BillerTypeFilter) Parse() (string, map[string]interface{}, error) {
	var filters []string
	var err error
	args := make(map[string]interface{})

	if f.Search != "" {
		filters = append(filters, "LOWER(biller_type) LIKE LOWER(:search)")
		args["search"] = "%" + f.Search + "%"
	}

	if f.BillerType != "" {
		filters = append(filters, "biller_type=:biller_type")
		args["biller_type"] = f.BillerType
	}

	if f.TrxTypeId != "" {
		filters = append(filters, "trx_type_id=:trx_type_id")
		args["trx_type_id"], err = strconv.ParseInt(f.TrxTypeId, 10, 64)
	}

	if f.IsActive != "" {
		filters = append(filters, "is_active=:is_active")
		args["is_active"], err = strconv.ParseBool(f.IsActive)

		if err != nil {
			return "", nil, err
		}
	}

	return strings.Join(filters, " AND "), args, nil
}

func (f *BillerTypeFilter) FromRequest(r *http.Request) {
	f.Search = r.FormValue("search")
	f.BillerType = r.FormValue("biller_type")
	f.TrxTypeId = r.FormValue("trx_type_id")
	f.IsActive = r.FormValue("is_active")
}
