package models

import (
	"net/http"
	"strconv"
	"strings"
	"time"
)

type SettingType struct {
	Id          int64   `json:"id" db:"id"`
	CreatedAt   string  `json:"created_at" db:"created_at"`
	CreatedBy   string  `json:"created_by" db:"created_by"`
	UpdatedAt   *string `json:"updated_at" db:"updated_at"`
	UpdatedBy   *string `json:"updated_by" db:"updated_by"`
	SettingType string  `json:"setting_type" db:"setting_type"`
	Active      bool    `json:"is_active" db:"is_active"`
}

type SettingTypeFilter struct {
	Search      string `json:"search" db:"search"`
	SettingType string `json:"setting_type" db:"setting_type"`
	Active      string `json:"is_active" db:"is_active"`
}

func (b *SettingType) TableName() string {
	return "m_setting_types"
}

func (b *SettingType) GetKeyColumn() string {
	return "id"
}

func (b *SettingType) GetKey() any {
	return b.Id
}

func (b *SettingType) Get(id any) error {
	b.Id, _ = strconv.ParseInt(id.(string), 10, 64)

	return get(b, []string{"id", "setting_type", "is_active"})
}

func (b *SettingType) List(pr PaginationRequest, f Filter) ([]Model, int64, error) {
	var bankAccountTypes []SettingType

	total, err := list(&bankAccountTypes, b, []string{"id", "setting_type", "is_active"}, pr, f)
	if err != nil {
		return nil, 0, err
	}

	var models []Model
	for i := range bankAccountTypes {
		models = append(models, &bankAccountTypes[i])
	}

	return models, total, nil
}

func (b *SettingType) Create() error {
	b.CreatedAt = time.Now().Format(time.DateTime)

	id, err := create(b, []string{"setting_type", "created_at", "created_by"}, nil)
	if err != nil {
		return err
	}

	b.Id = id.(int64)
	return nil
}

func (b *SettingType) Update() error {
	t := time.Now().Format(time.DateTime)
	b.UpdatedAt = &t

	return update(b, []string{"setting_type", "updated_at", "updated_by"}, nil)
}

func (b *SettingType) Delete() error {
	return destroy(b, nil)
}

func (f *SettingTypeFilter) Parse() (string, map[string]any, error) {
	var filters []string
	var err error
	args := make(map[string]any)

	if f.Search != "" {
		filters = append(filters, "LOWER(setting_type) LIKE LOWER(:search)")
		args["search"] = "%" + f.Search + "%"
	}

	if f.SettingType != "" {
		filters = append(filters, "setting_type=:setting_type")
		args["setting_type"] = f.SettingType
	}

	if f.Active != "" {
		filters = append(filters, "is_active=:is_active")
		args["is_active"], err = strconv.ParseBool(f.Active)

		if err != nil {
			return "", nil, err
		}
	}

	return strings.Join(filters, " AND "), args, nil
}

func (f *SettingTypeFilter) FromRequest(r *http.Request) {
	f.Search = r.FormValue("search")
	f.SettingType = r.FormValue("setting_type")
	f.Active = r.FormValue("is_active")
}
