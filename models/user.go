package models

import "dg-datamaster-service/config"

type User struct {
	Id       int    `json:"id"`
	Username string `json:"username"`
	Password string `json:"password"`
}

func (u *User) Get(id int) error {
	u.Id = id

	err := config.DB().Get(u, "SELECT id, username, password FROM users WHERE id = $1", id)
	if err != nil {
		return err
	}

	return nil
}

func (u *User) GetByUsername(username string) error {
	u.Username = username

	err := config.DB().Get(u, "SELECT id, username, password FROM users WHERE username = $1", username)
	if err != nil {
		return err
	}

	return nil
}
