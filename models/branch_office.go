package models

import (
	"net/http"
	"strconv"
	"strings"
	"time"
)

type BranchOffice struct {
	Id               int64   `json:"id" db:"id"`
	CreatedAt        string  `json:"created_at" db:"created_at"`
	CreatedBy        string  `json:"created_by" db:"created_by"`
	UpdatedAt        *string `json:"updated_at" db:"updated_at"`
	UpdatedBy        *string `json:"updated_by" db:"updated_by"`
	BranchCode       string  `json:"branch_code" db:"branch_code"`
	ShariaBranchCode *string `json:"sharia_branch_code" db:"sharia_branch_code"`
	OfficeName       string  `json:"office_name" db:"office_name"`
	OfficeAddress    string  `json:"office_address" db:"office_address"`
	IsActive         bool    `json:"is_active" db:"is_active"`
}

type BranchOfficeFilter struct {
	Search           string `json:"search" db:"search"`
	BranchCode       string `json:"branch_code" db:"branch_code"`
	ShariaBranchCode string `json:"sharia_branch_code" db:"sharia_branch_code"`
	OfficeName       string `json:"office_name" db:"office_name"`
	IsActive         string `json:"is_active" db:"is_active"`
}

func (b *BranchOffice) TableName() string {
	return "m_branch_offices"
}

func (b *BranchOffice) GetKeyColumn() string {
	return "id"
}

func (b *BranchOffice) GetKey() any {
	return b.Id
}

func (b *BranchOffice) Get(id any) error {
	b.Id, _ = strconv.ParseInt(id.(string), 10, 64)

	return get(b, []string{"id", "branch_code", "sharia_branch_code", "office_name", "office_address", "is_active"})
}

func (b *BranchOffice) List(pr PaginationRequest, f Filter) ([]Model, int64, error) {
	var branchOffices []BranchOffice

	total, err := list(&branchOffices, b, []string{"id", "branch_code", "sharia_branch_code", "office_name", "office_address",
		"is_active"}, pr, f)
	if err != nil {
		return nil, 0, err
	}

	var models []Model
	for i, _ := range branchOffices {
		models = append(models, &branchOffices[i])
	}

	return models, total, nil
}

func (b *BranchOffice) Create() error {
	b.CreatedAt = time.Now().Format(time.DateTime)

	id, err := create(b, []string{"branch_code", "sharia_branch_code", "office_name", "office_address", "created_at", "created_by"}, nil)
	if err != nil {
		return err
	}

	b.Id = id.(int64)
	return nil
}

func (b *BranchOffice) Update() error {
	t := time.Now().Format(time.DateTime)
	b.UpdatedAt = &t

	return update(b, []string{"branch_code", "sharia_branch_code", "office_name", "office_address", "updated_at", "updated_by",
		"is_active"}, nil)
}

func (b *BranchOffice) Delete() error {
	return destroy(b, nil)
}

func (f *BranchOfficeFilter) Parse() (string, map[string]any, error) {
	var filters []string
	var err error
	args := make(map[string]any)

	if f.Search != "" {
		filters = append(filters, "LOWER(office_name) LIKE LOWER(:search)")
		args["search"] = "%" + f.Search + "%"
	}

	if f.BranchCode != "" {
		filters = append(filters, "branch_code=:branch_code")
		args["branch_code"] = f.BranchCode
	}

	if f.ShariaBranchCode != "" {
		filters = append(filters, "sharia_branch_code=:sharia_branch_code")
		args["sharia_branch_code"] = f.ShariaBranchCode
	}

	if f.OfficeName != "" {
		filters = append(filters, "office_name=:office_name")
		args["office_name"] = f.OfficeName
	}

	if f.IsActive != "" {
		filters = append(filters, "is_active=:is_active")
		args["is_active"], err = strconv.ParseBool(f.IsActive)

		if err != nil {
			return "", nil, err
		}
	}

	return strings.Join(filters, " AND "), args, nil
}

func (f *BranchOfficeFilter) FromRequest(r *http.Request) {
	f.Search = r.FormValue("search")
	f.BranchCode = r.FormValue("branch_code")
	f.ShariaBranchCode = r.FormValue("sharia_branch_code")
	f.OfficeName = r.FormValue("office_name")
	f.IsActive = r.FormValue("is_active")
}
