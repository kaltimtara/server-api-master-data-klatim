package models

import (
	"net/http"
	"strconv"
	"strings"
	"time"
)

type Holiday struct {
	Id                 int64   `json:"id" db:"id"`
	CreatedAt          string  `json:"created_at" db:"created_at"`
	CreatedBy          string  `json:"created_by" db:"created_by"`
	UpdatedAt          *string `json:"updated_at" db:"updated_at"`
	UpdatedBy          *string `json:"updated_by" db:"updated_by"`
	HolidayDate        string  `json:"holiday_date" db:"holiday_date"`
	HolidayDescription string  `json:"holiday_description" db:"holiday_description"`
	Active             bool    `json:"is_active" db:"is_active"`
}

type HolidayFilter struct {
	Search             string `json:"search" db:"search"`
	HolidayDate        string `json:"holiday_date"`
	HolidayDescription string `json:"holiday_description"`
	Active             string `json:"is_active"`
}

func (h *Holiday) TableName() string {
	return "m_holidays"
}

func (h *Holiday) GetKeyColumn() string {
	return "id"
}

func (h *Holiday) GetKey() any {
	return h.Id
}

func (h *Holiday) Get(id any) error {
	h.Id, _ = strconv.ParseInt(id.(string), 10, 64)

	return get(h, []string{"id", "holiday_date", "holiday_description", "is_active"})
}

func (h *Holiday) List(pr PaginationRequest, f Filter) ([]Model, int64, error) {
	var holidays []Holiday

	total, err := list(&holidays, h, []string{"id", "holiday_date", "holiday_description", "is_active"}, pr, f)
	if err != nil {
		return nil, 0, err
	}

	var models []Model
	for i, _ := range holidays {
		models = append(models, &holidays[i])
	}

	return models, total, nil
}

func (h *Holiday) Create() error {
	h.CreatedAt = time.Now().Format(time.DateTime)

	id, err := create(h, []string{"holiday_date", "holiday_description", "created_at", "created_by"}, nil)
	if err != nil {
		return err
	}

	h.Id = id.(int64)
	return nil
}

func (h *Holiday) Update() error {
	t := time.Now().Format(time.DateTime)
	h.UpdatedAt = &t

	return update(h, []string{"holiday_date", "holiday_description", "updated_at", "updated_by", "is_active"}, nil)
}

func (h *Holiday) Delete() error {
	return destroy(h, nil)
}

func (f *HolidayFilter) Parse() (string, map[string]interface{}, error) {
	var filters []string
	args := make(map[string]interface{})

	if f.Search != "" {
		filters = append(filters, "LOWER(holiday_description) LIKE LOWER(:search)")
		args["search"] = "%" + f.Search + "%"
	}

	if f.HolidayDate != "" {
		filters = append(filters, "holiday_date=:holiday_date")
		args["holiday_date"] = f.HolidayDate
	}

	if f.HolidayDescription != "" {
		filters = append(filters, "holiday_description=:holiday_description")
		args["holiday_description"] = f.HolidayDescription
	}

	if f.Active != "" {
		active, err := strconv.ParseBool(f.Active)
		if err != nil {
			return "", nil, err
		}
		filters = append(filters, "is_active=:is_active")
		args["is_active"] = active
	}

	return strings.Join(filters, " AND "), args, nil
}

func (f *HolidayFilter) FromRequest(r *http.Request) {
	f.Search = r.FormValue("search")
	f.HolidayDate = r.FormValue("holiday_date")
	f.HolidayDescription = r.FormValue("holiday_description")
	f.Active = r.FormValue("is_active")
}
