package models

import (
	"net/http"
	"strconv"
	"strings"
	"time"
)

type BankAccountType struct {
	Id              int64   `json:"id" db:"id"`
	CreatedAt       string  `json:"created_at" db:"created_at"`
	CreatedBy       string  `json:"created_by" db:"created_by"`
	UpdatedAt       *string `json:"updated_at" db:"updated_at"`
	UpdatedBy       *string `json:"updated_by" db:"updated_by"`
	BankAccountType string  `json:"bank_account_type" db:"bank_account_type"`
	BankAccountCode string  `json:"bank_account_code" db:"bank_account_code"`
	Description     string  `json:"description" db:"description"`
	MinBalance      float64 `json:"min_balance" db:"min_balance"`
	MaxBalance      float64 `json:"max_balance" db:"max_balance"`
	Sharia          bool    `json:"is_sharia" db:"is_sharia"`
	Active          bool    `json:"is_active" db:"is_active"`
}

type BankAccountTypeFilter struct {
	Search          string `json:"search" db:"search"`
	BankAccountType string `json:"bank_account_type" db:"bank_account_type"`
	BankAccountCode string `json:"bank_account_code" db:"bank_account_code"`
	Description     string `json:"description" db:"description"`
	Sharia          string `json:"is_sharia" db:"is_sharia"`
	Active          string `json:"is_active" db:"is_active"`
}

func (b *BankAccountType) TableName() string {
	return "m_bank_account_types"
}

func (b *BankAccountType) GetKeyColumn() string {
	return "id"
}

func (b *BankAccountType) GetKey() any {
	return b.Id
}

func (b *BankAccountType) Get(id any) error {
	b.Id, _ = strconv.ParseInt(id.(string), 10, 64)

	return get(b, []string{"id", "bank_account_code", "bank_account_type", "description", "min_balance", "max_balance", "is_sharia", "is_active"})
}

func (b *BankAccountType) List(pr PaginationRequest, f Filter) ([]Model, int64, error) {
	var bankAccountTypes []BankAccountType

	total, err := list(&bankAccountTypes, b, []string{"id", "bank_account_code", "bank_account_type", "description", "min_balance", "max_balance", "is_sharia", "is_active"}, pr, f)
	if err != nil {
		return nil, 0, err
	}

	var models []Model
	for i, _ := range bankAccountTypes {
		models = append(models, &bankAccountTypes[i])
	}

	return models, total, nil
}

func (b *BankAccountType) Create() error {
	b.CreatedAt = time.Now().Format(time.DateTime)

	id, err := create(b, []string{"bank_account_type", "bank_account_code", "description", "min_balance",
		"max_balance",
		"created_at",
		"created_by", "is_sharia"}, nil)
	if err != nil {
		return err
	}

	b.Id = id.(int64)
	return nil
}

func (b *BankAccountType) Update() error {
	t := time.Now().Format(time.DateTime)
	b.UpdatedAt = &t

	return update(b, []string{"bank_account_type", "bank_account_code", "description", "min_balance", "max_balance",
		"updated_at",
		"updated_by", "is_sharia"},
		nil)
}

func (b *BankAccountType) Delete() error {
	return destroy(b, nil)
}

func (f *BankAccountTypeFilter) Parse() (string, map[string]any, error) {
	var filters []string
	var err error
	args := make(map[string]any)

	if f.Search != "" {
		filters = append(filters, "(LOWER(bank_account_code) LIKE LOWER(:search_code) OR LOWER("+
			"bank_account_type) LIKE LOWER(:search))")
		args["search_code"] = "%" + f.Search + "%"
		args["search"] = "%" + f.Search + "%"
	}

	if f.BankAccountCode != "" {
		filters = append(filters, "bank_account_code=:bank_account_code")
		args["bank_account_code"] = f.BankAccountCode
	}

	if f.Sharia != "" {
		filters = append(filters, "is_sharia=:is_sharia")
		args["is_sharia"], err = strconv.ParseBool(f.Sharia)

		if err != nil {
			return "", nil, err
		}
	}

	if f.Active != "" {
		filters = append(filters, "is_active=:is_active")
		args["is_active"], err = strconv.ParseBool(f.Active)

		if err != nil {
			return "", nil, err
		}
	}

	return strings.Join(filters, " AND "), args, nil
}

func (f *BankAccountTypeFilter) FromRequest(r *http.Request) {
	f.Search = r.FormValue("search")
	f.BankAccountType = r.FormValue("bank_account_type")
	f.BankAccountCode = r.FormValue("bank_account_code")
	f.Sharia = r.FormValue("is_sharia")
	f.Active = r.FormValue("is_active")
}
