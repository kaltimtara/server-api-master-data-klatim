package models

import (
	"net/http"
	"strconv"
	"strings"
	"time"
)

type Setting struct {
	Id            int64   `json:"id" db:"id"`
	CreatedAt     string  `json:"created_at" db:"created_at"`
	CreatedBy     string  `json:"created_by" db:"created_by"`
	UpdatedAt     *string `json:"updated_at" db:"updated_at"`
	UpdatedBy     *string `json:"updated_by" db:"updated_by"`
	SettingTypeId int64   `json:"setting_type_id" db:"setting_type_id"`
	VariableName  string  `json:"variable_name" db:"variable_name"`
	VariableValue string  `json:"variable_value" db:"variable_value"`
	Active        bool    `json:"is_active" db:"is_active"`
}

type SettingFilter struct {
	Search        string `json:"search" db:"search"`
	SettingTypeId string `json:"setting_type_id" db:"setting_type_id"`
	VariableName  string `json:"variable_name" db:"variable_name"`
	VariableValue string `json:"variable_value" db:"variable_value"`
	Active        string `json:"is_active" db:"is_active"`
}

func (b *Setting) TableName() string {
	return "m_settings"
}

func (b *Setting) GetKeyColumn() string {
	return "id"
}

func (b *Setting) GetKey() any {
	return b.Id
}

func (b *Setting) Get(id any) error {
	b.Id, _ = strconv.ParseInt(id.(string), 10, 64)

	return get(b, []string{"id", "setting_type_id", "variable_name", "variable_value", "is_active"})
}

func (b *Setting) List(pr PaginationRequest, f Filter) ([]Model, int64, error) {
	var transactionTypes []Setting

	total, err := list(&transactionTypes, b, []string{"id", "setting_type_id", "variable_name", "variable_value", "is_active"}, pr, f)
	if err != nil {
		return nil, 0, err
	}

	var models []Model
	for i := range transactionTypes {
		models = append(models, &transactionTypes[i])
	}

	return models, total, nil
}

func (b *Setting) Create() error {
	b.CreatedAt = time.Now().Format(time.DateTime)

	id, err := create(b, []string{"setting_type_id", "variable_name", "variable_value", "created_at", "created_by"}, nil)
	if err != nil {
		return err
	}

	b.Id = id.(int64)
	return nil
}

func (b *Setting) Update() error {
	t := time.Now().Format(time.DateTime)
	b.UpdatedAt = &t

	return update(b, []string{"setting_type_id", "variable_name", "variable_value", "updated_at", "updated_by"}, nil)
}

func (b *Setting) Delete() error {
	return destroy(b, nil)
}

func (f *SettingFilter) Parse() (string, map[string]any, error) {
	var filters []string
	var err error
	args := make(map[string]any)

	if f.Search != "" {
		filters = append(filters, "LOWER(variable_name) LIKE LOWER(:search)")
		args["search"] = "%" + f.Search + "%"
	}

	if f.SettingTypeId != "" {
		filters = append(filters, "setting_type_id=:setting_type_id")
		args["setting_type_id"] = f.SettingTypeId
	}

	if f.VariableName != "" {
		filters = append(filters, "variable_name=:variable_name")
		args["variable_name"] = f.VariableName
	}

	if f.Active != "" {
		filters = append(filters, "variable_value=:variable_value")
		args["variable_value"], err = strconv.ParseBool(f.VariableValue)

		if err != nil {
			return "", nil, err
		}
	}

	if f.Active != "" {
		filters = append(filters, "is_active=:is_active")
		args["is_active"], err = strconv.ParseBool(f.Active)

		if err != nil {
			return "", nil, err
		}
	}

	return strings.Join(filters, " AND "), args, nil
}

func (f *SettingFilter) FromRequest(r *http.Request) {
	f.Search = r.FormValue("search")
	f.SettingTypeId = r.FormValue("setting_type_id")
	f.VariableName = r.FormValue("variable_name")
	f.VariableValue = r.FormValue("variable_value")
	f.Active = r.FormValue("is_active")
}
