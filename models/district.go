package models

import (
	"net/http"
	"strconv"
	"strings"
	"time"
)

type District struct {
	Id           int64   `json:"id" db:"id"`
	CreatedAt    string  `json:"created_at" db:"created_at"`
	CreatedBy    string  `json:"created_by" db:"created_by"`
	UpdatedAt    *string `json:"updated_at" db:"updated_at"`
	UpdatedBy    *string `json:"updated_by" db:"updated_by"`
	CityId       int64   `json:"city_id" db:"city_id"`
	DistrictCode string  `json:"district_code" db:"district_code"`
	DistrictName string  `json:"district_name" db:"district_name"`
	IsActive     bool    `json:"is_active" db:"is_active"`
}

type DistrictFilter struct {
	Search       string `json:"search" db:"search"`
	CityId       string `json:"city_id" db:"city_id"`
	DistrictCode string `json:"district_code" db:"district_code"`
	DistrictName string `json:"district_name" db:"district_name"`
	IsActive     string `json:"is_active" db:"is_active"`
}

func (d *District) TableName() string {
	return "m_districts"
}

func (d *District) GetKeyColumn() string {
	return "id"
}

func (d *District) GetKey() any {
	return d.Id
}

func (d *District) Get(id any) error {
	d.Id, _ = strconv.ParseInt(id.(string), 10, 64)

	return get(d, []string{"id", "city_id", "district_code", "district_name", "is_active"})
}

func (d *District) List(pr PaginationRequest, f Filter) ([]Model, int64, error) {
	var districts []District

	total, err := list(&districts, d, []string{"id", "city_id", "district_code", "district_name",
		"is_active"}, pr, f)
	if err != nil {
		return nil, 0, err
	}

	var models []Model
	for i, _ := range districts {
		models = append(models, &districts[i])
	}

	return models, total, nil
}

func (d *District) Create() error {
	d.CreatedAt = time.Now().Format(time.DateTime)

	id, err := create(d, []string{"district_code", "city_id", "district_name", "created_at", "created_by"}, nil)
	if err != nil {
		return err
	}

	d.Id = id.(int64)
	return nil
}

func (d *District) Update() error {
	t := time.Now().Format(time.DateTime)
	d.UpdatedAt = &t

	return update(d, []string{"city_id", "district_code", "district_name", "updated_at", "updated_by",
		"is_active"}, nil)
}

func (d *District) Delete() error {
	return destroy(d, nil)
}

func (f *DistrictFilter) Parse() (string, map[string]any, error) {
	var filters []string
	var err error
	args := make(map[string]any)

	if f.Search != "" {
		filters = append(filters, "LOWER(district_name) LIKE LOWER(:search)")
		args["search"] = "%" + f.Search + "%"
	}

	if f.CityId != "" {
		filters = append(filters, "city_id=:city_id")
		args["city_id"], err = strconv.Atoi(f.CityId)

		if err != nil {
			return "", nil, err
		}
	}

	if f.DistrictCode != "" {
		filters = append(filters, "district_code=:district_code")
		args["district_code"] = f.DistrictCode
	}

	if f.DistrictName != "" {
		filters = append(filters, "district_name=:district_name")
		args["district_name"] = f.DistrictName
	}

	if f.IsActive != "" {
		filters = append(filters, "is_active=:is_active")
		args["is_active"], err = strconv.ParseBool(f.IsActive)

		if err != nil {
			return "", nil, err
		}
	}

	return strings.Join(filters, " AND "), args, nil
}

func (f *DistrictFilter) FromRequest(r *http.Request) {
	f.Search = r.FormValue("search")
	f.CityId = r.FormValue("city_id")
	f.DistrictCode = r.FormValue("district_code")
	f.DistrictName = r.FormValue("district_name")
	f.IsActive = r.FormValue("is_active")
}
