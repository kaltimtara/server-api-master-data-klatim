package config

import (
	"fmt"
	"log"
	"os"
	"strconv"

	"github.com/jmoiron/sqlx"

	_ "github.com/lib/pq"
)

var db *sqlx.DB
var services ExternalServices
var jwtConfig JWTConfig

type ExternalServices struct {
	Log LogService `json:"log_service"`
}

type LogService struct {
	Host     string `json:"host"`
	Database string `json:"database"`
}

type JWTConfig struct {
	SecretKey string
	ExpiresIn int
}

func Init() {
	connectDatabase()
	setServices()
	setupJWT()
}

func DB() *sqlx.DB {
	return db
}

func Services() ExternalServices {
	return services
}

func JWT() JWTConfig {
	return jwtConfig
}

func connectDatabase() {
	var err error

	host := os.Getenv("DB_HOST")
	port := os.Getenv("DB_PORT")
	user := os.Getenv("DB_USER")
	password := os.Getenv("DB_PASSWORD")
	dbname := os.Getenv("DB_NAME")

	dsn := fmt.Sprintf("host='%s' port='%s' user='%s' password='%s' dbname='%s' sslmode=disable", host, port, user, password, dbname)
	fmt.Println(dsn)
	db, err = sqlx.Connect("postgres", dsn)
	if err != nil {
		log.Fatalln(err)
	}
}

func setServices() {
	services = ExternalServices{
		Log: LogService{
			Host:     os.Getenv("LOG_HOST"),
			Database: os.Getenv("LOG_DATABASE"),
		},
	}
}

func setupJWT() {
	var exp int
	e := os.Getenv("JWT_EXPIRES_IN")
	if e != "" {
		exp, _ = strconv.Atoi(e)
	} else {
		exp = 300

	}

	jwtConfig = JWTConfig{
		SecretKey: os.Getenv("JWT_SECRET_KEY"),
		ExpiresIn: exp,
	}
}
