package logging

import (
	"log"
	"net/url"
	"reflect"

	"encoding/json"
	"net/http"

	"dg-datamaster-service/config"
)

type Data map[string]interface{}

func send(logType string, collection string, msg string, data interface{}) {
	c := http.Client{}
	host := config.Services().Log.Host

	params, err := fillParams(collection, msg, data)
	if err != nil {
		log.Println("Error sending log: " + err.Error())
		return
	}

	_, err = c.PostForm(host+logType, params)
	if err != nil {
		log.Println("Error sending log: " + err.Error())
		log.Println(params)
		return
	}
}

func fillParams(collection string, msg string, data interface{}) (url.Values, error) {
	dataString, err := json.Marshal(data)
	if err != nil {
		return nil, err
	}

	db := config.Services().Log.Database

	params := url.Values{}
	params.Set("database", db)
	params.Set("collection", collection)
	params.Set("message", msg)
	params.Set("data", string(dataString))

	return params, nil
}

func Debug(collection string, msg string, data interface{}) {
	go send("debug", collection, msg, data)
}

func Info(collection string, msg string, data interface{}) {
	go send("info", collection, msg, data)
}

func Warning(collection string, msg string, data interface{}) {
	go send("warning", collection, msg, data)
}

func Error(collection string, msg string, data interface{}) {
	go send("error", collection, msg, data)
}

func OutgoingRequestResponse(path string, logType string, data any, headers http.Header) {
	var log any
	if reflect.TypeOf(data).Kind() == reflect.String {
		_ = json.Unmarshal([]byte(data.(string)), &log)
	} else {
		log = data
	}

	Info("outgoing_request", path, map[string]any{
		"type":    logType,
		"data":    log,
		"headers": headers,
	})
}

func IncomingRequest(path string, logType string, data any) {
	var d any
	if reflect.TypeOf(data).Kind() == reflect.String {
		_ = json.Unmarshal([]byte(data.(string)), &d)
	} else {
		d = data
	}

	Info("incoming_request", path, map[string]any{
		"type": logType,
		"data": d,
	})
}
