package auth

import (
	"dg-datamaster-service/config"
	"errors"
	"github.com/golang-jwt/jwt/v5"
	"time"
)

func GenerateToken(id string) (string, error) {
	conf := config.JWT()

	expirationTime := time.Now().Add(time.Duration(conf.ExpiresIn) * time.Second)
	claims := &jwt.RegisteredClaims{
		ID:        id,
		ExpiresAt: jwt.NewNumericDate(expirationTime),
		IssuedAt:  jwt.NewNumericDate(time.Now()),
	}

	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	tokenString, err := token.SignedString([]byte(conf.SecretKey))

	if err != nil {
		return "", err
	}

	return tokenString, nil
}

func ValidateToken(tknStr string) (*jwt.RegisteredClaims, error) {
	conf := config.JWT()

	claims := &jwt.RegisteredClaims{}

	tkn, err := jwt.ParseWithClaims(tknStr, claims, func(token *jwt.Token) (interface{}, error) {
		return []byte(conf.SecretKey), nil
	})

	if err != nil {
		if errors.Is(err, jwt.ErrSignatureInvalid) {
			return nil, err
		}
		return nil, err
	}

	if !tkn.Valid {
		return nil, err
	}

	return claims, nil
}
