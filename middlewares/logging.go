package middlewares

import (
	"bytes"
	"dg-datamaster-service/services/logging"
	"github.com/julienschmidt/httprouter"
	"io"
	"net/http"
)

func readRequestBody(r *http.Request) ([]byte, error) {
	var buf bytes.Buffer
	_, err := io.Copy(&buf, r.Body)
	if err != nil {
		return nil, err
	}

	r.Body = io.NopCloser(bytes.NewReader(buf.Bytes()))
	return buf.Bytes(), nil
}

func Logging(next httprouter.Handle) httprouter.Handle {
	return func(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
		requestBody, _ := readRequestBody(r)
		logging.IncomingRequest(r.RequestURI, "request", string(requestBody))

		next(w, r, ps)
	}
}
