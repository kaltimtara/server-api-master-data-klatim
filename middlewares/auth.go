package middlewares

import (
	"dg-datamaster-service/helpers"
	"dg-datamaster-service/services/auth"
	"github.com/julienschmidt/httprouter"
	"net/http"
	"strings"
)

func Auth(next httprouter.Handle) httprouter.Handle {
	return Logging(func(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
		authHeader := r.Header.Get("Authorization")
		if authHeader == "" {
			helpers.WriteResponse(r, w, helpers.UnauthorizedResponse())
			return
		}

		tknStr := strings.TrimPrefix(authHeader, "Bearer ")
		_, err := auth.ValidateToken(tknStr)
		if err != nil {
			helpers.WriteResponse(r, w, helpers.UnauthorizedResponse())
			return
		}

		next(w, r, ps)
	})
}
