package main

import (
	"dg-datamaster-service/config"
	"dg-datamaster-service/routes"
	"fmt"
	"log"
	"net/http"
	"os"

	"github.com/joho/godotenv"
)

// @title DG by Bankaltimtara Datamaster API
// @version 1.0
// @description This is the API documentation for DG by Bankaltimtara Datamaster API
// @contact.name API Support
// @contact.email info@lawangsewu.com
// @license.name Apache 2.0
// @license.url http://www.apache.org/licenses/LICENSE-2.0.html
// @BasePath /
func main() {
	// load .env file
	err := godotenv.Load(".env")
	if err != nil {
		log.Println("Error loading .env file")
	}

	//Load .env variables
	config.Init()

	//Load routes and run server
	r := routes.Router()
	fmt.Println("DG by Bankaltimtara Datamaster API is running on port: " + os.Getenv("PORT"))
	log.Fatal(http.ListenAndServe(":"+os.Getenv("PORT"), r))
}
