CREATE TABLE m_provinces (
    id SERIAL4 PRIMARY KEY,
    created_at TIMESTAMP NOT NULL,
    created_by VARCHAR(255) NOT NULL,
    updated_at TIMESTAMP,
    updated_by VARCHAR(255),
    province_code VARCHAR(255) NOT NULL UNIQUE,
    province_name VARCHAR(255) NOT NULL,
    is_active BOOLEAN DEFAULT TRUE
);