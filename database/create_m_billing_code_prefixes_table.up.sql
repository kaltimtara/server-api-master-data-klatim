CREATE TABLE m_billing_code_prefixes (
    id SERIAL PRIMARY KEY,
    created_at TIMESTAMP NOT NULL,
    created_by VARCHAR(255) NOT NULL,
    updated_at TIMESTAMP,
    updated_by VARCHAR(255),
    biller_type_id INT NOT NULL,
    biller_prefix VARCHAR(20) NOT NULL,
    description VARCHAR(255),
    is_active BOOLEAN DEFAULT FALSE,
    UNIQUE (biller_type_id, biller_prefix),
    FOREIGN KEY (biller_type_id) REFERENCES m_biller_types(id)
);

CREATE INDEX idx_m_billing_code_prefixes_biller_type_id_is_active ON m_billing_code_prefixes(biller_type_id, is_active);
CREATE INDEX idx_m_billing_code_prefixes_is_active ON m_billing_code_prefixes(is_active);