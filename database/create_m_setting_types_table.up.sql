CREATE TABLE m_setting_types (
    id SERIAL PRIMARY KEY,
    created_at timestamp NOT NULL,
    created_by varchar(255) NOT NULL,
    updated_at timestamp,
    updated_by varchar(255),
    setting_type varchar(255) NOT NULL,
    is_active BOOLEAN NOT NULL DEFAULT TRUE
);