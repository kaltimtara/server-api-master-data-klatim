CREATE TABLE m_transaction_options (
    id SERIAL PRIMARY KEY,
    created_at timestamp NOT NULL,
    created_by varchar(255) NOT NULL,
    updated_at timestamp,
    updated_by varchar(255),
    trx_type_id int NOT NULL,
    transaction_option varchar(255) NOT NULL,
    is_realtime BOOLEAN NOT NULL DEFAULT TRUE,
    is_active BOOLEAN NOT NULL DEFAULT TRUE,
    FOREIGN KEY (trx_type_id) REFERENCES m_transaction_types(id)
);

CREATE INDEX idx_m_transaction_options_trx_type_id_is_active ON m_transaction_options (trx_type_id, is_active);

