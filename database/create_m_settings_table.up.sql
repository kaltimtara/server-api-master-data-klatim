CREATE TABLE m_settings (
    id SERIAL PRIMARY KEY,
    created_at timestamp NOT NULL,
    created_by varchar(255) NOT NULL,
    updated_at timestamp,
    updated_by varchar(255),
    setting_type_id INT4 NOT NULL,
    variable_name varchar(255) NOT NULL,
    variable_value varchar(255) NOT NULL,
    is_active BOOLEAN NOT NULL DEFAULT TRUE,
    CONSTRAINT m_setting_uk UNIQUE (setting_type_id, variable_name)
);

ALTER TABLE m_settings
    ADD CONSTRAINT fk_setting_setting_type_id
    FOREIGN KEY (setting_type_id)
    REFERENCES m_setting_types(id);

