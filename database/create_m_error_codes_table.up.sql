CREATE TABLE m_error_codes (
    id SERIAL PRIMARY KEY,
    created_at timestamp NOT NULL,
    created_by varchar(255) NOT NULL,
    updated_at timestamp,
    updated_by varchar(255),
    trx_type_id int NOT NULL,
    error_code varchar(255) NOT NULL,
    error_description varchar(255) NOT NULL,
    is_active BOOLEAN NOT NULL DEFAULT TRUE,
    CONSTRAINT m_error_code_uk UNIQUE (trx_type_id, error_code),
    FOREIGN KEY (trx_type_id) REFERENCES m_transaction_types(id)
);

CREATE INDEX idx_trx_type_id_is_active ON m_error_codes(trx_type_id, is_active);
CREATE INDEX idx_trx_type_id_error_code_is_active ON m_error_codes(trx_type_id, error_code, is_active);
CREATE INDEX idx_is_active ON m_error_codes(is_active);

