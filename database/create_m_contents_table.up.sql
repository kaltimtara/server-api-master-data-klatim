CREATE TABLE m_contents (
    id SERIAL4 PRIMARY KEY,
    created_at TIMESTAMP NOT NULL,
    created_by VARCHAR(255) NOT NULL,
    updated_at TIMESTAMP,
    updated_by VARCHAR(255),
    content_type_id INT4 NOT NULL,
    language_id INT4 NOT NULL,
    sharia_content TEXT,
    conventional_content TEXT NOT NULL,
    is_active BOOLEAN DEFAULT TRUE,
    FOREIGN KEY (content_type_id) REFERENCES m_content_types(id),
    FOREIGN KEY (language_id) REFERENCES m_languages(id)
);

CREATE INDEX idx_m_contents_content_type_id_language_id_is_active ON m_contents (content_type_id, language_id, is_active);
CREATE INDEX idx_m_contents_content_type_id_is_active ON m_contents (content_type_id, is_active);
CREATE INDEX idx_m_contents_is_active ON m_contents (is_active);