CREATE TABLE m_transaction_limits (
    id SERIAL PRIMARY KEY,
    created_at timestamp NOT NULL,
    created_by varchar(255) NOT NULL,
    updated_at timestamp,
    updated_by varchar(255),
    trx_type_id INT4 NOT NULL,
    daily_trx_max INT4,
    daily_trx_amount_max numeric(15, 2) NOT NULL,
    trx_amount_min numeric(15, 2) DEFAULT 1.00,
    trx_amount_max numeric(15, 2),
    is_active BOOLEAN NOT NULL DEFAULT TRUE,
    CONSTRAINT uk_transaction_limit_trx_type_id UNIQUE (trx_type_id),
    CONSTRAINT fk_transaction_limit_trx_type_id FOREIGN KEY (trx_type_id) REFERENCES m_transaction_types(id)
);

CREATE INDEX idx_transaction_limit_is_active ON m_transaction_limits(is_active);
CREATE INDEX idx_transaction_limit_trx_type_id_is_active ON m_transaction_limits(trx_type_id);

