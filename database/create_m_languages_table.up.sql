CREATE TABLE m_languages (
    id SERIAL4 PRIMARY KEY,
    created_at TIMESTAMP NOT NULL,
    created_by VARCHAR(255) NOT NULL,
    updated_at TIMESTAMP,
    updated_by VARCHAR(255),
    language_code VARCHAR(5) NOT NULL,
    language VARCHAR(255) NOT NULL,
    is_active BOOLEAN DEFAULT TRUE
);