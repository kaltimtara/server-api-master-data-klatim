CREATE TABLE m_bank_account_types (
    id SERIAL PRIMARY KEY,
    created_at timestamp NOT NULL,
    created_by varchar(255) NOT NULL,
    updated_at timestamp,
    updated_by varchar(255),
    bank_account_code varchar(255) NOT NULL,
    bank_account_type varchar(255) NOT NULL,
    description text,
    min_balance numeric(15, 2) NOT NULL,
    max_balance numeric(15, 2) NOT NULL,
    is_sharia BOOLEAN NOT NULL DEFAULT FALSE,
    is_active BOOLEAN NOT NULL DEFAULT TRUE,
    CONSTRAINT m_bank_account_type_uk UNIQUE (bank_account_code, is_sharia)
);

CREATE INDEX idx_m_bank_account_types_is_sharia_is_active ON m_bank_account_types (is_sharia, is_active);
