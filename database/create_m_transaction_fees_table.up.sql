CREATE TABLE m_transaction_fees (
    id SERIAL PRIMARY KEY,
    created_at timestamp NOT NULL,
    created_by varchar(255) NOT NULL,
    updated_at timestamp,
    updated_by varchar(255),
    trx_type_id INT4 NOT NULL,
    is_fixed_amount BOOLEAN NOT NULL DEFAULT FALSE,
    fee_amount numeric(15, 2) NOT NULL,
    is_active BOOLEAN NOT NULL DEFAULT TRUE,
    CONSTRAINT uk_trx_type UNIQUE (trx_type_id),
    FOREIGN KEY (trx_type_id) REFERENCES m_transaction_types(id)
);

CREATE INDEX idx_trx_type_id_is_active ON m_transaction_fees (trx_type_id, is_active);
CREATE INDEX idx_is_active ON m_transaction_fees (is_active);

