CREATE TABLE m_banks (
     id SERIAL PRIMARY KEY,
     created_at timestamp NOT NULL,
     created_by VARCHAR(255) NOT NULL,
     updated_at timestamp,
     updated_by VARCHAR(255),
     bank_name VARCHAR(255) NOT NULL,
     bank_name_alias VARCHAR(100) NOT NULL,
     bi_bank_code VARCHAR(5) NOT NULL,
     clearing_bank_code numeric(15, 2) NOT NULL,
     rtgs_bank_code numeric(15, 2) NOT NULL,
     bank_city_code numeric(5, 2) NOT NULL DEFAULT 0,
     is_sharia BOOLEAN NOT NULL DEFAULT FALSE,
     is_active BOOLEAN NOT NULL DEFAULT TRUE,
     CONSTRAINT m_banks_uk UNIQUE (bi_bank_code, clearing_bank_code, rtgs_bank_code, bank_city_code)
);

CREATE INDEX idx_m_banks_bank_name_is_active ON m_banks (bank_name, is_active);
CREATE INDEX idx_m_banks_is_active ON m_banks (is_active);