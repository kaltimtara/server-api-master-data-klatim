CREATE TABLE m_cities (
    id SERIAL4 PRIMARY KEY,
    created_at TIMESTAMP NOT NULL,
    created_by VARCHAR(255) NOT NULL,
    updated_at TIMESTAMP,
    updated_by VARCHAR(255),
    province_id INT2 NOT NULL,
    city_code VARCHAR(6) NOT NULL,
    bank_city_code VARCHAR(5) NOT NULL,
    city_name VARCHAR(255) NOT NULL,
    is_active BOOLEAN DEFAULT TRUE,
    CONSTRAINT m_cities_uk UNIQUE (city_code, bank_city_code),
    FOREIGN KEY (province_id) REFERENCES m_provinces(id)
);

CREATE INDEX idx_m_cities_province_id_is_active ON m_cities (province_id, is_active);
CREATE INDEX idx_m_cities_city_code_is_active ON m_cities (city_code, is_active);
