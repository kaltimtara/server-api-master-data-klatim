CREATE TABLE m_districts (
    id SERIAL4 PRIMARY KEY,
    created_at TIMESTAMP NOT NULL,
    created_by VARCHAR(255) NOT NULL,
    updated_at TIMESTAMP,
    updated_by VARCHAR(255),
    city_id INT4 NOT NULL,
    district_code VARCHAR(10) NOT NULL UNIQUE,
    district_name VARCHAR(255) NOT NULL,
    is_active BOOLEAN DEFAULT TRUE
);

ALTER TABLE m_districts
    ADD CONSTRAINT fk_districts_city_id
    FOREIGN KEY (city_id)
    REFERENCES m_cities(id);