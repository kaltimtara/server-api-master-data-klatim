CREATE TABLE m_deposit_types (
    id SERIAL4 PRIMARY KEY,
    created_at TIMESTAMP NOT NULL,
    created_by VARCHAR(255) NOT NULL,
    updated_at TIMESTAMP,
    updated_by VARCHAR(255),
    deposit_code VARCHAR(255) NOT NULL,
    deposit_description TEXT NOT NULL,
    currency VARCHAR(5) NOT NULL,
    min_amount NUMERIC(15, 2) NOT NULL,
    max_amount NUMERIC(15, 2) NOT NULL,
    aro_rate NUMERIC(5, 2) DEFAULT 0,
    deposit_period INT2 NOT NULL,
    tax_freq VARCHAR(5) NOT NULL,
    deposit_tax NUMERIC(5, 2) DEFAULT 0,
    tax_restitution NUMERIC(15, 2) NOT NULL,
    tax_rate NUMERIC(5, 2) DEFAULT 0,
    tax_divider INT2 NOT NULL,
    aro_flag VARCHAR(5) NOT NULL,
    period_code VARCHAR(3) NOT NULL,
    sharia_nisbah_for_bank NUMERIC(5, 2),
    sharia_nisbah_for_customer NUMERIC(5, 2),
    is_sharia BOOLEAN DEFAULT FALSE,
    is_active BOOLEAN DEFAULT TRUE
);

ALTER TABLE m_deposit_types
ADD CONSTRAINT unique_bank_account_code_is_sharia UNIQUE (deposit_code, is_sharia);