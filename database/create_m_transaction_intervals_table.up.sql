CREATE TABLE m_transaction_intervals (
    id SERIAL PRIMARY KEY,
    created_at timestamp NOT NULL,
    created_by varchar(255) NOT NULL,
    updated_at timestamp,
    updated_by varchar(255),
    trx_type_id int NOT NULL,
    transaction_interval varchar(255) NOT NULL,
    is_active BOOLEAN NOT NULL DEFAULT TRUE,
    FOREIGN KEY (trx_type_id) REFERENCES m_transaction_types(id)
);

CREATE INDEX idx_m_transaction_intervals_trx_type_id_is_active ON m_transaction_intervals (trx_type_id, is_active);

