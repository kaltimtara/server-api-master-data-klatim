CREATE TABLE m_content_types (
    id SERIAL4 PRIMARY KEY,
    created_at TIMESTAMP NOT NULL,
    created_by VARCHAR(255) NOT NULL,
    updated_at TIMESTAMP,
    updated_by VARCHAR(255),
    content_type_code VARCHAR(5) NOT NULL,
    content_type VARCHAR(255) NOT NULL,
    is_active BOOLEAN DEFAULT TRUE
);

CREATE INDEX idx_m_content_types_is_active ON m_content_types (is_active);