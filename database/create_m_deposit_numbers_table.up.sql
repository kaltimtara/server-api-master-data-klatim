CREATE TABLE m_deposit_numbers (
    id SERIAL PRIMARY KEY,
    created_at TIMESTAMP NOT NULL,
    created_by VARCHAR(255) NOT NULL,
    updated_at TIMESTAMP,
    updated_by VARCHAR(255),
    deposit_type_id INT NOT NULL,
    branch_office_id INT NOT NULL,
    deposit_no VARCHAR(255) NOT NULL,
    is_used BOOLEAN DEFAULT FALSE,
    UNIQUE (deposit_type_id, branch_office_id, deposit_no),
    FOREIGN KEY (deposit_type_id) REFERENCES m_deposit_types(id),
    FOREIGN KEY (branch_office_id) REFERENCES m_branch_offices(id)
);