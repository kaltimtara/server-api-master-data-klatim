CREATE TABLE m_villages (
    id SERIAL4 PRIMARY KEY,
    created_at TIMESTAMP NOT NULL,
    created_by VARCHAR(255) NOT NULL,
    updated_at TIMESTAMP,
    updated_by VARCHAR(255),
    district_id INT2 NOT NULL,
    village_code VARCHAR(12) NOT NULL UNIQUE,
    postcode VARCHAR(5) NOT NULL,
    village_name VARCHAR(255) NOT NULL,
    is_active BOOLEAN DEFAULT TRUE
);

ALTER TABLE m_villages
    ADD CONSTRAINT fk_villages_district_id
    FOREIGN KEY (district_id)
    REFERENCES m_districts(id);