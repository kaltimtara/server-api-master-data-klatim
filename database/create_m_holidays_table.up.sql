CREATE TABLE m_holidays (
    id SERIAL PRIMARY KEY,
    created_at TIMESTAMP NOT NULL,
    created_by VARCHAR(255) NOT NULL,
    updated_at TIMESTAMP,
    updated_by VARCHAR(255),
    holiday_date DATE NOT NULL,
    holiday_description VARCHAR(255) NOT NULL,
    is_active BOOLEAN DEFAULT TRUE
);