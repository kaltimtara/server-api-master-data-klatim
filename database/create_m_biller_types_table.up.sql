CREATE TABLE m_biller_types (
    id SERIAL PRIMARY KEY,
    created_at TIMESTAMP NOT NULL,
    created_by VARCHAR(255) NOT NULL,
    updated_at TIMESTAMP,
    updated_by VARCHAR(255),
    biller_type VARCHAR(255),
    trx_type_id INT NOT NULL,
    is_active BOOLEAN DEFAULT TRUE,
    FOREIGN KEY (trx_type_id) REFERENCES m_transaction_types(id)
);

CREATE INDEX idx_m_biller_types_trx_type_id_is_active ON m_biller_types (trx_type_id, is_active);
CREATE INDEX idx_m_biller_types_is_active ON m_biller_types (is_active);