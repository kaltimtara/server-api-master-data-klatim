CREATE TABLE m_branch_offices (
    id SERIAL4 PRIMARY KEY,
    created_at TIMESTAMP NOT NULL,
    created_by VARCHAR(255) NOT NULL,
    updated_at TIMESTAMP,
    updated_by VARCHAR(255),
    branch_code VARCHAR(6) NOT NULL,
    sharia_branch_code VARCHAR(6),
    office_name VARCHAR(255) NOT NULL,
    office_address VARCHAR(255) NOT NULL,
    is_active BOOLEAN DEFAULT TRUE,
    CONSTRAINT m_branch_offices_uk UNIQUE (branch_code, sharia_branch_code)
);

CREATE INDEX idx_m_branch_offices_is_active ON m_branch_offices (is_active);